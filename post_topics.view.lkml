view: post_topics {
  sql_table_name: public.post_topics ;;

  dimension: content_id {
    type: string
    sql: ${TABLE}.content_id ;;
  }

  dimension: probability {
    type: number
    sql: ${TABLE}.probability ;;
  }

  dimension: report_id {
    type: string
    sql: ${TABLE}.report_id ;;
  }

  dimension: topic {
    type: string
    sql: ${TABLE}.topic ;;
  }

  dimension: topic_type {
    type: string
    sql: ${TABLE}.topic_type ;;
  }

  measure: count {
    type: count
    drill_fields: [reports.last_name, reports.middle_name, reports.report_id, reports.first_name]
  }
}
