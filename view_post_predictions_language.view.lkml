view: view_post_predictions_language {
  label: "Post Prediction - Language"
  derived_table: {
    sql_trigger_value:  SELECT COUNT(*) FROM public.predictions ;;
    distribution_style: all
    explore_source: predictions {
      column: post_id {field:predictions.content_id}
      column: language_value {field:predictions.value}
      filters: {
        field: predictions.prediction_type
        value: "LANGUAGE"
      }
      filters: {
        field: predictions.model_type
        value: "POST,POST^_IMAGE"
      }
    }
  }
  dimension: post_id {
    primary_key: yes
  }
  dimension: language_value {
    type: number
  }
}
