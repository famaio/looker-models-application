# If necessary, uncomment the line below to include explore_source.
# include: "application.model.lkml"

view: view_profile_flags {
  derived_table: {
    # trigger the rebuild of this table when the post_ratings table has updated. it is the last major table in the ETL update process.
    datagroup_trigger: etl_trigger
    distribution_style: all
    explore_source: companies {
      column: proper_case_name {}
      column: report_id { field: reports.report_id }
      column: profile_id { field: profiles.profile_id }
      column: alcohol_flag_count { field: post_flags.alcohol_flag_count }
      column: language_flag_count { field: post_flags.language_flag_count }
      column: type { field: profiles.type }
      column: bigotry_flag_count { field: post_flags.bigotry_flag_count }
      column: crime_flag_count { field: post_flags.crime_flag_count }
      column: drugs_flag_count { field: post_flags.drugs_flag_count }
      column: violence_flag_count { field: post_flags.violence_flag_count }
      column: sex_flag_count { field: post_flags.sex_flag_count }
      column: bad_flag_count { field: post_flags.bad_flag_count }
      column: good_flag_count { field: post_flags.good_flag_count }
    }
  }
#   dimension: compound_key {
#     primary_key: yes
#     hidden: yes
#     sql: ${TABLE}.report_id || '-' ||  ${TABLE}.profile_id;;
#   }
  dimension: proper_case_name {}
  dimension: report_id {}
  dimension: profile_id {
    primary_key: yes
  }
  dimension: alcohol_flag_count {
    type: number
  }
  dimension: language_flag_count {
    type: number
  }
  dimension: type {}
  dimension: bigotry_flag_count {
    type: number
  }
  dimension: crime_flag_count {
    type: number
  }
  dimension: drugs_flag_count {
    type: number
  }
  dimension: violence_flag_count {
    type: number
  }
  dimension: sex_flag_count {
    type: number
  }
  dimension: bad_flag_count {
    type: number
  }
  dimension: good_flag_count {
    type: number
  }
  dimension: risk {
    type: yesno
    sql: CASE WHEN ${bigotry_flag_count} > 0 THEN 1 ELSE 0 END ;;
  }

  measure: profiles_with_bad_flag {
    label: "Bad Flags - Profiles"
    type: count
    filters: {
      field: bad_flag_count
      value: ">0"
    }
  }

  measure: profiles_with_alcohol_flag {
    label: "Bad Flags - Alcohol Profiles"
    type: count
    filters: {
      field: alcohol_flag_count
      value: ">0"
    }
  }

  measure: profiles_with_bigotry_flag {
    label: "Bad Flags - Bigotry Profiles"
    type: count
    filters: {
      field: bigotry_flag_count
      value: ">0"
    }
  }

  measure: profiles_with_language_flag {
    label: "Bad Flags - Language Profiles"
    type: count
    filters: {
      field: language_flag_count
      value: ">0"
    }
  }

  measure: profiles_with_sex_flag {
    label: "Bad Flags - Sex Profiles"
    type: count
    filters: {
      field: sex_flag_count
      value: ">0"
    }
  }

  measure: profiles_with_drugs_flag {
    label: "Bad Flags - Drugs Profiles"
    type: count
    filters: {
      field: drugs_flag_count
      value: ">0"
    }
  }

  measure: profiles_with_violence_flag {
    label: "Bad Flags - Violence Profiles"
    type: count
    filters: {
      field: violence_flag_count
      value: ">0"
    }
  }

  measure: profiles_with_crime_flag {
    label: "Bad Flags - Crime Profiles"
    type: count
    filters: {
      field: crime_flag_count
      value: ">0"
    }
  }

  measure: facebook_profiles_bad_flags {
    label: "Bad Flags - Facebook Profiles"
    type: count
    filters: {
      field: type
      value: "FACEBOOK"
    }
    filters: {
      field: bad_flag_count
      value: ">0"
    }
  }
  measure: facebook_profiles_alcohol_flags {
    label: "Facebook - Alcohol Flags"
    type: count
    filters: {
      field: type
      value: "FACEBOOK"
    }
    filters: {
      field: alcohol_flag_count
      value: ">0"
    }
  }
  measure: facebook_profiles_language_flags {
    label: "Facebook - Language Flags"
    type: count
    filters: {
      field: type
      value: "FACEBOOK"
    }
    filters: {
      field: language_flag_count
      value: ">0"
    }
  }
  measure: facebook_profiles_sex_flags {
    label: "Facebook - Sex Flags"
    type: count
    filters: {
      field: type
      value: "FACEBOOK"
    }
    filters: {
      field: sex_flag_count
      value: ">0"
    }
  }
  measure: facebook_profiles_drugs_flags {
    label: "Facebook - Drugs Flags"
    type: count
    filters: {
      field: type
      value: "FACEBOOK"
    }
    filters: {
      field: drugs_flag_count
      value: ">0"
    }
  }
  measure: facebook_profiles_bigotry_flags {
    label: "Facebook - Bigotry Flags"
    type: count
    filters: {
      field: type
      value: "FACEBOOK"
    }
    filters: {
      field: bigotry_flag_count
      value: ">0"
    }
  }
  measure: facebook_profiles_violence_flags {
    label: "Facebook - Violence Flags"
    type: count
    filters: {
      field: type
      value: "FACEBOOK"
    }
    filters: {
      field: violence_flag_count
      value: ">0"
    }
  }
  measure: facebook_profiles_crime_flags {
    label: "Facebook - Crime Flags"
    type: count
    filters: {
      field: type
      value: "FACEBOOK"
    }
    filters: {
      field: crime_flag_count
      value: ">0"
    }
  }
  measure: twitter_profiles_bad_flags {
    label: "Bad Flags - Twitter Profiles"
    type: count
    filters: {
      field: type
      value: "TWITTER"
    }
    filters: {
      field: bad_flag_count
      value: ">0"
    }
  }
  measure: twitter_profiles_alcohol_flags {
    label: "Twitter - Alcohol Flags"
    type: count
    filters: {
      field: type
      value: "TWITTER"
    }
    filters: {
      field: alcohol_flag_count
      value: ">0"
    }
  }
  measure: twitter_profiles_language_flags {
    label: "Twitter - Language Flags"
    type: count
    filters: {
      field: type
      value: "TWITTER"
    }
    filters: {
      field: language_flag_count
      value: ">0"
    }
  }
  measure: twitter_profiles_sex_flags {
    label: "Twitter - Sex Flags"
    type: count
    filters: {
      field: type
      value: "TWITTER"
    }
    filters: {
      field: sex_flag_count
      value: ">0"
    }
  }
  measure: twitter_profiles_drugs_flags {
    label: "Twitter - Drugs Flags"
    type: count
    filters: {
      field: type
      value: "TWITTER"
    }
    filters: {
      field: drugs_flag_count
      value: ">0"
    }
  }
  measure: twitter_profiles_bigotry_flags {
    label: "Twitter - Bigotry Flags"
    type: count
    filters: {
      field: type
      value: "TWITTER"
    }
    filters: {
      field: bigotry_flag_count
      value: ">0"
    }
  }
  measure: twitter_profiles_violence_flags {
    label: "Twitter - Violence Flags"
    type: count
    filters: {
      field: type
      value: "TWITTER"
    }
    filters: {
      field: violence_flag_count
      value: ">0"
    }
  }
  measure: twitter_profiles_crime_flags {
    label: "Twitter - Crime Flags"
    type: count
    filters: {
      field: type
      value: "TWITTER"
    }
    filters: {
      field: crime_flag_count
      value: ">0"
    }
  }
  measure: instagram_profiles_bad_flags {
    label: "Bad Flags - Instagram Profiles"
    type: count
    filters: {
      field: type
      value: "INSTAGRAM"
    }
    filters: {
      field: bad_flag_count
      value: ">0"
    }
  }
  measure: google_profiles_bad_flags {
    label: "Bad Flags - Google Plus Profiles"
    type: count
    filters: {
      field: type
      value: "GOOGLEPLUS"
    }
    filters: {
      field: bad_flag_count
      value: ">0"
    }
  }
}
