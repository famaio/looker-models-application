view: flag_kits {
  sql_table_name: public.flag_kits ;;

  dimension: pk {
    primary_key: yes
    hidden: yes
    type: string
    sql: ${flag_kit_id} || ${version_id} ;;
  }

  dimension: flag_kit_id {
    type: string
    sql: ${TABLE}.flag_kit_id ;;
  }

  dimension: version_id {
    type: string
    sql: ${TABLE}.version_id ;;
  }

  dimension: name {
    type: string
    sql: ${TABLE}.name ;;
  }

  dimension: company_id {
    type: string
    sql: ${TABLE}.company_id ;;
  }

  dimension: sensitivities {
    type: string
    sql: ${TABLE}.sensitivities ;;
  }

  dimension: keywords {
    type: string
    sql: ${TABLE}.keywords ;;
  }

  dimension: stemmable_keywords {
    type: string
    sql: ${TABLE}.stemmable_keywords ;;
  }

  measure: version_count {
    type: count
    drill_fields: [flag_kit_id, name]
  }

  measure: flag_kit_count {
    type: count_distinct
    sql: ${name} ;;
    drill_fields: [flag_kit_id, name]
  }
}
