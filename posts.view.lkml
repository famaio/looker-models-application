view: posts {
  sql_table_name: public.posts ;;

  dimension: post_id {
    primary_key: yes
    type: string
    sql: ${TABLE}.post_id ;;
  }

  dimension_group: date_posted {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.date_posted ;;
  }

  dimension: deleted {
    type: yesno
    sql: ${TABLE}.deleted ;;
  }

  dimension: hidden {
    type: yesno
    sql: ${TABLE}.hidden ;;
  }

  dimension: post_type {
    type: string
    sql: ${TABLE}.post_type ;;
  }

  dimension: probability {
    type: number
    sql: ${TABLE}.probability ;;
  }

  dimension: profile_id {
    type: string
    sql: ${TABLE}.profile_id ;;
  }

  dimension: report_id {
    type: string
    sql: ${TABLE}.report_id ;;
  }

  dimension: flag_kit_version_id {
    type: string
    sql: ${TABLE}.flag_kit_version_id ;;
  }

  dimension: text {
    type: string
    #sql: ${TABLE}.text ;;
    sql: CASE WHEN ${TABLE}.text NOT LIKE '%\\\\u%' THEN replace(${TABLE}.text,'\\','')
              ELSE ${TABLE}.text END;;
    #sql: replace(${TABLE}.text,'\\','') ;;
  }

  dimension: url {
    label: "Post URL"
    type: string
    sql: ${TABLE}.url ;;
  }

  dimension: randomizer {
    type: number
    sql: RANDOM() ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  measure: count_distinct {
    type: count_distinct
    sql: ${post_id} ;;
    drill_fields: [detail*]
  }

  measure: twitter_post_count {
    type: count_distinct
    sql: ${post_id} ;;
    filters: {
      field: profiles.type
      value: "TWITTER"
    }
  }

  measure: tweets_and_retweets_post_count {
    type: count_distinct
    sql: ${post_id} ;;
    filters: {
      field: profiles.type
      value: "TWITTER"
    }
    filters: {
      field: posts.post_type
      value: "ORIGINAL, SHARED"
    }
    description: "Twitter post count excluding liked posts"
  }

  measure: facebook_post_count {
    type: count_distinct
    sql: ${post_id} ;;
    filters: {
      field: profiles.type
      value: "FACEBOOK"
    }
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      post_id,
      reports.last_name,
      reports.middle_name,
      reports.report_id,
      reports.first_name,
      reports.date_created_date,
      profiles.profile_id,
      profiles.display_name
    ]
  }
}
