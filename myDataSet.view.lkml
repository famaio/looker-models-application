view: myDataSet {
  # # You can specify the table name if it's different from the view name:
  sql_table_name: myDataSet.mySurveyAnswers ;;
  #
  # # Define your dimensions and measures here, like this:
#   dimension: date_time  {
#     type: date_time
#     sql: ${TABLE}.timestamp ;;
#   }

  dimension_group: dateGroup {
    type: time
    timeframes: [date, week, month,time]
    sql: ${TABLE}.timestamp ;;
  }

  dimension: report_id {
    primary_key: yes
    type: string
    sql: ${TABLE}.reportID ;;
  }
  measure: id_count {
    type: count
  }
  dimension: missed_linkedin {
    type: number
    sql: ${TABLE}.missedLI ;;
  }
  measure: total_missed_linkedin {
    type: sum
    sql: ${missed_linkedin} ;;
  }
  dimension: missed_facebook {
    type: number
    sql: ${TABLE}.missedFB ;;
  }
  measure: total_missed_facebook {
    type: sum
    sql: ${missed_facebook} ;;
  }
  dimension: missed_instagram {
    type: number
    sql: ${TABLE}.missedIG ;;
  }
  measure: total_missed_instagram {
    type: sum
    sql: ${missed_instagram} ;;
  }
  dimension: missed_twitter {
    type: number
    sql: ${TABLE}.missedTW ;;
  }
  measure: total_missed_twitter {
    type: sum
    sql: ${missed_twitter} ;;
  }
  dimension: reasons_Missed {
    type: string
    sql: ${TABLE}.reasonsMissed ;;
  }
  dimension: incorrect_linkedin {
    type: number
    sql: ${TABLE}.incorrectLI ;;
  }
  measure: total_incorrect_linkedin {
    type: sum
    sql: ${incorrect_linkedin} ;;
  }
  dimension: incorrect_facebook {
    type: number
    sql: ${TABLE}.incorrectFB ;;
  }
  measure: total_incorrect_facebook {
    type: sum
    sql: ${incorrect_facebook} ;;
  }
  dimension: incorrect_instagram {
    type: number
    sql: ${TABLE}.incorrectIG ;;
  }
  measure: total_incorrect_instagram {
    type: sum
    sql: ${incorrect_instagram} ;;
  }
  dimension: incorrect_twitter {
    type: number
    sql: ${TABLE}.incorrectTW ;;
  }
  measure: total_incorrect_twitter {
    type: sum
    sql: ${incorrect_twitter} ;;
  }
  dimension: reasons_Incorrect {
    type: string
    sql: ${TABLE}.reasonsIncorrect ;;
  }




}
