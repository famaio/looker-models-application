connection: "redshift"
# #
# # include: "*.view.lkml"                       # include all views in this project
# include: "fama.model.lkml"
# # # include: "my_dashboard.dashboard.lookml"   # include a LookML dashboard called my_dashboard
# #
# #
# # # # Select the views that should be a part of this model,
# # # # and define the joins that connect them together.
# # #
#
# explore: fama_operations {
#   label: "Fama Operations"
#   view_name: companies
#   extends: [companies]
#
#   join: view_confirm_actions {
#     view_label: "Rater Actions | Confirms"
#     type: left_outer
#     sql_on: ${view_confirm_actions.report_id} = ${reports.report_id} ;;
#     relationship: many_to_one
#   }
#
#   join: view_qa_actions {
#     view_label: "Rater Actions | QAs"
#     type: left_outer
#     sql_on: ${view_qa_actions.report_id} = ${reports.report_id} ;;
#     relationship: many_to_one
#   }
#
#   join: view_post_actions {
#     view_label: "Rater Actions | Posts"
#     type: left_outer
#     sql_on: ${view_post_actions.post_id} = ${posts.post_id} ;;
#     relationship: many_to_one
#   }
# }
#
#
# explore: post_audits{
#   label: "Post Audits"
#   view_name: companies
#   extends: [fama_operations]
#
#   join: view_audit_profiles {
#     view_label: "0 | Auditted Profiles"
#     type: inner
#     sql_on: ${view_audit_profiles.profile_id} = ${profiles.profile_id} ;;
#     relationship: one_to_one
#   }
# }
# # explore: fama {
# #   view_name: companies
# #   label: "Fama Data"
# #   view_label: "01 | Companies"
# #   description: "Main explore that uses companies as the main join table. This insures that companies without reports will still be accounted for."
# #
# #   join: users {
# #     view_label: "02 | Users"
# #     type: left_outer
# #     sql_on: ${users.company_id} = ${companies.company_id} ;;
# #     relationship: many_to_one
# #   }
# #
# #   join: reports {
# #     view_label: "03 | Reports"
# #     type:  left_outer
# #     sql_on: ${reports.user_id} = ${users.user_id}
# #       and ${reports.deleted} = false ;;
# #     relationship: many_to_one
# #   }
# #
# #   # second level base tables. profiles, articles and flag kits and their meta tables that are assoicated directly with the reports table
# #   join: profiles {
# #     view_label: "04 | Profiles"
# #     type: left_outer
# #     sql_on: ${profiles.report_id} = ${reports.report_id}
# #       and ${profiles.deleted} = false ;;
# #     relationship: many_to_one
# #   }
# #
# #   join: articles {
# #     view_label: "06 | Articles"
# #     type: left_outer
# #     sql_on: ${articles.report_id} = ${reports.report_id} ;;
# #     relationship: many_to_one
# #   }
# #
# #   join: article_flags {
# #     view_label: "07 | Article Flags"
# #     type: left_outer
# #     sql_on: ${article_flags.article_id} = ${articles.article_id} ;;
# #     relationship: many_to_one
# #   }
# #
# #   join: flag_kits {
# #     view_label: "05 | Flag Kits"
# #     type: left_outer
# #     sql_on: ${flag_kits.version_id} = ${reports.flag_kit_version_id} ;;
# #     relationship: many_to_one
# #   }
# #
# #   #third level base tables their meta tables that are associated directly with profiles,
# #
# #   join: posts {
# #     view_label: "08 | Posts"
# #     type: left_outer
# #     sql_on: ${posts.profile_id} = ${profiles.profile_id}
# #       and  ${posts.deleted} = false and ${posts.hidden} = false;;
# #     relationship: many_to_one
# #   }
# #
# #   join: post_flags {
# #     view_label: "09 | Post Flag"
# #     type: left_outer
# #     sql_on: ${post_flags.post_id} = ${posts.post_id} ;;
# #     relationship: many_to_one
# #   }
# # }
