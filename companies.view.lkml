view: companies {
  sql_table_name: public.companies ;;

  dimension: company_id {
    primary_key: yes
    type: string
    sql: ${TABLE}.company_id ;;
  }

  dimension: company_type {
    type: string
    sql: ${TABLE}.company_type ;;
  }

  dimension: deleted {
    type: yesno
    sql: ${TABLE}.deleted ;;
  }

  dimension: lower_case_name {
    type: string
    sql: ${TABLE}.lower_case_name ;;
  }

  dimension: proper_case_name {
    type: string
    sql: ${TABLE}.proper_case_name ;;
  }


   dimension: client_group {
    type: string
    sql:CASE
        WHEN ${TABLE}.lower_case_name ilike '%nba%'
          THEN 'NBA'
        WHEN ${TABLE}.lower_case_name ilike 'goodchem'
          THEN 'Good Chem'
        WHEN ${TABLE}.lower_case_name ilike 'association for talent development'
          THEN 'Association for Talent Development (ATD)'
        WHEN ${TABLE}.lower_case_name ilike 'bank of america'
          THEN 'Bank of America'
        WHEN ${TABLE}.lower_case_name ilike 'bank of america - profiles'
          THEN 'Bank of America'
        WHEN ${TABLE}.lower_case_name ilike '%bird%'
          THEN 'Bird Marella'
        WHEN ${TABLE}.lower_case_name ilike 'bluecross blueshield of tennessee'
          THEN 'BlueCross BlueShield of Tennessee'
        WHEN ${TABLE}.lower_case_name ilike 'boyden'
          THEN 'Boyden'
        WHEN ${TABLE}.lower_case_name ilike '%cleveland clinic%'
          THEN 'Cleveland Clinic'
        WHEN ${TABLE}.lower_case_name ilike '%summit%'
          THEN 'City of Lees Summit'
        WHEN ${TABLE}.lower_case_name ilike 'decipher global'
          THEN 'Decipher Global '
        WHEN ${TABLE}.lower_case_name ilike '%edelman%'
          THEN 'Edelman'
        WHEN ${TABLE}.lower_case_name ilike '%fruit%'
          THEN 'Fruit of the Loom'
        WHEN ${TABLE}.lower_case_name ilike '%gavin de becker%'
          THEN 'GDBA'
        WHEN ${TABLE}.lower_case_name ilike 'genpact'
          THEN 'Genpact'
        WHEN ${TABLE}.lower_case_name ilike 'genpact us hiring'
          THEN 'Genpact'
        WHEN ${TABLE}.lower_case_name ilike '%genpact executive%'
          THEN 'Genpact'
        WHEN ${TABLE}.lower_case_name ilike '%holcim%'
          THEN 'Holcim'
        WHEN ${TABLE}.lower_case_name ilike '%hueston%'
          THEN 'Hueston Hennigan'
        WHEN ${TABLE}.lower_case_name ilike '%intact%'
          THEN 'Intact Insurance'
        WHEN ${TABLE}.lower_case_name ilike 'kroll'
          THEN 'Kroll'
        WHEN ${TABLE}.lower_case_name ilike '%loreal uk%'
          THEN 'LOreal UK'
        WHEN ${TABLE}.lower_case_name ilike 'loreal canada'
          THEN 'LOreal Canada'
        WHEN ${TABLE}.lower_case_name ilike '%l3%'
          THEN 'L3 Technologies'
        WHEN ${TABLE}.lower_case_name ilike '%lacarguy%'
          THEN 'LA Car Guy'
        WHEN ${TABLE}.lower_case_name ilike '%lindenwood%'
          THEN 'Lindenwood University'
        WHEN ${TABLE}.lower_case_name ilike '%baseball%'
          THEN 'Major League Baseball'
        WHEN ${TABLE}.lower_case_name ilike 'mcafee'
          THEN 'McAfee'
        WHEN ${TABLE}.lower_case_name ilike 'miami dolphins'
          THEN 'Miami Dolphins'
        WHEN ${TABLE}.lower_case_name ilike '%mintz%'
          THEN 'Mintz Group'
        WHEN ${TABLE}.lower_case_name ilike 'new york times'
          THEN 'New York Times'
        WHEN ${TABLE}.lower_case_name ilike 'nfl'
          THEN 'NFL'
        WHEN ${TABLE}.lower_case_name ilike '%overland%'
          THEN 'Overland Park Police Department'
        WHEN ${TABLE}.lower_case_name ilike '%parker executive%'
          THEN 'Parker Executive Search'
        WHEN ${TABLE}.lower_case_name ilike '%pga%'
          THEN 'PGA Tour'
        WHEN ${TABLE}.lower_case_name ilike '%prospect medical%'
          THEN 'Prospect Medical - Hiring'
        WHEN ${TABLE}.lower_case_name ilike '%qatar%'
          THEN 'Qatar Petroleum'
        WHEN ${TABLE}.lower_case_name ilike '%sutton%'
          THEN 'Sutton Associates'
        WHEN ${TABLE}.lower_case_name ilike '%humane%'
          THEN 'The Institute for Humane Studies'
        WHEN ${TABLE}.lower_case_name ilike '%tufts%'
          THEN 'Tufts Health Plan'
        WHEN ${TABLE}.lower_case_name ilike '%vanderbloemen%'
          THEN 'Vanderbloemen'
        WHEN ${TABLE}.lower_case_name ilike '%wittkieffer%'
          THEN 'Witt Kieffer'
        WHEN ${TABLE}.lower_case_name ilike '%mto%'
          THEN 'Munger Tolles & Olson'
        WHEN ${TABLE}.lower_case_name ilike '%carrington%'
          THEN 'Richards Carrington'
        WHEN ${TABLE}.lower_case_name ilike '%werksman%'
          THEN 'Werksman Jackson'
        WHEN ${TABLE}.lower_case_name ilike '%wichita%'
          THEN 'Wichita Police Department'
        WHEN ${TABLE}.lower_case_name ilike '%via agency%'
          THEN 'The Via Group'
        WHEN ${TABLE}.lower_case_name ilike '%k1%'
          THEN 'K1 - Hiring'










        ELSE NULL END;;
  }


# Uncomment whenever we actually use these
#   dimension: msa_contract_group {
#     type: string
#     sql: ${TABLE}.msa_contract_group ;;
#   }

#   dimension: fcra_contract_group {
#     type: string
#     sql: ${TABLE}.fcra_contract_group ;;
#   }

  dimension: callback_headers {
    type: string
    sql: ${TABLE}.callback_headers ;;
  }

  dimension: callback_url {
    type: string
    sql: ${TABLE}.callback_url ;;
  }

  dimension: callbacks {
    type: string
    sql: ${TABLE}.callbacks ;;
  }

  dimension: lookback_window {
    type: number
    hidden: yes
    sql: ${TABLE}.lookback_window ;;
  }

  dimension: lookback_window_years {
    label: "Lookerback Window - Years"
    description: "Company has selected to hide posts before this number of years"
    type: number
    sql: (1.00000000*${lookback_window}/3153600)/10000 ;;
    value_format_name: decimal_2
  }

  dimension: news_probability {
    type: yesno
    sql: ${TABLE}.news_probability ;;
  }

  dimension: notes {
    type: string
    sql: ${TABLE}.notes ;;
  }

  dimension: segregate_users {
    type: yesno
    sql: ${TABLE}.segregate_users ;;
  }

  dimension: task_timers {
    type: string
    sql: ${TABLE}.task_timers ;;
  }

  dimension: real_time_search_company {
    label: "Real Time Search Company"
    type: yesno
    sql: CASE WHEN ${lower_case_name} = 'homeadvisor' THEN true
              WHEN ${lower_case_name} = 'homeadvisor-production' THEN true
              WHEN ${lower_case_name} = 'jbhunt-internal' THEN true
              WHEN ${lower_case_name} = 'rts demo corp res' THEN true
              WHEN ${lower_case_name} = 'barsala' THEN true ELSE false END;;
  }

  dimension: fake_companies {
    label: "Testing Company"
    type: yesno
    sql: CASE WHEN ${lower_case_name} = 'fama demo' THEN true
              WHEN ${lower_case_name} = 'famaexecdemo' THEN true
              WHEN ${lower_case_name} = 'data science data' THEN true
              WHEN ${lower_case_name} = 'rtn_test' THEN true
              WHEN ${lower_case_name} = 'fama administrator' THEN true
              WHEN ${lower_case_name} = 'web-signups' THEN true
              WHEN ${lower_case_name} = 'screenertestcompany' THEN true
              WHEN ${lower_case_name} = 'fadv-testing' THEN true
              WHEN ${lower_case_name} = 'stress-test' THEN true
              WHEN ${lower_case_name} = 'ops team testing' THEN true
              WHEN ${lower_case_name} = 'cleveland clinic' THEN true
              WHEN ${lower_case_name} = 'samplecompany' THEN true
              WHEN ${lower_case_name} = 'mlb testing' THEN true
              ELSE false END;;
  }


dimension: batch_company {
    label: "Batch Company"
    type: yesno
    sql: CASE WHEN ${lower_case_name} = 'genpact' THEN true
              WHEN ${lower_case_name} = 'genpact us hiring' THEN true
              WHEN ${lower_case_name} = 'zeel' THEN true
              WHEN ${lower_case_name} = 'wag' THEN true
              WHEN ${lower_case_name} = 'loreal canada' THEN true
              WHEN ${lower_case_name} = 'loreal uk' THEN true
              WHEN ${lower_case_name} = 'plexus' THEN true
              WHEN ${lower_case_name} = 'sterling - the goldman sachs group inc' THEN true
              WHEN ${lower_case_name} = 'univ of california' THEN true
              WHEN ${lower_case_name} = 'pga tour' THEN true
              WHEN ${lower_case_name} = 'miami dolphins' THEN true
              WHEN ${lower_case_name} = 'major league baseball' THEN true
              WHEN ${lower_case_name} = 'nba - g league' THEN true
              WHEN ${lower_case_name} = 'nba - combine' THEN true ELSE false END;;
   }

  measure: count {
    type: count
    drill_fields: [company_id, lower_case_name, proper_case_name]
  }
}
