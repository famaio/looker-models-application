include: "articles.view"

view: articles_relevant {

  extends: [articles]

  derived_table: {
    sql: select * from public.articles where relevant = true and deleted = false and hidden = false ;;
    distribution_style: all
  }

  dimension: relevant {
    hidden: yes
  }

  measure: relevant_count {
    hidden: yes
  }
}
