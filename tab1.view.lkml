view: tab1 {

  dimension: opp_name {
    type: string
    label: "                  Opportunity name"
    primary_key: yes
    sql: ${TABLE}.opp_name ;;
  }

  dimension: owner {
    type: string
    label: "                 Owner"
    sql: ${TABLE}.owner;;
  }

  dimension_group: start_date {
    type: time
    label: "                Start Date"
    description: "contract start date"
    datatype: date
    timeframes: [raw,date,week,month,fiscal_quarter_of_year,fiscal_quarter]
    sql: ${TABLE}.start_date;;
  }

  dimension_group: end_date {
    type: time
    label: "               End Date"
    description: "contract end date"
    datatype: date
    timeframes: [raw,date,week,month,fiscal_quarter_of_year,fiscal_quarter]
    sql: ${TABLE}.end_date;;
  }

  dimension: days_in {
    type: number
    hidden: yes
    sql: ${TABLE}.days_in;;
  }

  dimension_group: days_in {
    type: duration
    label: "              days in"
    description: "days into contract"
    sql_start: TIMESTAMP(${start_date_raw}) ;;
    sql_end: current_timestamp() ;;
    intervals: [day,week,month]
  }


  dimension: terms {
    type: string
    label: "             terms"
    description: "PAYG, annual subscription, auto-renewal"
    sql: ${TABLE}.terms;;
  }

  dimension: scope {
    type: string
    label: "            scope"
    description: "FB, IG, TW, summaries, 360, lookback window"
    sql: ${TABLE}.scope ;;
  }

  dimension: amount {
    type: number
    description: "contract money amount"
    label: "           Amount"
    #value_format:"$#.00;($#.00)"
    sql: REPLACE(REPLACE(${TABLE}.amount, "$",""),",","");;
  }

  dimension: number_screens {
    type: number
    description: "screens purchased in current contract"
    label: "          number of screens"
    sql: ${TABLE}.number_screens;;
  }

  dimension: rescreens{
   type: string
    label: "         rescreens"
   description: "none or quarterly"
   sql: ${TABLE}.rescreens;;
  }

  dimension: total_use_td {
    type: number
    label: "        Total Use to-date"
    description: "credits + current use"
    sql: ${TABLE}.total_use_td;;
  }

  dimension: credits {
    type: number
    label: "       Previous contract/credits"
    description: "reports ran on an earlier contract"
    sql: ${TABLE}.credits;;
  }

  dimension: current_use {
    type: string
    label: "      Current use"
    description: "reports ran in current contract"
    sql: ${TABLE}.current_use ;;
  }

  dimension: last_report_run {
    type: string
    label: "     Last report run"
    sql: ${TABLE}.last_report_run;;
  }

  dimension: percent_expected {
    type: string
    label: "    Percent expected"
    sql: ${TABLE}.percent_expected;;
  }

  dimension: percent_actual {
    type: string
    label: "   Percent actual"
    sql: ${TABLE}.percent_actual;;
  }

  dimension: days_remaining {
    type: number
    hidden: yes
    sql: ${TABLE}.days_remaining;;
  }

  dimension_group: days_remaining {
    type: duration
    label: "  Days remaining"
    description: "days remaining in contract"
    sql_start: current_timestamp() ;;
    sql_end: TIMESTAMP(${end_date_raw}) ;;
    intervals: [day,week,month]
  }

  dimension: segment {
    type: string
    label: " Segment"
    description: "non-employees, investigative, employees, hybrid investigators"
    sql: ${TABLE}.segment ;;
  }

  dimension: health {
    type: string
    label: "Health"
    sql: ${TABLE}.health ;;
  }



}
