view: post_ratings {
#   sql_table_name: public.post_ratings ;;

  derived_table: {
    datagroup_trigger: etl_trigger
    distribution_style: all
    sql:
      SELECT
        post_ratings.*,
        users.username
      FROM
        public.post_ratings
      LEFT JOIN users on users.user_id = public.post_ratings.user_id ;;
  }

  dimension: pk {
    primary_key: yes
    hidden: yes
    type: string
    sql: ${post_id} || ${rating_id} ;;
  }

  dimension: post_id {
    hidden: yes
    type: string
    sql: ${TABLE}.post_id ;;
  }

  dimension: rating_id {
    hidden: yes
    type: string
    sql: ${TABLE}.rating_id ;;
  }

  dimension: username {
    type: string
    sql: ${TABLE}.username;;
  }

  dimension: user_id {
    hidden: yes
    type: string
    sql: ${TABLE}.user_id ;;
  }

  dimension: auto_flagged {
    label: "Auto Flagged for Language"
    type: yesno
#     sql: CASE WHEN ${user_id} = '3f338f51-27ce-11e8-ac13-cb3c59c4f56a' THEN true
#               WHEN ${user_id} = '74567d11-8452-11e8-b59c-3d0fc96c1234' THEN true
#               WHEN ${user_id} = '48fdc4a1-8f7b-11e8-a08c-1d55f6ed7abe' THEN true ELSE false END;;
    sql: CASE WHEN ${username} LIKE ('ml-auto-rate%') THEN true ELSE false END;;
    description: "Column flag to show if a post was auto rated."

  }

  measure: count {
    label: "Rating Count"
    type: count
    drill_fields: [posts.post_id, users.user_id, users.username]
  }

  measure: count_distinct {
    type: count_distinct
    sql: ${post_id} ;;
    drill_fields: [posts.post_id, users.user_id, users.username]
  }

  measure: auto_flagged_post_count {
    label: "Total Auto Flag Posts"
    type: count_distinct
    sql: ${post_id} ;;
    filters: {
      field: auto_flagged
      value: "yes"
    }
  }
}
