view: view_post_predictions_violence {
  label: "Post Prediction - Violence"
  derived_table: {
    sql_trigger_value:  SELECT COUNT(*) FROM public.predictions ;;
    distribution_style: all
    explore_source: predictions {
      column: post_id {field:predictions.content_id}
      column: violence_value {field:predictions.value}
      filters: {
        field: predictions.prediction_type
        value: "OTHER_BAD"
      }
      filters: {
        field: predictions.model_type
        value: "POST,POST^_IMAGE"
      }
    }
  }
  dimension: post_id {
    primary_key: yes
  }
  dimension: violence_value {
    type: number
  }
}
