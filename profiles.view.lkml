view: profiles {
  sql_table_name: public.profiles ;;

#   derived_table: {
#     distribution_style: all
#     datagroup_trigger: etl_trigger
#     sql: select * from public.profiles where deleted = false ;;
#   }

  dimension: profile_id {
    primary_key: yes
    type: string
    sql: ${TABLE}.profile_id ;;
  }

  dimension: added_by_rater {
    type: string
    sql: ${TABLE}.added_by_rater ;;
  }

  dimension: deleted {
    type: yesno
    sql: ${TABLE}.deleted ;;
  }

  dimension: display_name {
    type: string
    sql: ${TABLE}.display_name ;;
  }

  dimension: handle {
    type: string
    sql: ${TABLE}.handle ;;
  }

  dimension: picture_url {
    type: string
    sql: ${TABLE}.picture_url ;;
  }

  dimension: report_id {
    type: string
    sql: ${TABLE}.report_id ;;
  }

  dimension: flag_kit_version_id {
    type: string
    sql: ${TABLE}.flag_kit_version_id ;;
  }

  dimension: status {
    type: string
    sql: ${TABLE}.status ;;
  }

  dimension: type {
    type: string
    sql: ${TABLE}.type ;;
  }

  dimension: url {
    type: string
    sql: ${TABLE}.url ;;
  }

  dimension: domain_url {
    type: string
    sql: REGEXP_SUBSTR(${url}, '[^/]+\\.[^/:]+') ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

 # Wouldn't this give you a maximum of 1 per type?
  measure: count_distinct_type {
    type: count_distinct
    sql:  ${type} ;;
    drill_fields: [detail*]
  }

  measure: confirmed_count {
    type:  count
    filters: {
      field: status
      value: "CONFIRMED"
    }
    drill_fields: [detail*]
  }

  measure: unconfirmed_count {
    type:  count
    filters: {
      field: status
      value: "UNCONFIRMED"
    }
    drill_fields: [detail*]
  }

  #these measures count the number of profiles
  measure: facebook_count {
    type:  count
    filters: {
      field: type
      value: "FACEBOOK"
    }
    drill_fields: [detail*]
  }

  measure: twitter_count {
    type:  count
    filters: {
      field: type
      value: "TWITTER"
    }
    drill_fields: [detail*]
  }

  measure: instagram_count {
    type:  count
    filters: {
      field: type
      value: "INSTAGRAM"
    }
    drill_fields: [detail*]
  }

  measure: linkedin_count {
    type:  count
    filters: {
      field: type
      value: "LINKEDIN"
    }
    drill_fields: [detail*]
  }
  ##############################################
  #these measure the number of reports
  measure: report_twitter_count {
    label: "Reports with a Twitter"
    type:  count_distinct
    sql: ${report_id} ;;
    filters: {
      field: type
      value: "TWITTER"
    }
    drill_fields: [detail*]
  }

  measure: report_facebook_count {
    label: "Reports with a Facebook"
    type:  count_distinct
    sql: ${report_id} ;;
    filters: {
      field: type
      value: "FACEBOOK"
    }
    drill_fields: [detail*]
  }

  measure: report_instagram_count {
    label: "Reports with an Instagram"
    type:  count_distinct
    sql: ${report_id} ;;
    filters: {
      field: type
      value: "INSTAGRAM"
    }
    drill_fields: [detail*]
  }

  measure: report_linkedin_count {
    label: "Reports with a Linkedin"
    type:  count_distinct
    sql: ${report_id} ;;
    filters: {
      field: type
      value: "LINKEDIN"
    }
    drill_fields: [detail*]
  }

  measure: report_count {
    label: "Report Count"
    type: count_distinct
    sql: ${report_id} ;;
  }
  ######################################

  measure: big_5_count {
    type:  count_distinct
    label: "Big 4 Count"
    sql: CASE WHEN ${status} = 'CONFIRMED' AND (${type} = 'FACEBOOK' OR ${type} = 'INSTAGRAM' OR ${type} = 'TWITTER' OR ${type} = 'LINKEDIN')
      THEN ${profile_id}
      ELSE NULL
      END;;
    drill_fields: [detail*]
  }

  measure: reports_with_big_4_profiles{
    type:  count_distinct
    sql: CASE WHEN ${status} = 'CONFIRMED' AND (${type} = 'FACEBOOK' OR ${type} = 'YOUTUBE' OR ${type} = 'INSTAGRAM' OR ${type} = 'TWITTER' OR ${type} = 'LINKEDIN')
      THEN ${report_id}
      ELSE NULL
      END;;
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      profile_id,
      display_name,
      reports.first_name,
      reports.middle_name,
      reports.last_name,
      reports.report_id,
      posts.count,
      type
    ]
  }
}
