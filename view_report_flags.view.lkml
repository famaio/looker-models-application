
view: view_report_flags {
  derived_table: {
    #persist_for: "6 hours"
    sql_trigger_value:  SELECT COUNT(*) FROM public.post_ratings ;;
    distribution_style: all
    explore_source: companies {
      column: proper_case_name {}
      column: report_id { field: reports.report_id }
      column: alcohol_flag_count { field: post_flags.alcohol_flag_count }
      column: language_flag_count { field: post_flags.language_flag_count }
      column: bigotry_flag_count { field: post_flags.bigotry_flag_count }
      column: cannabis_flag_count { field: post_flags.cannabis_flag_count }
      column: other_drugs_flag_count { field: post_flags.other_drugs_flag_count }

      column: crime_flag_count { field: post_flags.crime_flag_count }
      column: drugs_flag_count { field: post_flags.drugs_flag_count }
      column: sexism_flag_count { field: post_flags.sexism_flag_count }
      column: violence_flag_count { field: post_flags.violence_flag_count }
      column: threat_flag_count { field: post_flags.threat_flag_count }
      column: intolerance_flag_count { field: post_flags.intolerance_flag_count }
      column: harassment_flag_count { field: post_flags.harassment_flag_count }
      column: sex_flag_count { field: post_flags.sex_flag_count }
      column: bad_flag_count { field: post_flags.bad_flag_count }
      column: good_flag_count { field: post_flags.good_flag_count }
      column: keyword_count { field: post_flags.keyword_flag_count }
    }
  }
#   dimension: compound_key {
#     primary_key: yes
#     hidden: yes
#     sql: ${TABLE}.report_id || '-' ||  ${TABLE}.profile_id;;
#   }
  dimension: proper_case_name {}
  dimension: report_id {
    primary_key: yes
  }
  dimension: alcohol_flag_count {
    type: number
  }
  dimension: language_flag_count {
    type: number
  }
  dimension: type {}
  dimension: bigotry_flag_count {
    type: number
  }
  dimension: crime_flag_count {
    type: number
  }
  dimension: cannabis_flag_count {
    type: number
  }
  dimension: drugs_flag_count {
    type: number
  }
  dimension: other_drugs_flag_count {
    type: number
  }
  dimension: violence_flag_count {
    type: number
  }
  dimension: threat_flag_count {
    type: number
  }
  dimension: harassment_flag_count {
    type: number
  }
  dimension: sex_flag_count {
    type: number
  }
  dimension: sexism_flag_count {
    type: number
  }
  dimension: intolerance_flag_count {
    type: number
  }
  dimension: bad_flag_count {
    type: number
  }
  dimension: good_flag_count {
    type: number
  }
  dimension: keyword_count {
    type: number
  }
  dimension: standard_risk {
    type: yesno
    sql: CASE
          WHEN ${bigotry_flag_count} > 0 THEN 1
          WHEN ${alcohol_flag_count} > 40 THEN 1
          WHEN ${crime_flag_count} > 0 THEN 1
          WHEN ${drugs_flag_count} > 2 THEN 1
          WHEN ${language_flag_count} > 15 THEN 1
          WHEN ${sex_flag_count} > 3 THEN 1
          WHEN ${violence_flag_count} > 0 THEN 1
          ELSE 0
        END ;;
  }
  dimension: risk_no_language {
    type: yesno
    sql: CASE
          WHEN ${bigotry_flag_count} > 0 THEN 1
          WHEN ${alcohol_flag_count} > 40 THEN 1
          WHEN ${crime_flag_count} > 0 THEN 1
          WHEN ${drugs_flag_count} > 2 THEN 1
          WHEN ${sex_flag_count} > 3 THEN 1
          WHEN ${violence_flag_count} > 0 THEN 1
          ELSE 0
        END ;;
  }

  measure: total_risk_reports {
    type: count
    filters: {
      field: standard_risk
      value: "yes"
    }
  }

  measure: total_keyword_reports {
    type: count
    filters: {
      field: keyword_count
      value: ">0"
    }
  }
  measure: total_alcohol_reports {
    type: count
    filters: {
      field: alcohol_flag_count
      value: ">0"
    }
  }
  measure: total_bigotry_reports {
    type: count
    filters: {
      field: bigotry_flag_count
      value: ">0"
    }
  }
  measure: total_language_reports {
    type: count
    filters: {
      field: language_flag_count
      value: ">0"
    }
  }
  measure: total_drugs_reports {
    type: count
    filters: {
      field: drugs_flag_count
      value: ">0"
    }
  }
  measure: total_sex_reports {
    type: count
    filters: {
      field: sex_flag_count
      value: ">0"
    }
  }
  measure: total_sexism_reports {
    type: count
    filters: {
      field: sexism_flag_count
      value: ">0"
    }
  }
  measure: total_violence_reports {
    type: count
    filters: {
      field: violence_flag_count
      value: ">0"
    }
  }
  measure: total_crime_reports {
    type: count
    filters: {
      field: crime_flag_count
      value: ">0"
    }
  }
  measure: total_intolerance_reports {
    type: count
    filters: {
      field: intolerance_flag_count
      value: ">0"
    }
  }
  measure: total_bad_flag_reports {
    type: count
    filters: {
      field: bad_flag_count
      value: ">0"
    }
  }

  measure: total_no_language_risk_reports {
    type: count
    filters: {
      field: risk_no_language
      value: "yes"
    }
  }
}
