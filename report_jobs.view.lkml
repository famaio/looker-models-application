view: report_jobs {
  sql_table_name: public.report_jobs ;;

  #Brian Yardley - 12/15/17
  dimension: compound_key {
    primary_key: yes
    hidden: yes
    sql: ${TABLE}.report_id || '-' ||  ${TABLE}.employer || '-' || ${TABLE}.start_date || ${TABLE}.end_date;;
  }

  dimension: employer {
    type: string
    sql: ${TABLE}.employer ;;
  }

  dimension_group: end {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.end_date ;;
  }

  dimension: report_id {
    type: string
    # hidden: yes
    sql: ${TABLE}.report_id ;;
  }

  dimension_group: start {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.start_date ;;
  }

  dimension: title {
    type: string
    sql: ${TABLE}.title ;;
  }

  dimension: senority {
    label: "Senority"
    description: "Categorizes titles into a group."
#     sql: CASE WHEN lower(${title}) IN('chief','principal','president','partner','ceo','cmo','cio','cto','executive','chairman','chro','chief human','cro','chief revenue','cfo','vp','vice','vice president','general manager') THEN 'Executive'
#               WHEN lower(${title}) IN('director','dir.','lead','senior','sr','advanced','head') THEN 'Director'
#               WHEN lower(${title}) IN('mgr','manager','associate') THEN 'Mid Level'
#               WHEN lower(${title}) IN('entry level','junior','generalist','specialist','coordinator') THEN 'Junior'
#               WHEN lower(${title}) LIKE '%consultant%' or lower(${title}) like '%independent%' or lower(${title}) like '%contract%' THEN 'Consultant'
#               ELSE 'Undefined' END ;;

    sql: CASE WHEN
                    (lower(${title}) LIKE '%chief%' or
                    lower(${title}) LIKE '%principal%' or
                    lower(${title}) LIKE '%president%' or
                    lower(${title}) LIKE '%partner%' or
                    lower(${title}) LIKE '%ceo%' or
                    lower(${title}) LIKE '%cmo%' or
                    lower(${title}) LIKE '%cio%' or
                    lower(${title}) LIKE '%cto%' or
                    lower(${title}) LIKE '%executive%' or
                    lower(${title}) LIKE '%chairman%' or
                    lower(${title}) LIKE '%chro%' or
                    lower(${title}) LIKE '%chief human%' or
                    lower(${title}) LIKE '%cro%' or
                    lower(${title}) LIKE '%chief revenue%' or
                    lower(${title}) LIKE '%cfo%' or
                    lower(${title}) LIKE '%vp %' or
                    lower(${title}) LIKE '% vice %' or
                    lower(${title}) LIKE '%vice president%' or
                    lower(${title}) LIKE '%general manager%' or
                    lower(${title}) LIKE '%founder%')
                    AND
                    (lower(${title}) NOT LIKE '%account%' and
                    lower(${title}) NOT LIKE '%director%' and
                    lower(${title}) NOT LIKE '%cheif of staff%') THEN 'Executive'
              WHEN
                    (lower(${title}) LIKE '%director%' or
                    lower(${title}) LIKE '%dir%' or
                    lower(${title}) LIKE '%lead%' or
                    lower(${title}) LIKE '%senior%' or
                    lower(${title}) LIKE '%sr%' or
                    lower(${title}) LIKE '%advanced%' or
                    lower(${title}) LIKE '%head%')
                    AND
                    (lower(${title}) NOT LIKE '%board%' and
                    lower(${title}) NOT LIKE '%vp%' and
                    lower(${title}) NOT LIKE '%executive%' and
                    lower(${title}) NOT LIKE '%chairman%' and
                    lower(${title}) NOT LIKE '%vice president%' and
                    lower(${title}) NOT LIKE '%chief%') THEN 'Director'
              WHEN
                    (lower(${title}) LIKE '%mgr%' or
                    lower(${title}) LIKE '%manager%' or
                    lower(${title}) LIKE '%associate%')  THEN 'Manager'
              WHEN
                    (lower(${title}) LIKE '%entry level%' or
                    lower(${title}) LIKE '%junior%' or
                    lower(${title}) LIKE '%generalist%' or
                    lower(${title}) LIKE '%specialist%' or
                    lower(${title}) LIKE '%coordinator%')
                    AND
                    (lower(${title}) NOT LIKE '%advnaced%') THEN 'Junior'
              WHEN
                    (lower(${title}) LIKE '%consultant%' or
                    lower(${title}) LIKE '%independent%' or
                    lower(${title}) LIKE '%contract%' or
                    lower(${title}) LIKE '%Massage Therapist%') THEN 'Contract'
              ELSE 'Undefined' END ;;
  }

  measure: count {
    type: count
    drill_fields: [reports.last_name, reports.middle_name, reports.report_id, reports.first_name]
  }

  measure: count_distinct {
    type: count_distinct
    sql: ${report_id} ;;
  }

}
