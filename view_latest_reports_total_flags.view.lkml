include: "*.view"

# When dealing with flags, we want to join with the reports_most_recent table to prevent duplicates flag counts.
# This way we only count the flags on the most recent reports.

# This table might not be necessary anymore after re-add candidate changes
view: report_flags {

  extends: [view_latest_reports]

  measure: bad_post_flag_report_count {
    label: "Reports with a Bad Post Flag"
    type: count_distinct
    sql: ${report_id} ;;
    filters: {
      field: post_flags.flag
      value: "LANGUAGE, INTOLERANCE, THREAT, CANNABIS, OTHER_DRUGS HARASSMENT, ALCOHOL, SEX, VIOLENCE, CRIMINAL, BIGOTRY, DRUGS, SEXISM"
    }
  }

  measure: bad_article_flag_report_count {
    label: "Reports with a Bad Article Flag"
    type: count_distinct
    sql: ${report_id} ;;
    filters: {
      field: article_flags.flag
      value: "LANGUAGE, INTOLERANCE, THREAT, CANNABIS, OTHER_DRUGS HARASSMENT, ALCOHOL, SEX, VIOLENCE, CRIMINAL, BIGOTRY, DRUGS, SEXISM"
    }
  }

  measure: bad_flag_reports {
    label: "Reports with a Bad Flag"
    type: number
    sql: ${bad_post_flag_report_count}+${bad_article_flag_report_count} ;;
  }

  measure: good_post_flag_report_count {
    label: "Reports with a Good Post Flag"
    type: count_distinct
    sql: ${report_id} ;;
    filters: {
      field: post_flags.flag
      value: "DONATION, VOLUNTEERING, HELPING_OTHERS, PROTECTING_OTHERS, COMMUNITY"
    }
  }

  measure: good_article_flag_report_count {
    label: "Reports with a Good Article Flag"
    type: count_distinct
    sql: ${report_id} ;;
    filters: {
      field: article_flags.flag
      value: "DONATION, VOLUNTEERING, HELPING_OTHERS, PROTECTING_OTHERS, COMMUNITY"
    }
  }

  measure: good_flag_reports {
    label: "Reports with a Good Flag"
    type: number
    sql: ${good_post_flag_report_count}+${good_article_flag_report_count} ;;
  }

  measure: count_crime_flags {
    label: "Bad - Total Crime Flags"
    type: number
    sql: ${post_flags.crime_flag_count} + ${article_flags.crime_flag_count} ;;
  }

  measure: count_sex_flags {
    label: "Bad - Total Sex Flags"
    type: number
    sql: ${post_flags.sex_flag_count} + ${article_flags.sex_flag_count} ;;
  }

  measure: count_bigotry_flags {
    label: "Bad - Total Bigotry Flags"
    type: number
    sql: ${post_flags.bigotry_flag_count} + ${article_flags.bigotry_flag_count} ;;
  }

  measure: count_drugs_flags {
    label: "Bad - Total Drug Flags"
    type: number
    sql: ${post_flags.drugs_flag_count} + ${article_flags.drugs_flag_count} ;;
  }

  measure: count_violence_flags {
    label: "Bad - Violence Flags"
    type: number
    sql: ${post_flags.violence_flag_count} + ${article_flags.violence_flag_count} ;;
  }

  measure: count_alcohol_flags {
    label: "Bad - Total Alcohol Flags"
    type: number
    sql: ${post_flags.alcohol_flag_count} + ${article_flags.alcohol_flag_count} ;;
  }

  measure: count_harassment_flags {
    label: "Bad - Total Harassment Flags"
    type: number
    sql: ${post_flags.harassment_flag_count} + ${article_flags.harassment_flag_count} ;;
  }

  measure: count_threat_flags {
    label: "Bad - Total Threat Flags"
    type: number
    sql: ${post_flags.threat_flag_count} + ${article_flags.threat_flag_count} ;;
  }

  measure: count_cannabis_flags {
    label: "Bad - Total Cannabis Flags"
    type: number
    sql: ${post_flags.cannabis_flag_count} + ${article_flags.cannabis_flag_count} ;;
  }
  measure: count_other_drugs_flags {
    label: "Bad - Total Other Drugs Flags"
    type: number
    sql: ${post_flags.other_drugs_flag_count} + ${article_flags.other_drugs_flag_count} ;;
  }

  measure: count_language_flags {
    label: "Bad - Total Language Flags"
    type: number
    sql: ${post_flags.language_flag_count} + ${article_flags.language_flag_count} ;;
  }

  measure: count_sexism_flags {
    label: "Bad - Total Sexism Flags"
    type: number
    sql: ${post_flags.sexism_flag_count} + ${article_flags.sexism_flag_count} ;;
  }

  measure: count_intolerance_flags {
    label: "Bad - Total Intolerance Flags"
    type: number
    sql: ${post_flags.intolerance_flag_count} + ${article_flags.intolerance_flag_count} ;;
  }

  measure: count_keyword_flags {
    label: "Keyword - Total Keywords"
    type: number
    sql: ${post_flags.keyword_flag_count} + ${article_flags.keyword_flags_count} ;;
  }

  measure: count_donation_flags {
    label: "Good - Total Donation Flags"
    type: number
    sql: ${post_flags.donation_flag_count} + ${article_flags.donation_flag_count} ;;
  }

  measure: count_volunteering_flags {
    label: "Good - Total Volunteering Flags"
    type: number
    sql: ${post_flags.volunteering_flag_count} + ${article_flags.volunteering_flag_count} ;;
  }

  measure: count_helping_others_flags {
    label: "Good - Total Helping Others Flags"
    type: number
    sql: ${post_flags.helping_others_flag_count} + ${article_flags.helping_others_flag_count} ;;
  }

  measure: count_protecting_others_flags {
    label: "Good - Total Protecting Others Flags"
    type: number
    sql: ${post_flags.protecting_others_flag_count} + ${article_flags.protecting_others_flag_count} ;;
  }

  measure: count_community_flags {
    label: "Good - Total Community Flags"
    type: number
    sql: ${post_flags.community_flag_count} + ${article_flags.community_flag_count} ;;
  }


  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      report_id,
      reports.first_name,
      reports.middle_name,
      reports.last_name
    ]
  }

  set: report_details {
    fields: [
      reports.date_created_time,
      reports.date_finished_time,
      reports.report_run_duration,
      reports.company_id,
      reports.first_name,
      reports.last_name
    ]
  }
}
