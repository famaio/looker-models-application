

view: view_confirm_actions{
  view_label: "Rater Actions - Profile Confirms"
  derived_table: {
    sql_trigger_value:  SELECT COUNT(*) FROM public.rater_actions ;;
    distribution_style: all
    explore_source: rater_actions {
      column: report_id {field: rater_actions.action_item}
      column: action_type {}
      column: start_time {field: rater_actions.start_raw}
      column: end_time {field: rater_actions.end_raw}
      column: duration {field: rater_actions.duration}
      #column: duration_minutes {field: rater_actions.duration_minutes}
      column: rater_id {}
      column: username {}
      column: masked_username {}
      filters: {
        field: rater_actions.action_type
        value: "MANUAL^_PERSON^_CONFIRM, PERSON^_MANUAL^_CONFIRM"
      }
    }
  }

  dimension: compound_primary_key {
    primary_key: yes
    hidden: yes
    sql: ${TABLE}.report_id || ${TABLE}.duration ;;
  }
  dimension: report_id {
    #primary_key: yes
  }
  dimension: action_type {}
  dimension: duration {
    type: number
  }
  dimension: duration_minutes {
    type: number
    sql: round(${duration}/60,2) ;;
    value_format_name: "decimal_2"
  }
  dimension: rater_id {}

  dimension: username {
    type: string
    sql: ${TABLE}.username ;;
  }
  measure: username_count {
    type: count

  }

  dimension: team {
    type: string
    sql:

    CASE WHEN ${TABLE}.username in ('yohandra','samir-s','michelle','julied','niaroane',
    'mark-coco','angel','georgios', 'val', 'valscrape','favy','alex','jessica','dorga','david','james','tracy')
    THEN 'in-house'
        WHEN ${TABLE}.username ilike '%os%'
    THEN 'AOS'
        WHEN ${TABLE}.username ilike '%jenald2%'
    THEN 'AOS'
        WHEN ${TABLE}.username ilike '%cf%'
    THEN 'CF'
    END;;
  }

  measure: inhouse_count {
    label: "Total in-house PC count"
    type: count
    filters: {field: username
      value: "in-house"}
  }

  dimension: masked_username {}

  dimension_group: start {
    type: time
    timeframes: [
      raw,
      time,
      date,
      day_of_week,
      week,
      month,
      quarter,
      year,
      hour_of_day,
      hour
    ]
    sql: ${TABLE}.start_time ;;
  }

  dimension_group: end {
    type: time
    timeframes: [
      raw,
      time,
      date,
      day_of_week,
      week,
      month,
      quarter,
      year,
      hour_of_day,
      hour
    ]
    sql: ${TABLE}.end_time ;;
  }

  measure: count {
    type: count
  }

  measure: count_distinct {
    type: count_distinct
    sql: ${report_id} ;;
  }

  measure: total_raters {
    label: "Total Number of Raters"
    type: count_distinct
    sql: ${username} ;;
  }

  measure: profile_confirm_count {
    label: "Total Profile Confirm Tasks Completed"
    description: "Profile Confirm Count counts completed profile confirms.  End date is not null."
    type: count
    filters: {
      field: end_time
      value: "-null"
    }
  }

  measure: confirms_started_count {
    label: "Total Profile Confirm Tasks Started"
    description: "Counts all tasks where Start Time is not null."
    type: count
    filters: {
      field: start_time
      value: "-null"
    }
  }

  measure: average_duration_seconds {
    label: "Average Duration - Seconds"
    type: average
    sql: ${duration} ;;
    filters: {
      field: end_time
      value: "-null"
    }
    value_format_name: "decimal_2"
  }

#   measure: median_duration {
#     label: "Median Duration - Seconds"
#     type: median
#     sql: ${duration} ;;
#     value_format_name: "decimal_2"
#     filters: {
#       field: end_date
#       value: "-null"
#     }
#   }

  measure: average_duration_minutes {
    label: "Average Duration - Minutes"
    description: "Use Average Duration - Seconds for a more accurate measure"
    type: average
    sql: ${duration_minutes} ;;
    filters: {
      field: end_time
      value: "-null"
    }
    value_format_name: "decimal_2"
  }

  measure: sum_duration_seconds {
    label: "Sum Duration - Seconds"
    type:  sum
    sql: ${duration} ;;
    filters: {
      field: end_time
      value: "-null"
    }
    value_format_name: "decimal_2"
  }
}
