view: article_flags {
  sql_table_name: public.article_flags ;;

  dimension: pk {
    primary_key: yes
    hidden: yes
    type: string
    sql: ${article_id} || ${flag} || ${flag_type} ;;
  }

  dimension: article_id {
    type: string
    sql: ${TABLE}.article_id ;;
  }

  dimension: flag {
    type: string
    sql: ${TABLE}.flag ;;
  }

  dimension: flag_type {
    type: string
    sql: ${TABLE}.flag_type ;;
  }

  measure: count {
    type: count
    drill_fields: [articles.article_id]
  }

  measure: count_distinct {
    type: count_distinct
    sql: ${article_id} ;;
  }

  measure: sex_flag_count {
    type: count
    filters: {
      field: flag
      value: "SEX"
    }
    filters: {
      field: flag_type
      value: "SENSITIVITY"
    }
  }

  measure: bigotry_flag_count {
    type: count
    filters: {
      field: flag
      value: "BIGOTRY"
    }
    filters: {
      field: flag_type
      value: "SENSITIVITY"
    }
  }

  measure: drugs_flag_count {
    type: count
    filters: {
      field: flag
      value: "DRUGS"
    }
    filters: {
      field: flag_type
      value: "SENSITIVITY"
    }
  }

  measure: violence_flag_count {
    type: count
    filters: {
      field: flag
      value: "VIOLENCE"
    }
    filters: {
      field: flag_type
      value: "SENSITIVITY"
    }
  }

  measure: crime_flag_count {
    type: count
    filters: {
      field: flag
      value: "CRIMINAL"
    }
    filters: {
      field: flag_type
      value: "SENSITIVITY"
    }
  }

  measure: alcohol_flag_count {
    type: count
    filters: {
      field: flag
      value: "ALCOHOL"
    }
    filters: {
      field: flag_type
      value: "SENSITIVITY"
    }
  }

  measure: language_flag_count {
    type: count
    filters: {
      field: flag
      value: "LANGUAGE"
    }
    filters: {
      field: flag_type
      value: "SENSITIVITY"
    }
  }

  measure: sexism_flag_count {
    type: count
    filters: {
      field: flag
      value: "SEXISM"
    }
    filters: {
      field: flag_type
      value: "SENSITIVITY"
    }
  }

  measure: harassment_flag_count {
    type: count
    filters: {
      field: flag
      value: "HARASSMENT"
    }
    filters: {
      field: flag_type
      value: "SENSITIVITY"
    }
  }

  measure: threat_flag_count {
    type: count
    filters: {
      field: flag
      value: "THREAT"
    }
    filters: {
      field: flag_type
      value: "SENSITIVITY"
    }
  }
  measure: other_drugs_flag_count {
    type: count
    filters: {
      field: flag
      value: "OTHER_DRUGS"
    }
    filters: {
      field: flag_type
      value: "SENSITIVITY"
    }
  }
  measure: cannabis_flag_count {
    type: count
    filters: {
      field: flag
      value: "CANNABIS"
    }
    filters: {
      field: flag_type
      value: "SENSITIVITY"
    }
  }

  measure: intolerance_flag_count {
    type: count
    filters: {
      field: flag
      value: "INTOLERANCE"
    }
    filters: {
      field: flag_type
      value: "SENSITIVITY"
    }
  }

  measure: community_flag_count {
    type: count
    filters: {
      field: flag
      value: "COMMUNITY"
    }
    filters: {
      field: flag_type
      value: "SENSITIVITY"
    }
  }

  measure: donation_flag_count {
    type: count
    filters: {
      field: flag
      value: "DONATION"
    }
    filters: {
      field: flag_type
      value: "SENSITIVITY"
    }
  }

  measure: volunteering_flag_count {
    type: count
    filters: {
      field: flag
      value: "VOLUNTEERING"
    }
    filters: {
      field: flag_type
      value: "SENSITIVITY"
    }
  }

  measure: helping_others_flag_count {
    type: count
    filters: {
      field: flag
      value: "HELPING_OTHERS"
    }
    filters: {
      field: flag_type
      value: "SENSITIVITY"
    }
  }

  measure: protecting_others_flag_count {
    type: count
    filters: {
      field: flag
      value: "PROTECTING_OTHERS"
    }
    filters: {
      field: flag_type
      value: "SENSITIVITY"
    }
  }
  #Jordan Lincenberg created on 12/14/17
  measure: bad_flags_count {
    type: count
    filters: {
      field: flag
      value: "LANGUAGE, HARASSMENT, THREAT, OTHER_DRUGS, CANNABIS, ALCOHOL, SEX, VIOLENCE, CRIMINAL, BIGORTY, DRUGS, SEXISM"
    }
  }

  measure: bad_flags_article_count {
    label: "Bad Flagged Article Count"
    type: count_distinct
    sql: ${article_id} ;;
    filters: {
      field: flag
      value: "LANGUAGE, HARASSMENT, THREAT, OTHER_DRUGS, CANNABIS, ALCOHOL, SEX, VIOLENCE, CRIMINAL, BIGORTY, DRUGS, SEXISM"
    }
  }

  measure: article_bad_flags_count {
    type: count_distinct
    sql: ${article_id} ;;
    filters: {
      field: flag
      value: "LANGUAGE, HARASSMENT, THREAT, OTHER_DRUGS, CANNABIS, ALCOHOL, SEX, VIOLENCE, CRIMINAL, BIGORTY, DRUGS, SEXISM"
    }
  }

  #Jordan Lincenberg created on 12/14/17
  measure: good_flags_count {
    type: count
    filters: {
      field: flag
      value: "DONATION, VOLUNTEERING, HELPING_OTHERS, PROTECTING_OTHERS"
    }
  }

  #Jordan Lincenberg created on 12/14/17
  measure: keyword_flags_count {
    type: count
    filters: {
      field: flag_type
      value: "KEYWORD, STEMMABLE_KEYWORD"
    }
  }
}
