view: report_bad_facebook_twitter {
  # Or, you could make this view a derived table, like this:
  derived_table: {
    datagroup_trigger: etl_trigger
    distribution_style: all
    sql: SELECT
          reports.report_id  AS "report_id",
          COUNT(DISTINCT CASE WHEN (post_flags.flag = 'LANGUAGE' OR post_flags.flag = 'ALCOHOL' OR post_flags.flag = 'SEX' OR post_flags.flag = 'VIOLENCE' OR post_flags.flag = 'CRIMINAL' OR post_flags.flag = 'BIGORTY' OR post_flags.flag = 'DRUGS') AND (profiles.type = 'TWITTER') THEN post_flags.post_id  ELSE NULL END) AS "post_flags.twitter_bad_flag_count",
          COUNT(DISTINCT CASE WHEN (post_flags.flag = 'LANGUAGE' OR post_flags.flag = 'ALCOHOL' OR post_flags.flag = 'SEX' OR post_flags.flag = 'VIOLENCE' OR post_flags.flag = 'CRIMINAL' OR post_flags.flag = 'BIGORTY' OR post_flags.flag = 'DRUGS') AND (profiles.type = 'FACEBOOK') THEN post_flags.post_id  ELSE NULL END) AS "post_flags.facebook_bad_flag_count",
          1 as "has_bad_facebook_and_twitter"
        FROM public.reports  AS reports
        LEFT JOIN public.profiles  AS profiles ON profiles.report_id = reports.report_id
        LEFT JOIN public.posts  AS posts ON posts.profile_id = profiles.profile_id
        LEFT JOIN public.post_ratings  AS post_ratings ON post_ratings.post_id = posts.post_id
        LEFT JOIN public.post_flags  AS post_flags ON post_flags.post_id = post_ratings.post_id
        LEFT JOIN public.companies  AS companies ON reports.company_id = companies.company_id

        WHERE (profiles.type = 'FACEBOOK' OR profiles.type = 'TWITTER')
        GROUP BY 1
        HAVING ((COUNT(DISTINCT CASE WHEN (post_flags.flag = 'LANGUAGE' OR post_flags.flag = 'ALCOHOL' OR post_flags.flag = 'SEX' OR post_flags.flag = 'VIOLENCE' OR post_flags.flag = 'CRIMINAL' OR post_flags.flag = 'BIGORTY' OR post_flags.flag = 'DRUGS') THEN post_flags.post_id  ELSE NULL END) > 0)) AND ((COUNT(DISTINCT CASE WHEN (post_flags.flag = 'LANGUAGE' OR post_flags.flag = 'ALCOHOL' OR post_flags.flag = 'SEX' OR post_flags.flag = 'VIOLENCE' OR post_flags.flag = 'CRIMINAL' OR post_flags.flag = 'BIGORTY' OR post_flags.flag = 'DRUGS') AND (profiles.type = 'TWITTER') THEN post_flags.post_id  ELSE NULL END) > 0)) AND ((COUNT(DISTINCT CASE WHEN (post_flags.flag = 'LANGUAGE' OR post_flags.flag = 'ALCOHOL' OR post_flags.flag = 'SEX' OR post_flags.flag = 'VIOLENCE' OR post_flags.flag = 'CRIMINAL' OR post_flags.flag = 'BIGORTY' OR post_flags.flag = 'DRUGS') AND (profiles.type = 'FACEBOOK') THEN post_flags.post_id  ELSE NULL END) > 0))
            ;;
  }
  dimension: report_id {
    type: number
    primary_key: yes
    sql: ${TABLE}.report_id ;;
  }
  dimension: has_bad_facebook_and_twitter {
    type: yesno
    sql: ${TABLE}.has_bad_facebook_and_twitter ;;
  }

  measure: report_count {
    label: "Reports with a Bad Flag on Facebook and Twitter"
    type:  count
    filters: {
      field: has_bad_facebook_and_twitter
      value: "yes"
    }
  }
#   dimension: count {}
#   dimension: count_distinct_type {}
}
