# If necessary, uncomment the line below to include explore_source.
# include: "application.model.lkml"

view: view_profile_flags_metrics {
  derived_table: {
    # trigger the rebuild of this table when the post_ratings table has updated. it is the last major table in the ETL update process.
    sql_trigger_value:  SELECT COUNT(*) FROM ${view_profile_flags.SQL_TABLE_NAME} ;;
    distribution_style: all
    explore_source: view_profile_flags {
      column: report_id {}
      column: profiles_with_bad_flag {}
      column: facebook_profiles_bad_flags {}
      column: instagram_profiles_bad_flags {}
      column: twitter_profiles_bad_flags {}
      column: facebook_profiles_alcohol_flags {}
      column: facebook_profiles_bigotry_flags {}
      column: facebook_profiles_drugs_flags {}
      column: facebook_profiles_language_flags {}
      column: facebook_profiles_sex_flags {}
      column: facebook_profiles_crime_flags {}
      column: facebook_profiles_violence_flags {}
      column: twitter_profiles_alcohol_flags {}
      column: twitter_profiles_bigotry_flags {}
      column: twitter_profiles_crime_flags {}
      column: twitter_profiles_drugs_flags {}
      column: twitter_profiles_language_flags {}
      column: twitter_profiles_sex_flags {}
      column: twitter_profiles_violence_flags {}
      column: profiles_with_alcohol_flag {}
      column: profiles_with_bigotry_flag {}
      column: profiles_with_crime_flag {}
      column: profiles_with_drugs_flag {}
      column: profiles_with_language_flag {}
      column: profiles_with_sex_flag {}
      column: profiles_with_violence_flag {}

      filters: {
        field: view_profile_flags.proper_case_name
        value: ""
      }
      filters: {
        field: view_profile_flags.report_id
        value: ""
      }
    }
  }
  dimension: report_id {
    primary_key: yes
  }
  dimension: profiles_with_bad_flag {
    label: "Bad Flags - Profiles"
    type: number
  }
  dimension: facebook_profiles_bad_flags {
    label: "Bad Flags - Facebook Profiles"
    type: number
  }
  dimension: instagram_profiles_bad_flags {
    label: "Bad Flags - Instagram Profiles"
    type: number
  }
  dimension: twitter_profiles_bad_flags {
    label: "Bad Flags - Twitter Profiles"
    type: number
  }
  dimension: facebook_profiles_alcohol_flags {
    label: "Facebook - Alcohol Flags"
    type: number
  }
  dimension: facebook_profiles_bigotry_flags {
    label: "Facebook - Bigotry Flags"
    type: number
  }
  dimension: facebook_profiles_drugs_flags {
    label: "Facebook - Drugs Flags"
    type: number
  }
  dimension: facebook_profiles_language_flags {
    label: "Facebook - Language Flags"
    type: number
  }
  dimension: facebook_profiles_sex_flags {
    label: "Facebook - Sex Flags"
    type: number
  }
  dimension: facebook_profiles_crime_flags {
    label: "Facebook - Crime Flags"
    type: number
  }
  dimension: facebook_profiles_violence_flags {
    label: "Facebook - Violence Flags"
    type: number
  }
  dimension: twitter_profiles_alcohol_flags {
    label: "View Profile Flags Twitter - Alcohol Flags"
    type: number
  }
  dimension: twitter_profiles_bigotry_flags {
    label: "Twitter - Bigotry Flags"
    type: number
  }
  dimension: twitter_profiles_crime_flags {
    label: "Twitter - Crime Flags"
    type: number
  }
  dimension: twitter_profiles_drugs_flags {
    label: "Twitter - Drugs Flags"
    type: number
  }
  dimension: twitter_profiles_language_flags {
    label: "Twitter - Language Flags"
    type: number
  }
  dimension: twitter_profiles_sex_flags {
    label: "Twitter - Sex Flags"
    type: number
  }
  dimension: twitter_profiles_violence_flags {
    label: "Twitter - Violence Flags"
    type: number
  }
  dimension: profiles_with_alcohol_flag {
    label: "Bad Flags - Alcohol Profiles"
    type: number
  }
  dimension: profiles_with_bigotry_flag {
    label: "Bad Flags - Bigotry Profiles"
    type: number
  }
  dimension: profiles_with_crime_flag {
    label: "Bad Flags - Crime Profiles"
    type: number
  }
  dimension: profiles_with_drugs_flag {
    label: "Bad Flags - Drugs Profiles"
    type: number
  }
  dimension: profiles_with_language_flag {
    label: "Bad Flags - Language Profiles"
    type: number
  }
  dimension: profiles_with_sex_flag {
    label: "Bad Flags - Sex Profiles"
    type: number
  }
  dimension: profiles_with_violence_flag {
    label: "Bad Flags - Violence Profiles"
    type: number
  }


  measure: has_facebook_flag_no_twitter_flag {
    label: "Reports - FB Flag No Twitter Flag"
    type: count
    filters: {
      field: facebook_profiles_bad_flags
      value: ">0"
    }
    filters: {
      field: twitter_profiles_bad_flags
      value: "0"
    }
  }

  measure: has_twitter_flag_no_facebook_flag {
    label: "Reports - Twiiter Flag No Facebook Flag"
    type: count
    filters: {
      field: facebook_profiles_bad_flags
      value: "0"
    }
    filters: {
      field: twitter_profiles_bad_flags
      value: ">0"
    }
  }

  measure: has_facebook_flag_an_twitter_flag {
    label: "Reports - FB Flag and Twitter Flag"
    type: count
    filters: {
      field: facebook_profiles_bad_flags
      value: ">0"
    }
    filters: {
      field: twitter_profiles_bad_flags
      value: ">0"
    }
  }

  measure: has_facebook_flag_no_flag_other {
    label: "Reports - FB Flag No Flag Other Profiles"
    type: count
    filters: {
      field: facebook_profiles_bad_flags
      value: ">0"
    }
    filters: {
      field: twitter_profiles_bad_flags
      value: "0"
    }
    filters: {
      field: instagram_profiles_bad_flags
      value: "0"
    }
  }

  measure: has_twitter_flag_no_flag_other {
    label: "Reports - Twitter Flag No Flag Other Profiles"
    type: count
    filters: {
      field: facebook_profiles_bad_flags
      value: "0"
    }
    filters: {
      field: twitter_profiles_bad_flags
      value: ">0"
    }
    filters: {
      field: instagram_profiles_bad_flags
      value: "0"
    }
  }

  measure: has_twitter_flag {
    label: "Reports - Twitter Bad Flag"
    type: count
    filters: {
      field: twitter_profiles_bad_flags
      value: ">0"
    }
  }

  measure: has_facebook_flag {
    label: "Reports - Facebook Bad Flag"
    type: count
    filters: {
      field: facebook_profiles_bad_flags
      value: ">0"
    }
  }

  measure: has_instagram_flag {
    label: "Reports - Instagram Bad Flag"
    type: count
    filters: {
      field: instagram_profiles_bad_flags
      value: ">0"
    }
  }

  measure: has_bad_flag {
    label: "Reports - Bad Flag"
    type: count
    filters: {
      field: profiles_with_bad_flag
      value: ">0"
    }
  }

  measure: has_fb_alcohol_no_twitter{
    label: "1 - Alcohol - FB Not Twitter"
    type: count
    filters: {
      field: facebook_profiles_alcohol_flags
      value: ">0"
    }
    filters: {
      field: twitter_profiles_alcohol_flags
      value: "0"
    }
  }

  measure: has_twitter_alcohol_no_facebook{
    label: "1 - Alcohol - T Not FB"
    type: count
    filters: {
      field: facebook_profiles_alcohol_flags
      value: "0"
    }
    filters: {
      field: twitter_profiles_alcohol_flags
      value: ">0"
    }
  }

  measure: has_fb_bigotry_no_twitter{
    label: "1 - Bigotry - FB Not Twitter"
    type: count
    filters: {
      field: facebook_profiles_bigotry_flags
      value: ">0"
    }
    filters: {
      field: twitter_profiles_bigotry_flags
      value: "0"
    }
  }

  measure: has_twitter_bigotry_no_facebook{
    label: "1 - Bigotry - T Not FB"
    type: count
    filters: {
      field: facebook_profiles_bigotry_flags
      value: "0"
    }
    filters: {
      field: twitter_profiles_bigotry_flags
      value: ">0"
    }
  }

  measure: has_fb_language_no_twitter{
    label: "1 - Language - FB Not Twitter"
    type: count
    filters: {
      field: facebook_profiles_language_flags
      value: ">0"
    }
    filters: {
      field: twitter_profiles_language_flags
      value: "0"
    }
  }

  measure: has_twitter_language_no_facebook{
    label: "1 - Language - T Not FB"
    type: count
    filters: {
      field: facebook_profiles_language_flags
      value: "0"
    }
    filters: {
      field: twitter_profiles_language_flags
      value: ">0"
    }
  }

  measure: has_fb_drugs_no_twitter{
    label: "1 - Drugs - FB Not Twitter"
    type: count
    filters: {
      field: facebook_profiles_drugs_flags
      value: ">0"
    }
    filters: {
      field: twitter_profiles_drugs_flags
      value: "0"
    }
  }

  measure: has_twitter_drugs_no_facebook{
    label: "1 - Drugs - T Not FB"
    type: count
    filters: {
      field: facebook_profiles_drugs_flags
      value: "0"
    }
    filters: {
      field: twitter_profiles_drugs_flags
      value: ">0"
    }
  }

  measure: has_fb_violence_no_twitter{
    label: "1 - Violence - FB Not Twitter"
    type: count
    filters: {
      field: facebook_profiles_violence_flags
      value: ">0"
    }
    filters: {
      field: twitter_profiles_violence_flags
      value: "0"
    }
  }

  measure: has_twitter_violence_no_facebook{
    label: "1 - Violence - T Not FB"
    type: count
    filters: {
      field: facebook_profiles_violence_flags
      value: "0"
    }
    filters: {
      field: twitter_profiles_violence_flags
      value: ">0"
    }
  }

  measure: has_fb_sex_no_twitter{
    label: "1 - Sex - FB Not Twitter"
    type: count
    filters: {
      field: facebook_profiles_sex_flags
      value: ">0"
    }
    filters: {
      field: twitter_profiles_sex_flags
      value: "0"
    }
  }

  measure: has_twitter_sex_no_facebook{
    label: "1 - Sex - T Not FB"
    type: count
    filters: {
      field: facebook_profiles_sex_flags
      value: "0"
    }
    filters: {
      field: twitter_profiles_sex_flags
      value: ">0"
    }
  }

  measure: has_fb_crime_no_twitter{
    label: "1 - Crime - FB Not Twitter"
    type: count
    filters: {
      field: facebook_profiles_crime_flags
      value: ">0"
    }
    filters: {
      field: twitter_profiles_crime_flags
      value: "0"
    }
  }

  measure: has_twitter_crime_no_facebook{
    label: "1 - Crime - T Not FB"
    type: count
    filters: {
      field: facebook_profiles_crime_flags
      value: "0"
    }
    filters: {
      field: twitter_profiles_crime_flags
      value: ">0"
    }
  }

  measure: has_alcohol_flag {
    label: "2 - Reports - Alcohol Bad Flag"
    type: count
    filters: {
      field: profiles_with_alcohol_flag
      value: ">0"
    }
  }
  measure: has_bigotry_flag {
    label: "2 - Reports - Bigotry Bad Flag"
    type: count
    filters: {
      field: profiles_with_bigotry_flag
      value: ">0"
    }
  }
  measure: has_language_flag {
    label: "2 - Reports - Language Bad Flag"
    type: count
    filters: {
      field: profiles_with_language_flag
      value: ">0"
    }
  }
  measure: has_sex_flag {
    label: "2 - Reports - Sex Bad Flag"
    type: count
    filters: {
      field: profiles_with_sex_flag
      value: ">0"
    }
  }
  measure: has_violence_flag {
    label: "2 - Reports - Violence Bad Flag"
    type: count
    filters: {
      field: profiles_with_violence_flag
      value: ">0"
    }
  }
  measure: has_drugs_flag {
    label: "2 - Reports - Drugs Bad Flag"
    type: count
    filters: {
      field: profiles_with_drugs_flag
      value: ">0"
    }
  }
  measure: has_crime_flag {
    label: "2 - Reports - Crime Bad Flag"
    type: count
    filters: {
      field: profiles_with_crime_flag
      value: ">0"
    }
  }
}
