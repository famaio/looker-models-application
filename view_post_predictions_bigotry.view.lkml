# If necessary, uncomment the line below to include explore_source.
# include: "application.model.lkml"

view: view_post_predictions_bigotry {
  label: "Post Prediction - Bigotry"
  derived_table: {
    sql_trigger_value:  SELECT COUNT(*) FROM public.predictions ;;
    distribution_style: all
    explore_source: predictions {
      column: post_id {field:predictions.content_id}
      column: bigotry_value {field:predictions.value}
      filters: {
        field: predictions.prediction_type
        value: "BIGOTRY"
      }
      filters: {
        field: predictions.model_type
        value: "POST,POST^_IMAGE"
      }
    }
  }
  dimension: post_id {
    primary_key: yes
  }
  dimension: bigotry_value {
    type: number
  }
}
