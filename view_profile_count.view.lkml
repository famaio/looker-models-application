# If necessary, uncomment the line below to include explore_source.
# include: "application.model.lkml"

view: view_profile_count {
  derived_table: {
    # trigger the rebuild of this table when the post_ratings table has updated. it is the last major table in the ETL update process.
    datagroup_trigger: etl_trigger
    distribution_style: all
    explore_source: companies {
      column: proper_case_name {}
      column: report_id { field: reports.report_id }
      column: facebook_count { field: profiles.facebook_count }
      column: twitter_count { field: profiles.twitter_count }
      column: instagram_count { field: profiles.instagram_count }
    }
  }
  dimension: proper_case_name {}
  dimension: report_id {
    primary_key: yes
  }
  dimension: facebook_count {
    type: number
  }
  dimension: twitter_count {
    type: number
  }
  dimension: instagram_count {
    type: number
  }
  measure:  reports_without_twitter{
    type: count
    filters: {
      field: twitter_count
      value: "0"
    }
  }
  measure:  reports_without_facebook{
    type: count
    filters: {
      field: facebook_count
      value: "0"
    }
  }
  measure:  reports_without_instagram{
    type: count
    filters: {
      field: instagram_count
      value: "0"
    }
  }
  measure:  reports_with_twitter_no_facebook{
    label: "Reports with Twitter and No Facebook"
    type: count
    filters: {
      field: twitter_count
      value: ">0"
    }
    filters: {
      field: facebook_count
      value: "0"
    }
  }
  measure:  reports_with_twitter_and_facebook{
    label: "Reports with Twitter and Facebook"
    type: count
    filters: {
      field: twitter_count
      value: ">0"
    }
    filters: {
      field: facebook_count
      value: ">0"
    }
  }
  measure:  reports_with_facebook_no_twitter{
    label: "Reports with Facebook and no Twitter"
    type: count
    filters: {
      field: twitter_count
      value: "0"
    }
    filters: {
      field: facebook_count
      value: ">0"
    }
  }
}
