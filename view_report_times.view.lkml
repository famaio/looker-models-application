

view: view_report_times {
  derived_table: {
    explore_source: companies {
      column: proper_case_name {}
      column: report_id { field: reports.report_id }
      column: report_start_date { field: reports.date_created_date }
      column: report_end_date { field: reports.date_finished_date }
      column: report_created_time { field: reports.date_created_time }
      column: report_confirm_start_time { field: view_confirm_actions.start_time }
      column: report_confirm_end_time { field: view_confirm_actions.end_time }
      column: report_qa_start_time { field: view_qa_actions.start_time }
      column: report_finished_time { field: reports.date_finished_time }
      filters: {
        field: view_confirm_actions.start_time
        value: "NOT NULL"
      }
      filters: {
        field: view_confirm_actions.end_time
        value: "NOT NULL"
      }
      filters: {
        field: view_qa_actions.start_time
        value: "NOT NULL"
      }
    }
  }
  dimension: proper_case_name {
    label: "Company Name"
  }
  dimension: report_id {
    label: "Report ID"
    primary_key: yes
  }
  dimension: report_start_date {
    label: "Report Start Date"
    type: date
  }
  dimension: report_end_date {
    label: "Report Complete Date"
    type: date
  }
  dimension: report_created_time {
    label: "Report Created Time"
    type: date_time
  }
  dimension: report_confirm_start_time {
    label: "Confirm Start Time"
    type: date_time
  }
  dimension: report_confirm_end_time {
    label: "Confirm End Time"
    type: date_time
  }
  dimension: report_qa_start_time {
    label: "QA Start Time"
    type: date_time
  }
  dimension: report_finished_time {
    label: "Report Finshed Time"
    type: date_time
  }
}
