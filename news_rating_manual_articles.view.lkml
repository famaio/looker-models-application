

view: news_rating_manual_articles {

  derived_table: {
    distribution_style: all
    sql: SELECT a.report_id, a.article_id, foo.url, r.date_finished, r.first_name, r.last_name, foo.probability, aq.data_type, foo.relevant, 1 as missed_flag
    FROM
      (
      SELECT a.url, a.probability, a.relevant
      FROM articles a
      JOIN article_queries aq on aq.article_id = a.article_id
      WHERE a.deleted is false
      AND a.hidden is false
      AND
        ((a.probability >= 0.5 AND a.relevant = FALSE) OR
        (a.probability < 0.5 AND a.relevant = TRUE))
      ) foo
    JOIN articles a on foo.url = a.url
    JOIN reports r on r.report_id = a.report_id
    JOIN article_queries aq on aq.article_id = a.article_id
    --filter out news feature company (homeadvisor because they didn't get manually added
    WHERE r.company_id != '7fbc65b1-c0e0-11e7-ae9a-0decbd7d5a26'
    --where r.date_finished >= '2017-10-26 00:00:00'
    --ORDER BY r.last_name
      ;;
    # trigger the rebuild of this table when the post_ratings table has updated. it is the last major table in the ETL update process.
    sql_trigger_value:  SELECT COUNT(*) FROM public.post_ratings ;;
  }

  dimension: report_id {
    type: string
    sql: ${TABLE}.report_id ;;
  }

  dimension: article_id {
    primary_key: yes
    type: string
    sql: ${TABLE}.article_id ;;
  }

  # Define your dimensions and measures here, like this:
  dimension: url {
    type: string
    sql: ${TABLE}.url ;;
  }

  dimension: first_name {
    type: string
    sql: ${TABLE}.first_name ;;
  }

  dimension: last_name {
    type: string
    sql: ${TABLE}.last_name ;;
  }

  dimension: missed_flag {
    type: yesno
    sql: ${TABLE}.missed_flag ;;
  }

  dimension: relevant {
    type: yesno
    sql: ${TABLE}.relevant ;;
  }

  dimension_group: date_finished {
  type: time
  timeframes: [
    raw,
    time,
    date,
    week,
    month,
    quarter,
    year
  ]
  sql: ${TABLE}.date_finished ;;
  }

  dimension: probability {
    type: number
    sql: ${TABLE}.probability ;;
  }

  dimension: data_type {
    type: string
    sql: ${TABLE}.data_type ;;
  }

  dimension: probablility_tiers {
    type: tier
    tiers: [-4,-2,0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8]
    style: classic
    sql: ${TABLE}.probability ;;
  }

  measure: count {
    type: count
  }

  measure: missed_count {
    type: count
    filters: {
      field: missed_flag
      value: "yes"
      }
  }
}
