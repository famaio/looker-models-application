view: report_addresses {
  sql_table_name: public.report_addresses ;;

#Brian Yardley - 12/15/17
  dimension: compound_key {
    primary_key: yes
    hidden: yes
    sql: ${TABLE}.report_id || '-' ||  ${TABLE}.line_1;;
  }

  dimension: city {
    type: string
    sql: ${TABLE}.city ;;
  }

  dimension: country {
    type: string
    map_layer_name: countries
    sql: ${TABLE}.country ;;
  }

  dimension: united_states_report {
    label: "US Report"
    type: yesno
    sql: CASE WHEN lower(${country}) LIKE '%states%' or
                   lower(${country}) LIKE 'usa' or
                   lower(${country}) LIKE 'us'
                   THEN true
                   ELSE false END;;
  }

  dimension_group: end {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.end_date ;;
  }

  dimension: line_1 {
    type: string
    sql: ${TABLE}.line_1 ;;
  }

  dimension: line_2 {
    type: string
    sql: ${TABLE}.line_2 ;;
  }

  dimension: full_address {
    type: string
    sql: ${line_1} || ' '|| ${line_2} ;;
  }

  dimension: report_id {
    type: string
    # hidden: yes
    sql: ${TABLE}.report_id ;;
  }

  dimension_group: start {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.start_date ;;
  }

  dimension: state {
    type: string
    sql: ${TABLE}.state ;;
  }

  measure: count {
    type: count
    drill_fields: [reports.last_name, reports.middle_name, reports.report_id, reports.first_name]
  }
}
