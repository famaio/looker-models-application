view: view_post_actions{
#   view_label: "Z"
  derived_table: {
    sql_trigger_value:  SELECT COUNT(*) FROM public.rater_actions ;;
    distribution_style: all
    explore_source: rater_actions {
      column: post_id {field: rater_actions.action_item}
      column: action_type {}
      column: start_time {field: rater_actions.start_raw}
      column: end_time {field: rater_actions.end_raw}
      column: duration {field: rater_actions.duration}
      column: duration_minutes {field: rater_actions.duration_minutes}
      column: rater_id {}
      column: username {}
      column: masked_username {}
      filters: {
        field: rater_actions.action_type
        value: "MANUAL^_POST^_RATE, POST^_MANUAL^_RATE"
      }
    }
  }
  dimension: compound_primary_key {
    primary_key: yes
    hidden: yes
    sql: ${TABLE}.post_id || ${TABLE}.duration ;;
  }
  dimension: post_id {}
  dimension: action_type {}
  dimension: duration {
    type: number
  }
  dimension: duration_minutes {
    type: number
  }
  dimension: rater_id {}
  dimension: username {}
  dimension: masked_username {}
#   dimension: start_time {}
#   dimension: end_time {}

  dimension: team {
    type: string
    sql:

    CASE WHEN ${TABLE}.username in ('yohandra','samir-s','michelle','julied','niaroane',
    'mark-coco','angel','georgios','favy','alex','jessica','dorga','david','james','tracy')
    THEN 'in-house'
        WHEN ${TABLE}.username ilike '%os%'
    THEN 'AOS'
    WHEN ${TABLE}.username ilike '%jenald2%'
    THEN 'AOS'
    WHEN ${TABLE}.username ilike '%cf%'
    THEN 'CF'
    END;;
  }

  measure: inhouse_count {
    label: "Total in-house PR count"
    type: count
    filters: {field: username
      value: "in-house"}
  }


  dimension_group: start {
    type: time
    timeframes: [
      raw,
      time,
      date,
      day_of_week,
      week,
      month,
      quarter,
      year,
      hour_of_day,
      hour
    ]
    sql: ${TABLE}.start_time ;;
  }

  dimension_group: end {
    type: time
    timeframes: [
      raw,
      time,
      date,
      day_of_week,
      week,
      month,
      quarter,
      year,
      hour_of_day,
      hour
    ]
    sql: ${TABLE}.end_time ;;
  }

  measure: count {
    type: count
  }

  measure: rated_post_count {
    type: count_distinct
    sql: ${post_id} ;;
    filters: {
      field: end_time
      value: "-null"
    }
  }

  measure: post_task_started_count {
    label: "Total Post Rate Tasks Started"
    description: "Distinct count of all tasks where start time is not null"
    type: count_distinct
    sql: ${post_id} ;;
    filters: {
      field: start_time
      value: "-null"
    }
  }

  measure: sum_duration_seconds {
    label: "Total Seconds Post Rating"
    hidden: yes
    type: sum
    sql: ${duration} ;;
#     filters: {
#       field: duration_minutes
#       value: "<= 5"
#     }
    value_format: "0.00"
  }

  measure: sum_duration_minutes {
    label: "Total Minutes Post Rating"
    type: number
    sql: ${sum_duration_seconds}/60 ;;
    value_format: "0.00"
  }

  measure: average_seconds_per_post {
    label: "Average Duration - Seconds"
    type: number
    sql: ${sum_duration_seconds}/${rated_post_count} ;;
    value_format: "0.00"
  }

  measure: total_pr_raters {
    label: "Total Number of Raters"
    type: count_distinct
    sql: ${username} ;;
  }
}
