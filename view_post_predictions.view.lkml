
view: view_post_predictions {
  label: "Post Model Predictions"
  derived_table: {
    # trigger the rebuild of this table when the post_ratings table has updated. it is the last major table in the ETL update process.
#     sql_trigger_value:  SELECT COUNT(*) FROM ${view_post_predictions_alcohol.SQL_TABLE_NAME} ;;
#     distribution_style: all
#     indexes: ["post_id"]
    explore_source: posts {
      column: post_id {}
      column: single_model_prediction {field:posts.probability}
      column: alcohol_prediction { field: view_post_predictions_alcohol.alcohol_value }
      column: bigotry_prediction { field: view_post_predictions_bigotry.bigotry_value }
      column: crime_prediction { field: view_post_predictions_crime.crime_value }
      column: drugs_prediction { field: view_post_predictions_drugs.drugs_value }
      column: language_prediction { field: view_post_predictions_language.language_value }
      column: sex_prediction { field: view_post_predictions_sex.sex_value }
      column: sexism_prediction { field: view_post_predictions_sexism.sexism_value }
      column: violence_prediction { field: view_post_predictions_violence.violence_value }
      column: alcohol_flag_count { field: post_flags.alcohol_flag_count }
      column: bad_flag_count { field: post_flags.bad_flag_count }
      column: bigotry_flag_count { field: post_flags.bigotry_flag_count }
      column: crime_flag_count { field: post_flags.crime_flag_count }
      column: drugs_flag_count { field: post_flags.drugs_flag_count }
      column: language_flag_count { field: post_flags.language_flag_count }
      column: sex_flag_count { field: post_flags.sex_flag_count }
      column: sexism_flag_count { field: post_flags.sexism_flag_count }
      column: violence_flag_count { field: post_flags.violence_flag_count }

    }
  }
  dimension: post_id {
    primary_key: yes
  }
  dimension: single_model_prediction {
    label: "Prediction - Single Model"
    type: number
  }
  dimension: alcohol_prediction {
    label: "Prediction - Alcohol"
    type: number
  }
  dimension: bigotry_prediction {
    label: "Prediction - Bigotry"
    type: number
  }
  dimension: crime_prediction {
    label: "Prediction - Crime"
    type: number
  }
  dimension: drugs_prediction {
    label: "Prediction - Drugs"
    type: number
  }
  dimension: language_prediction {
    label: "Prediction - Language"
    type: number
  }
  dimension: sex_prediction{
    label: "Prediction - Sex"
    type: number
  }
  dimension: sexism_prediction {
    label: "Prediction - Sexism"
    type: number
  }
  dimension: violence_prediction {
    label: "Prediction - Violence"
    type: number
  }
  dimension: sent_to_rating {
    label: "Prediction - Positive Bad Flag"
    type: yesno
    sql: CASE WHEN ${alcohol_prediction} >= 0.2 THEN 1
              WHEN ${drugs_prediction} >= 0.2 THEN 1
              WHEN ${bigotry_prediction} >= 0.25 THEN 1
              WHEN ${sex_prediction} >= 0.23 THEN 1
              WHEN ${language_prediction} >= 0.35 THEN 1
              WHEN ${crime_prediction} >= 2.71 THEN 1
              WHEN ${violence_prediction} >= 2.71 THEN 1
              ELSE 0 END;;
  }
  dimension: alcohol_flag_count {
    label: "Flagged - Alcohol"
    type: number
  }
  dimension: bad_flag_count {
    type: number
  }
  dimension: bigotry_flag_count {
    label: "Flagged - Bigotry"
    type: number
  }
  dimension: crime_flag_count {
    label: "Flagged - Crime"
    type: number
  }
  dimension: drugs_flag_count {
    label: "Flagged - Drugs"
    type: number
  }
  dimension: language_flag_count {
    label: "Flagged - Language"
    type: number
  }
  dimension: sex_flag_count {
    label: "Flagged - Sex"
    type: number
  }
  dimension: sexism_flag_count {
    label: "Flagged - Sexism"
    type: number
  }
  dimension: violence_flag_count {
    label: "Flagged - Violence"
    type: number
  }

  measure: count {
    type: count
  }

  measure: positive_probablity_count {
    type: count
    filters: {
      field: sent_to_rating
      value: "yes"
    }
  }

  measure: bad_flag_false_positive_count {
    type: count
    filters: {
      field: bad_flag_count
      value: "0"
    }
    filters: {
      field: sent_to_rating
      value: "yes"
    }
  }
}
