view: view_reports_total_article_flags {
  derived_table: {
#     datagroup_trigger: etl_trigger
    sql_trigger_value: SELECT COUNT(*) FROM public.article_flags ;;
    distribution_style: all
    explore_source: companies {
      column: report_id { field: reports.report_id }
      column: articles_bad_flag_count { field: article_flags.article_bad_flags_count }
      column: articles_alcohol_flag_count { field: article_flags.alcohol_flag_count }
      column: articles_bigotry_flag_count { field: article_flags.bigotry_flag_count }
      column: articles_crime_flag_count { field: article_flags.crime_flag_count }
      column: articles_threat_flag_count { field: article_flags.threat_flag_count }
      column: articles_harassment_flag_count { field: article_flags.harassment_flag_count }
      column: articles_intolerance_flag_count { field: article_flags.intolerance_flag_count }
      column: articles_community_flag_count { field: article_flags.community_flag_count }
      column: articles_donation_flag_count { field: article_flags.donation_flag_count }
      column: articles_drugs_flag_count { field: article_flags.drugs_flag_count }
      column: articles_other_drugs_flag_count { field: article_flags.other_drugs_flag_count }
      column: articles_cannabis_flag_count { field: article_flags.cannabis_flag_count }
      column: articles_helping_others_flag_count { field: article_flags.helping_others_flag_count }
      column: articles_keyword_flags_count { field: article_flags.keyword_flags_count }
      column: articles_language_flag_count { field: article_flags.language_flag_count }
      column: articles_protecting_others_flag_count { field: article_flags.protecting_others_flag_count }
      column: articles_sex_flag_count { field: article_flags.sex_flag_count }
      column: articles_sexism_flag_count { field: article_flags.sexism_flag_count }
      column: articles_violence_flag_count { field: article_flags.violence_flag_count }
      column: articles_volunteering_flag_count { field: article_flags.volunteering_flag_count }
      column: articles_good_flag_count { field: article_flags.good_flags_count }
      column: articles_keyword_flag_count { field: article_flags.keyword_flags_count }
    }
  }
  dimension: report_id {
    hidden: yes
    primary_key: yes
  }
  dimension: articles_bad_flag_count {
    label: "Articles | Total | Bad"
    type: number
  }
  dimension: articles_good_flag_count {
    label: "Articles | Total | Good"
    type: number
  }
  dimension: articles_keyword_flag_count {
    label: "Articles | Total | Keywords"
    type: number
  }
  dimension: articles_alcohol_flag_count {
    label: "Articles | Bad | Alcohol"
    type: number
  }
  dimension: articles_bigotry_flag_count {
    label: "Articles | Bad | Bigotry"
    type: number
  }
  dimension: articles_crime_flag_count {
    label: "Articles | Bad | Crime"
    type: number
  }
  dimension: articles_donation_flag_count {
    label: "Articles | Good | Donation"
    type: number
  }
  dimension: articles_drugs_flag_count {
    label: "Articles | Bad | Drugs"
    type: number
  }
  dimension: articles_helping_others_flag_count {
    label: "Articles | Good | Helping Others"
    type: number
  }
  dimension: articles_keyword_flags_count {
    label: "Articles | Keyword | Total"
    type: number
  }
  dimension: articles_language_flag_count {
    label: "Articles | Bad | Language"
    type: number
  }
  dimension: articles_protecting_others_flag_count {
    label: "Articles | Good | Protecting Others"
    type: number
  }
  dimension: articles_sex_flag_count {
    label: "Articles | Bad | Sex"
    type: number
  }
  dimension: articles_sexism_flag_count {
    label: "Articles | Bad | Sexism"
    type: number
  }
  dimension: articles_violence_flag_count {
    label: "Articles | Bad | Violence"
    type: number
  }
  dimension: articles_harassment_flag_count {
    label: "Articles | Bad | Harassment"
    type: number
  }
  dimension: articles_threat_flag_count {
    label: "Articles | Bad | Threat"
    type: number
  }
  dimension: articles_cannabis_flag_count {
    label: "Articles | Bad | Cannabis"
    type: number
  }
  dimension: articles_intolerance_flag_count {
    label: "Articles | Bad | Intolerance"
    type: number
  }
  dimension: articles_community_flag_count {
    label: "Articles | Good | Community"
    type: number
  }
  dimension: articles_other_drugs_flag_count {
    label: "Articles | Bad | Other Drugs"
    type: number
  }
  dimension: articles_volunteering_flag_count {
    label: "Articles | Good | Volunteering"
    type: number
  }
  dimension: articles_total_flag_count {
    label: "Articles | Total | Flags"
    type: number
    sql: ${articles_bad_flag_count} + ${articles_good_flag_count} + ${articles_keyword_flag_count} ;;
  }
}
