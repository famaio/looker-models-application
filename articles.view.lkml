view: articles {
  sql_table_name: public.articles ;;

#   derived_table: {
#     distribution_style: all
#     sql: select * from public.articles;;
#     # trigger the rebuild of this table when the post_ratings table has updated. it is the last major table in the ETL update process.
#     sql_trigger_value:  SELECT COUNT(*) FROM public.post_ratings ;;
#   }

  dimension: article_id {
    primary_key: yes
    type: string
    sql: ${TABLE}.article_id ;;
  }

  dimension_group: date_posted {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.date_posted ;;
  }

  dimension: deleted {
    type: yesno
    sql: ${TABLE}.deleted ;;
  }

  dimension: description {
    type: string
    sql: ${TABLE}.description ;;
  }

  dimension: hidden {
    type: yesno
    sql: ${TABLE}.hidden ;;
  }

  dimension: probability {
    type: number
    sql: ${TABLE}.probability ;;
  }

  dimension: relevant {
    type: yesno
    sql: ${TABLE}.relevant ;;
  }

  dimension: homeadvisor_relevant {
    description: "Does the article have a rating of 0.5 and above?"
    type: yesno
    sql: ${probability}>0.5;;
  }

  dimension: report_id {
    type: string
    # hidden: yes
    sql: ${TABLE}.report_id ;;
  }

  dimension: flag_kit_version_id {
    type: string
    sql: ${TABLE}.flag_kit_version_id ;;
  }

  dimension: text {
    type: string
    sql: ${TABLE}.text ;;
  }

  dimension: title {
    type: string
    sql: ${TABLE}.title ;;
  }

  dimension: probablity_flag {
    type: yesno
    sql: ${probability} > .5 ;;
  }

  dimension: probablility_tiers {
    type: tier
    tiers: [-4,-2,0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8]
    style: classic
    sql: ${probability} ;;
  }

  dimension: url {
    type: string
    sql: ${TABLE}.url ;;
  }

  dimension: domain_url {
    type: string
    sql: REGEXP_SUBSTR(${url}, '[^/]+\\.[^/:]+') ;;
  }

#   dimension: bing_article {
#     type: yesno
#     sql: article_queries.external_api_type = 'BING' ;;
#   }
#
#   dimension: webhose_article {
#     type: yesno
#     sql: article_queries.external_api_type = 'WEBHOSE' ;;
#   }

  measure: count {
    type: count
    drill_fields: [article_detail*]
  }

  measure: relevant_count {
    type:  count
    filters: {
      field: relevant
      value: "yes"
    }
    drill_fields: [detail*]
  }

#   measure: total_articles_on_bing {
#     type: count
#     filters: {
#       field: bing_article
#       value: "yes"
#     }
#   }
#
#   measure: total_articles_on_webhose {
#     type: count
#     filters: {
#       field: webhose_article
#       value: "yes"
#     }
#   }
#
#   measure: total_articles_on_bing_webhose {
#     type: count
#     filters: {
#       field: bing_article
#       value: "yes"
#     }
#     filters: {
#       field: webhose_article
#       value: "yes"
#     }
#   }

  measure: eight_and_above_relevant_count {
    label: "Relevant - 0.8 and Above"
    type:  count
    filters: {
      field: probability
      value: ">= 0.8"
    }
    filters: {
      field: relevant
      value: "yes"
    }
    drill_fields: [detail*]
  }

  measure: eight_and_above_count {
    label: "0.8 and Above"
    type:  count
    filters: {
      field: probability
      value: ">= 0.8"
    }
    drill_fields: [detail*]
  }

  measure: five_and_above_count {
    label: "0.5 and Above"
    type:  count
    filters: {
      field: probability
      value: ">= 0.5"
    }
    drill_fields: [detail*]
  }

  measure: four_and_above_count {
    label: "0.4 and Above"
    type:  count
    filters: {
      field: probability
      value: ">= 0.4"
    }
    drill_fields: [detail*]
  }

  measure: three_and_above_count {
    label: "0.3 and Above"
    type:  count
    filters: {
      field: probability
      value: ">= 0.3"
    }
    drill_fields: [detail*]
  }

  measure: two_and_above_count {
    label: "0.2 and Above"
    type:  count
    filters: {
      field: probability
      value: ">= 0.2"
    }
    drill_fields: [detail*]
  }

  measure: one_and_above_count {
    label: "0.1 and Above"
    type:  count
    filters: {
      field: probability
      value: ">= 0.1"
    }
    drill_fields: [detail*]
  }

  measure: zero_and_above_count {
    label: "0.0 and Above"
    type:  count
    filters: {
      field: probability
      value: ">= 0"
    }
    drill_fields: [detail*]
  }

  measure: probablity_threshold_count {
    type:  count
    filters: {
      field: probablity_flag
      value: "yes"
    }
    drill_fields: [detail*]
  }

  measure: total_report_count {
    #view_label: "Total Reports"
    type: count_distinct
    sql: ${report_id} ;;
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      article_id,
      reports.first_name,
      reports.middle_name,
      reports.last_name,
      reports.report_id,
      article_flags.count,
      article_predictions.count,
      article_queries.count,
      article_ratings.count
    ]
  }

# ----- Sets of fields for drilling ------
  set: article_detail {
    fields: [
      article_id,
      title,
      probability,
      article_queries.data_type
    ]
  }
}
