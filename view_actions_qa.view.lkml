

view: view_qa_actions{
  view_label: "Rater Actions - QA"
  derived_table: {
    sql_trigger_value: SELECT count(*) FROM public.rater_actions ;;
    distribution_style: all
    explore_source: rater_actions {
      column: report_id {field: rater_actions.action_item}
      column: action_type {}
      column: start_time {field: rater_actions.start_raw}
      column: end_time {field: rater_actions.end_raw}
      column: duration {}
      column: duration_minutes {field: rater_actions.duration_minutes}
      column: rater_id {}
      column: username {}
      column: masked_username {}
      filters: {
        field: rater_actions.action_type
        value: "MANUAL^_PERSON^_QA, PERSON^_MANUAL^_QA"
      }
    }
  }
  dimension: compound_primary_key {
    primary_key: yes
    hidden: yes
    sql: ${TABLE}.report_id || ${TABLE}.duration ;;
  }
  dimension: report_id {
    #primary_key: yes
  }
  dimension: action_type {}
  dimension: duration {
    type: number
  }
  dimension: duration_minutes {
    type: number
  }
  dimension: rater_id {}

  dimension: username {
    type: string
    sql: ${TABLE}.username ;;

  }

  dimension: team {
    type: string
    sql:

    CASE WHEN ${TABLE}.username in ('yohandra','samir-s','michelle','julied','niaroane',
    'mark-coco','angel','georgios','favy','alex','jessica','dorga','david','james','tracy')
    THEN 'in-house'
        WHEN ${TABLE}.username ilike '%os%'
    THEN 'AOS'
    WHEN ${TABLE}.username ilike '%jenald2%'
    THEN 'AOS'
    WHEN ${TABLE}.username ilike '%cf%'
    THEN 'CF'
    END;;
  }

  measure: inhouse_count {
    label: "Total in-house QA count"
    type: count
    filters: {field: username
              value: "in-house"}
  }

  dimension: masked_username {}


  dimension_group: start {
    type: time
    timeframes: [
      raw,
      time,
      date,
      day_of_week,
      week,
      month,
      quarter,
      year,
      hour_of_day,
      hour
    ]
    sql: ${TABLE}.start_time ;;
  }

  dimension_group: end {
    type: time
    timeframes: [
      raw,
      time,
      date,
      day_of_week,
      week,
      month,
      quarter,
      year,
      hour_of_day,
      hour
    ]
    sql: ${TABLE}.end_time ;;
  }

  measure: count {
    type: count
  }

  measure: duration_sum {
    type: sum
    sql: ${duration};;
  }

  measure: manual_qa_count {
    label: "Total QA Tasks Completed"
    description: "Includes all tasks with an end time"
    type: count
    filters: {
      field:  end_time
      value: "-null"
    }
  }

  measure: task_start_count {
    label: "Total QA Tasks Started"
    description: "Includes all tasks with a start time"
    type: count
    filters: {
      field:  start_time
      value: "-null"
    }
  }

  measure: average_duration_seconds {
    label: "Average Duration - Seconds"
    type: average
    sql: ${duration} ;;
    filters: {
      field: end_time
      value: "-null"
    }
    value_format_name: "decimal_2"
  }

  measure:  average_duration_minutes {
    label: "Average Duration - Minutes"
    type: number
    sql: ${average_duration_seconds}/60 ;;
    value_format_name: "decimal_2"
  }

  measure: total_qa_raters {
    label: "Total Number of Raters"
    type: count_distinct
    sql: ${username} ;;
  }

#   measure: median_duration {
#     label: "Median Duration - Seconds"
#     type: median
#     sql: ${duration} ;;
#     value_format_name: "decimal_2"
#     filters: {
#       field: end_date
#       value: "-null"
#     }
#   }
}
