# If necessary, uncomment the line below to include explore_source.
# include: "application.model.lkml"

view: view_auto_flagged_posts {
  derived_table: {
    sql_trigger_value:  SELECT COUNT(*) FROM ${view_post_actions_meta_data.SQL_TABLE_NAME} ;;
    distribution_style: all
    explore_source: companies {
      column: post_id { field: posts.post_id }
      column: auto_flagged { field: post_ratings.auto_flagged }
      column: rated_post_count { field: view_post_actions_meta_data.rated_post_count }
      column: bad_flag_count { field: post_flags.bad_flag_count }
      column: good_flag_count { field: post_flags.good_flag_count }
      column: language_flag_count {field:post_flags.language_flag_count}
      column: date_created_time { field: reports.date_created_raw }
      column: date_finished_time { field: reports.date_finished_raw }
      filters: {
        field: post_ratings.auto_flagged
        value: "Yes"
      }
      filters: {
        field: reports.date_finished_date
        value: "after 2018/03/01"
      }
#       filters: {
#
#         field: post_flags.language_flag_count
#         value: ">0"
#       }

    }
  }
  dimension: post_id {
    primary_key: yes
  }
  dimension: auto_flagged {
    label: "Auto Flagged for Language"
    description: "Column flag to show if a post was auto rated."
  }
  dimension: rated_post_count {
    label: "Rater Actions - Posts Rated Post Count"
    type: number
  }
  dimension: bad_flag_count {
    type: number
  }
  dimension: good_flag_count {
    type: number
  }
  dimension: language_flag_count {
    type: number
  }
  dimension: flag_count {
    type: number
    sql: ${bad_flag_count}+${good_flag_count} ;;
  }
  dimension_group: start {
    type: time
    timeframes: [
      raw,
      time,
      date,
      day_of_week,
      week,
      month,
      quarter,
      year,
      hour_of_day,
      hour
    ]
    sql: ${TABLE}.date_created_time ;;
  }

  dimension_group: end {
    type: time
    timeframes: [
      raw,
      time,
      date,
      day_of_week,
      week,
      month,
      quarter,
      year,
      hour_of_day,
      hour
    ]
    sql: ${TABLE}.date_finished_time ;;
  }

  measure: count {
    label: "Auto Flag Count"
    description: "The number of posts that were auto flagged for language."
    type: count
  }

  measure: language_only_flag_count {
    label: "Language Only Flag Count"
    type: count_distinct
    sql: ${post_id} ;;
    filters: {
      field: language_flag_count
      value: "1"
    }
    filters: {
      field: bad_flag_count
      value: "1"
    }
  }

  measure:  no_rating_auto_flagged_count{
    label: "No Rating Post Count"
    type: count
    filters: {
      field: rated_post_count
      value: "0"
    }
  }

  measure: auto_flagged_rated_count {
    label: "Rated Post Count"
    type: count
    filters: {
      field: rated_post_count
      value: ">0"
    }
  }

  measure: auto_flagged_multiple_flags_count {
    label: "Posts with Multiple Flags"
    type: count
    filters: {
      field: flag_count
      value: ">1"
    }
  }
}
