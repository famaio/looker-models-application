view: view_latest_reports {
  # Or, you could make this view a derived table, like this:
  derived_table: {
    datagroup_trigger: etl_trigger
    distribution_style: all
    indexes: ["report_id", "company_id", "flag_kit_version_id","user_id","date_finished", "date_created"]
    #sortkeys: ["date_finished", "date_created"]
    sql:
      select
      r.report_id,
      r.company_id,
      r.flag_kit_version_id,
      user_id,
      date_finished,
      date_created,
      'Yes' as "latest_report"
      from
      (

      select person_id, max(date_finished) as latest_report_finished
      from reports
      where deleted = false
      group by person_id

      ) as foo

      left join reports r on r.person_id = foo.person_id AND r.date_finished = foo.latest_report_finished
      ;;
  }

  # Define your dimensions and measures here, like this:
  dimension: report_id {
    primary_key: yes
    hidden: yes
    description: "Report Id of the most recent report ran."
    type: string
    sql: ${TABLE}.report_id ;;
  }

  dimension:  latest_report{
    type: yesno
    description: "Use this dimension to view only the latest report a person has. Only filter YES. Remove filter to view all reports."
    sql: ${TABLE}.latest_report = 'Yes';;
  }
}
