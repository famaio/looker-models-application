# If necessary, uncomment the line below to include explore_source.
# include: "application.model.lkml"

view: view_reports_total_post_flags{
  derived_table: {
#     datagroup_trigger: etl_trigger
    sql_trigger_value: SELECT COUNT(*) FROM public.post_flags ;;
    distribution_style: all
    explore_source: companies {
      column: report_id { field: reports.report_id }
      column: posts_bad_flag_count { field: post_flags.bad_flag_count }
      column: posts_good_flag_count { field: post_flags.good_flag_count }
      column: posts_keyword_flag_count { field: post_flags.keyword_flag_count }
      column: posts_alcohol_flag_count { field: post_flags.alcohol_flag_count }
      column: posts_alcohol_original_flag_count { field: post_flags.alcohol_original_flag_count }
      column: posts_alcohol_liked_flag_count { field: post_flags.alcohol_liked_flag_count }
      column: posts_alcohol_shared_flag_count { field: post_flags.alcohol_shared_flag_count }
      column: posts_bigotry_flag_count { field: post_flags.bigotry_flag_count }
      column: posts_bigotry_original_flag_count { field: post_flags.bigotry_original_flag_count }
      column: posts_bigotry_liked_flag_count { field: post_flags.bigotry_liked_flag_count }
      column: posts_bigotry_shared_flag_count { field: post_flags.bigotry_shared_flag_count }
      column: posts_crime_flag_count { field: post_flags.crime_flag_count }
      column: posts_crime_original_flag_count { field: post_flags.crime_original_flag_count }
      column: posts_crime_liked_flag_count { field: post_flags.crime_liked_flag_count }
      column: posts_crime_shared_flag_count { field: post_flags.crime_shared_flag_count }
      column: posts_donation_flag_count { field: post_flags.donation_flag_count }
      column: posts_donation_original_flag_count { field: post_flags.donation_original_flag_count }
      column: posts_donation_liked_flag_count { field: post_flags.donation_liked_flag_count }
      column: posts_donation_shared_flag_count { field: post_flags.donation_shared_flag_count }
      column: posts_drugs_flag_count { field: post_flags.drugs_flag_count }
      column: posts_drugs_original_flag_count { field: post_flags.drugs_original_flag_count }
      column: posts_drugs_liked_flag_count { field: post_flags.drugs_liked_flag_count }
      column: posts_drugs_shared_flag_count { field: post_flags.drugs_shared_flag_count }
      column: posts_helping_others_flag_count { field: post_flags.helping_others_flag_count }
      column: posts_helping_others_original_flag_count { field: post_flags.helping_others_original_flag_count }
      column: posts_helping_others_liked_flag_count { field: post_flags.helping_others_liked_flag_count }
      column: posts_helping_others_shared_flag_count { field: post_flags.helping_others_shared_flag_count }
      column: posts_language_flag_count { field: post_flags.language_flag_count }
      column: posts_language_original_flag_count { field: post_flags.language_original_flag_count }
      column: posts_language_liked_flag_count { field: post_flags.language_liked_flag_count }
      column: posts_language_shared_flag_count { field: post_flags.language_shared_flag_count }
      column: posts_protecting_others_flag_count { field: post_flags.protecting_others_flag_count }
      column: posts_protecting_others_original_flag_count { field: post_flags.protecting_others_original_flag_count }
      column: posts_protecting_others_liked_flag_count { field: post_flags.protecting_others_liked_flag_count }
      column: posts_protecting_others_shared_flag_count { field: post_flags.protecting_others_shared_flag_count }
      column: posts_sex_flag_count { field: post_flags.sex_flag_count }
      column: posts_sex_original_flag_count { field: post_flags.sex_original_flag_count }
      column: posts_sex_liked_flag_count { field: post_flags.sex_liked_flag_count }
      column: posts_sex_shared_flag_count { field: post_flags.sex_shared_flag_count }
      column: posts_sexism_flag_count { field: post_flags.sexism_flag_count }
      column: posts_sexism_original_flag_count { field: post_flags.sexism_original_flag_count }
      column: posts_sexism_liked_flag_count { field: post_flags.sexism_liked_flag_count }
      column: posts_sexism_shared_flag_count { field: post_flags.sexism_shared_flag_count }
      column: posts_violence_flag_count { field: post_flags.violence_flag_count }
      column: posts_violence_original_flag_count { field: post_flags.violence_original_flag_count }
      column: posts_violence_liked_flag_count { field: post_flags.violence_liked_flag_count }
      column: posts_violence_shared_flag_count { field: post_flags.violence_shared_flag_count }
      column: posts_threat_flag_count { field: post_flags.threat_flag_count }
      column: posts_threat_original_flag_count { field: post_flags.threat_original_flag_count }
      column: posts_threat_liked_flag_count { field: post_flags.threat_liked_flag_count }
      column: posts_threat_shared_flag_count { field: post_flags.threat_shared_flag_count }
      column: posts_harassment_flag_count { field: post_flags.harassment_flag_count }
      column: posts_harassment_original_flag_count { field: post_flags.harassment_original_flag_count }
      column: posts_harassment_liked_flag_count { field: post_flags.harassment_liked_flag_count }
      column: posts_harassment_shared_flag_count { field: post_flags.harassment_shared_flag_count }
      column: posts_volunteering_flag_count { field: post_flags.volunteering_flag_count }
      column: posts_volunteering_original_flag_count { field: post_flags.volunteering_original_flag_count }
      column: posts_volunteering_liked_flag_count { field: post_flags.volunteering_liked_flag_count }
      column: posts_volunteering_shared_flag_count { field: post_flags.volunteering_shared_flag_count }
      column: posts_other_drugs_flag_count { field: post_flags.other_drugs_flag_count }
      column: posts_other_drugs_original_flag_count { field: post_flags.other_drugs_original_flag_count }
      column: posts_other_drugs_liked_flag_count { field: post_flags.other_drugs_liked_flag_count }
      column: posts_other_drugs_shared_flag_count { field: post_flags.other_drugs_shared_flag_count }
      column: posts_cannabis_flag_count { field: post_flags.cannabis_flag_count }
      column: posts_cannabis_original_flag_count { field: post_flags.cannabis_original_flag_count }
      column: posts_cannabis_liked_flag_count { field: post_flags.cannabis_liked_flag_count }
      column: posts_cannabis_shared_flag_count { field: post_flags.cannabis_shared_flag_count }
      column: posts_intolerance_flag_count { field: post_flags.intolerance_flag_count }
      column: posts_intolerance_original_flag_count { field: post_flags.intolerance_original_flag_count }
      column: posts_intolerance_liked_flag_count { field: post_flags.intolerance_liked_flag_count }
      column: posts_intolerance_shared_flag_count { field: post_flags.intolerance_shared_flag_count }
      column: posts_community_flag_count { field: post_flags.community_flag_count }
      column: posts_community_original_flag_count { field: post_flags.community_original_flag_count }
      column: posts_community_liked_flag_count { field: post_flags.community_liked_flag_count }
      column: posts_community_shared_flag_count { field: post_flags.community_shared_flag_count }
   }
  }
  dimension: report_id {
    primary_key: yes
  }
  dimension: posts_bad_flag_count {
    label: "Posts | Total | Bad"
    type: number
  }
  dimension: posts_good_flag_count {
    label: "Posts | Total | Good"
    type: number
  }
  dimension: posts_keyword_flag_count {
    label: "Posts | Total | Keyword"
    type: number
  }
  dimension: posts_alcohol_flag_count {
    label: "Posts | Bad | Alcohol | Total"
    type: number
  }
  dimension: posts_alcohol_original_flag_count {
    label: "Posts | Bad | Alcohol | Original"
    type: number
  }
  dimension: posts_alcohol_liked_flag_count {
    label: "Posts | Bad | Alcohol | Liked"
    type: number
  }
  dimension: posts_alcohol_shared_flag_count {
    label: "Posts | Bad | Alcohol | Shared"
    type: number
  }
  dimension: posts_bigotry_flag_count {
    label: "Posts | Bad | Bigotry | Total"
    type: number
  }
  dimension: posts_bigotry_original_flag_count {
    label: "Posts | Bad | Bigotry | Original"
    type: number
  }
  dimension: posts_bigotry_liked_flag_count {
    label: "Posts | Bad | Bigotry | Liked"
    type: number
  }
  dimension: posts_bigotry_shared_flag_count {
    label: "Posts | Bad | Bigotry | Shared"
    type: number
  }
  dimension: posts_crime_flag_count {
    label: "Posts | Bad | Crime | Total"
    type: number
  }
  dimension: posts_crime_original_flag_count {
    label: "Posts | Bad | Crime | Original"
    type: number
  }
  dimension: posts_crime_liked_flag_count {
    label: "Posts | Bad | Crime | Liked"
    type: number
  }
  dimension: posts_crime_shared_flag_count {
    label: "Posts | Bad | Crime | Shared"
    type: number
  }
  dimension: posts_threat_flag_count {
    label: "Posts | Bad | Threat | Total"
    type: number
  }
  dimension: posts_threat_original_flag_count {
    label: "Posts | Bad | Threat | Original"
    type: number
  }
  dimension: posts_threat_liked_flag_count {
    label: "Posts | Bad | Threat | Liked"
    type: number
  }
  dimension: posts_threat_shared_flag_count {
    label: "Posts | Bad | Threat | Shared"
    type: number
  }
  dimension: posts_harassment_flag_count {
    label: "Posts | Bad | Harassment | Total"
    type: number
  }
  dimension: posts_harassment_original_flag_count {
    label: "Posts | Bad | Harassment | Original"
    type: number
  }
  dimension: posts_harassment_liked_flag_count {
    label: "Posts | Bad | Harassment | Liked"
    type: number
  }
  dimension: posts_harassment_shared_flag_count {
    label: "Posts | Bad | Harassment | Shared"
    type: number
  }
  dimension: posts_cannabis_flag_count {
    label: "Posts | Bad | Cannabis | Total"
    type: number
  }
  dimension: posts_cannabis_original_flag_count {
    label: "Posts | Bad | Cannabis | Original"
    type: number
  }
  dimension: posts_cannabis_liked_flag_count {
    label: "Posts | Bad | Cannabis | Liked"
    type: number
  }
  dimension: posts_cannabis_shared_flag_count {
    label: "Posts | Bad | Cannabis | Shared"
    type: number
  }
  dimension: posts_other_drugs_flag_count {
    label: "Posts | Bad | Other Drugs | Total"
    type: number
  }
  dimension: posts_other_drugs_original_flag_count {
    label: "Posts | Bad | Other Drugs | Original"
    type: number
  }
  dimension: posts_other_drugs_liked_flag_count {
    label: "Posts | Bad | Other Drugs | Liked"
    type: number
  }
  dimension: posts_other_drugs_shared_flag_count {
    label: "Posts | Bad | Other Drugs | Shared"
    type: number
  }
  dimension: posts_donation_flag_count {
    label: "Posts | Good | Donation | Total"
    type: number
  }
  dimension: posts_donation_original_flag_count {
    label: "Posts | Good | Donation | Original"
    type: number
  }
  dimension: posts_donation_liked_flag_count {
    label: "Posts | Good | Donation | Liked"
    type: number
  }
  dimension: posts_donation_shared_flag_count {
    label: "Posts | Good | Donation | Shared"
    type: number
  }
  dimension: posts_drugs_flag_count {
    label: "Posts | Bad | Drugs | Total"
    type: number
  }
  dimension: posts_drugs_original_flag_count {
    label: "Posts | Bad | Drugs | Original"
    type: number
  }
  dimension: posts_drugs_liked_flag_count {
    label: "Posts | Bad | Drugs | Liked"
    type: number
  }
  dimension: posts_drugs_shared_flag_count {
    label: "Posts | Bad | Drugs | Shared"
    type: number
  }
  dimension: posts_helping_others_flag_count {
    label: "Posts | Good | Helping Others | Total"
    type: number
  }
  dimension: posts_helping_others_original_flag_count {
    label: "Posts | Good | Helping Others | Original"
    type: number
  }
  dimension: posts_helping_others_liked_flag_count {
    label: "Posts | Good | Helping Others | Liked"
    type: number
  }
  dimension: posts_helping_others_shared_flag_count {
    label: "Posts | Good | Helping Others | Shared"
    type: number
  }
  dimension: posts_language_flag_count {
    label: "Posts | Bad | Language | Total"
    type: number
  }
  dimension: posts_language_original_flag_count {
    label: "Posts | Bad | Language | Original"
    type: number
  }
  dimension: posts_language_liked_flag_count {
    label: "Posts | Bad | Language | Liked"
    type: number
  }
  dimension: posts_language_shared_flag_count {
    label: "Posts | Bad | Language | Shared"
    type: number
  }
  dimension: posts_protecting_others_flag_count {
    label: "Posts | Good | Protecting Others | Total"
    type: number
  }
  dimension: posts_protecting_others_original_flag_count {
    label: "Posts | Good | Protecting Others | Original"
    type: number
  }
  dimension: posts_protecting_others_liked_flag_count {
    label: "Posts | Good | Protecting Others | Liked"
    type: number
  }
  dimension: posts_protecting_others_shared_flag_count {
    label: "Posts | Good | Protecting Others | Shared"
    type: number
  }
  dimension: posts_sex_flag_count {
    label: "Posts | Bad | Sex | Total"
    type: number
  }
  dimension: posts_sex_original_flag_count {
    label: "Posts | Bad | Sex | Original"
    type: number
  }
  dimension: posts_sex_liked_flag_count {
    label: "Posts | Bad | Sex | Liked"
    type: number
  }
  dimension: posts_sex_shared_flag_count {
    label: "Posts | Bad | Sex | Shared"
    type: number
  }
  dimension: posts_sexism_flag_count {
    label: "Posts | Bad | Sexism | Total"
    type: number
  }
  dimension: posts_sexism_original_flag_count {
    label: "Posts | Bad | Sexism | Original"
    type: number
  }
  dimension: posts_sexism_liked_flag_count {
    label: "Posts | Bad | Sexism | Liked"
    type: number
  }
  dimension: posts_sexism_shared_flag_count {
    label: "Posts | Bad | Sexism | Shared"
    type: number
  }
  dimension: posts_violence_flag_count {
    label: "Posts | Bad | Violence | Total"
    type: number
  }
  dimension: posts_violence_original_flag_count {
    label: "Posts | Bad | Violence | Original"
    type: number
  }
  dimension: posts_violence_liked_flag_count {
    label: "Posts | Bad | Violence | Liked"
    type: number
  }
  dimension: posts_violence_shared_flag_count {
    label: "Posts | Bad | Violence | Shared"
    type: number
  }
  dimension: posts_volunteering_flag_count {
    label: "Posts | Good | Volunteering | Total"
    type: number
  }
  dimension: posts_volunteering_original_flag_count {
    label: "Posts | Good | Volunteering | Original"
    type: number
  }
  dimension: posts_volunteering_liked_flag_count {
    label: "Posts | Good | Volunteering | Liked"
    type: number
  }
  dimension: posts_volunteering_shared_flag_count {
    label: "Posts | Good | Volunteering | Shared"
    type: number
  }

  dimension: posts_intolerance_flag_count {
    label: "Posts | Bad | Intolerance | Total"
    type: number
  }
  dimension: posts_intolerance_original_flag_count {
    label: "Posts | Bad | Intolerance | Original"
    type: number
  }
  dimension: posts_intolerance_liked_flag_count {
    label: "Posts | Bad | Intolerance | Liked"
    type: number
  }
  dimension: posts_intolerance_shared_flag_count {
    label: "Posts | Bad | Intolerance | Shared"
    type: number
  }

  dimension: posts_community_flag_count {
    label: "Posts | Good | Community | Total"
    type: number
  }
  dimension: posts_community_original_flag_count {
    label: "Posts | Good | Community | Original"
    type: number
  }
  dimension: posts_community_liked_flag_count {
    label: "Posts | Good | Community | Liked"
    type: number
  }
  dimension: posts_community_shared_flag_count {
    label: "Posts | Good | Community | Shared"
    type: number
  }
  dimension: posts_total_flag_count {
    label: "Posts | Total | Flags"
    type: number
    sql: ${posts_bad_flag_count} + ${posts_good_flag_count} + ${posts_keyword_flag_count} ;;
  }
}
