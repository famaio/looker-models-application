view: report_aliases {
  sql_table_name: public.report_aliases ;;

  #Brian Yardley - 12/15/17
  dimension: compound_key {
    primary_key: yes
    hidden: yes
    sql: ${TABLE}.report_id || '-' ||  ${TABLE}.alias;;
  }

  dimension: alias {
    type: string
    sql: ${TABLE}.alias ;;
  }

  dimension: report_id {
    type: string
    # hidden: yes
    sql: ${TABLE}.report_id ;;
  }

  measure: count {
    type: count
    drill_fields: [reports.last_name, reports.middle_name, reports.report_id, reports.first_name]
  }
}
