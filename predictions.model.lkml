connection: "redshift"

include: "*.view.lkml"                       # include all views in this project
# include: "my_dashboard.dashboard.lookml"   # include a LookML dashboard called my_dashboard

# # Select the views that should be a part of this model,
# # and define the joins that connect them together.
#
# explore: order_items {
#   join: orders {
#     relationship: many_to_one
#     sql_on: ${orders.id} = ${order_items.order_id} ;;
#   }
#
#   join: users {
#     relationship: many_to_one
#     sql_on: ${users.id} = ${orders.user_id} ;;
#   }
# }

explore: predictions {

  join: reports{
    relationship: many_to_one
    sql_on: ${predictions.report_id} = ${reports.report_id} ;;
  }

  join: profiles {
    relationship: many_to_one
    sql_on: ${predictions.report_id} = ${profiles.report_id} ;;
  }

  join: posts  {
    relationship: many_to_one
    sql_on: ${predictions.content_id} = ${posts.post_id} ;;
  }

  join: post_flags {
    relationship: many_to_one
    sql_on:  ${posts.post_id}=${post_flags.post_id} ;;
  }

  join: view_post_actions {
    type: left_outer
    sql_on: ${view_post_actions.post_id} = ${posts.post_id} ;;
    relationship: many_to_one
  }

}
#PDT Table References
explore: rater_actions {hidden:yes}
