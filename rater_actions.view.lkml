view: rater_actions {
  sql_table_name: public.rater_actions ;;

  dimension: pk {
    primary_key: yes
    hidden: yes
    sql: ${TABLE}.rater_id || '-' ||  ${TABLE}.action_item || '-' || ${TABLE}.start_time;;
  }
  dimension: accuracy {
    type: number
    sql: ${TABLE}.accuracy ;;
  }

  dimension: action_item {
    type: string
    sql: ${TABLE}.action_item ;;
  }

  dimension_group: end {
    type: time
    timeframes: [
      raw,
      time,
      date,
      day_of_week,
      week,
      month,
      quarter,
      year,
      hour_of_day,
      hour
    ]
    sql: ${TABLE}.end_time ;;
  }

  dimension: rater_id {
    type: string
    sql: ${TABLE}.rater_id ;;
  }

  dimension_group: start {
    type: time
    timeframes: [
      raw,
      time,
      date,
      day_of_week,
      week,
      month,
      quarter,
      year,
      hour_of_day,
      hour
    ]
    sql: ${TABLE}.start_time ;;
  }

  dimension: duration {
    type: number
    sql: CASE WHEN ${end_date} IS NOT NULL THEN datediff('seconds', ${start_raw}, ${end_raw}) else 0 END;;
    value_format_name: "decimal_2"
  }

  dimension: duration_with_null {
    label: "Duration Including Null End Dates"
    type: number
    sql: datediff('seconds', ${start_raw}, ${end_raw}) ;;
  }

  dimension: duration_minutes {
    type: number
    sql: CASE WHEN ${end_date} IS NOT NULL THEN datediff('minutes', ${start_raw}, ${end_raw}) END;;
    value_format: "0.00"
  }

  dimension: action_type {
    sql:  CASE
    WHEN ${TABLE}.action_type = 'PROFILE_MANUAL_INSTAGRAM_ADD' THEN 'instagram scrape'
    WHEN ${TABLE}.action_type = 'PROFILE_MANUAL_FACEBOOK_ADD' THEN 'facebook scrape'
    ELSE ${TABLE}.action_type
    END;;
  }

  dimension: username {
    type: string
    sql: ${TABLE}.username ;;
  }

  dimension: masked_username {
    type: string
    sql: CASE WHEN ${username} = 'james' THEN 'JFK'
              WHEN ${username} = 'jessica' THEN 'DFW'
              WHEN ${username} = 'willow' THEN 'MSP'
              WHEN ${username} = 'daniel-lee' THEN 'SEA'
              WHEN ${username} = 'tracy' THEN 'MDW'
              WHEN ${username} = 'dayna' THEN 'AUS'
              WHEN ${username} = 'rachael' THEN 'HOU'
              WHEN ${username} = 'paige' THEN 'PDX'
              WHEN ${username} = 'mark-coco' THEN 'MIA'
              WHEN ${username} = 'jenald' THEN 'YYZ'
              WHEN ${username} = 'thea-os' THEN 'YYC'
              WHEN ${username} = 'rofabel-os' THEN 'YUL'
              WHEN ${username} = 'john-os' THEN 'YYA'
              WHEN ${username} = 'jimmer-os' THEN 'YVR'
              WHEN ${username} LIKE 'cf-%' THEN 'CLY'
              WHEN ${username} LIKE '%-cf' THEN 'CLY' ELSE 'NA' END;;
  }

  dimension: team {
    type: string
    sql:

    CASE WHEN ${TABLE}.username in ('yohandra','samir-s','michelle','julied','niaroane',
    'mark-coco','angel','georgios','val', 'valscrape','favy','alex','jessica','dorga','david','james','tracy')
    THEN 'in-house'
        WHEN ${TABLE}.username ilike '%os%'
    THEN 'AOS'
        WHEN ${TABLE}.username ilike '%jenald2%'

    THEN 'AOS'
        WHEN ${TABLE}.username ilike '%cf%'
    THEN 'CF'
    END;;
  }

  measure: count {
    type: count
    drill_fields: [username]
  }

  measure : first_task {
    sql: MIN(${start_time}) ;;
  }

  measure : last_task {
    sql: MAX(${start_time}) ;;
  }

  measure: total_person_confirm_count {
    label: "Tasks - Total Person Confirms"
    type: count
    filters: {
      field: action_type
      value: "MANUAL_PERSON_CONFIRM"
    }
    filters: {
      field: end_time
      value: "-null"
    }
    drill_fields: [username]
  }

  measure: total_person_confirm_duration {
    label: "Tasks - Total Person Confirm Duration"
    type: sum
    sql: ${duration} ;;
    filters: {
      field: action_type
      value: "MANUAL_PERSON_CONFIRM"
    }
    filters: {
      field: end_time
      value: "-null"
    }
    drill_fields: [username]
  }

  measure: total_profile_scrape_count {
    label: "Tasks - Total Profile Scrape"
    type: count
    filters: {
      field: action_type
      value: "MANUAL_PROFILE_SCRAPE"
    }
    filters: {
      field: end_time
      value: "-null"
    }
    drill_fields: [username]
  }

  measure: total_news_rate_count {
    label: "Tasks - Total Articles Rates"
    type: count_distinct
    sql:
      CASE WHEN ${action_type} = 'ARTICLE_MANUAL_RATE' OR ${action_type} = 'MANUAL_NEWS_RATE'
      THEN ${action_item}
      ELSE NULL
      END ;;
#     sql: ${action_item} ;;
#     filters: {
#       field: action_type
#       value: "ARTICLE_MANUAL_RATE"
#     }
    filters: {
      field: end_time
      value: "-null"
    }
    drill_fields: [username]
  }

  measure: total_news_rate_duration {
    label: "Tasks - Total Articles Rate Duration"
    type: sum
    sql: ${duration} ;;
    filters: {
      field: action_type
      value: "ARTICLE_MANUAL_RATE"
    }
    filters: {
      field: end_time
      value: "-null"
    }
    drill_fields: [username]
  }

  measure: total_post_rate_count {
    label: "Tasks - Total Post Rates"
    type: count
#     sql: ${action_item} ;;
    filters: {
      field: action_type
      value: "MANUAL_POST_RATE"
    }
    filters: {
      field: end_time
      value: "-null"
    }
    drill_fields: [username]
  }

  measure: total_post_rate_duration {
    label: "Tasks - Total Post Rate Duration"
    type: sum
    sql: ${duration} ;;
    filters: {
      field: action_type
      value: "MANUAL_POST_RATE"
    }
    filters: {
      field: end_time
      value: "-null"
    }
    drill_fields: [username]
  }

  measure: total_QA_count {
    label: "Tasks - Total QAs"
    type: count
    filters: {
      field: action_type
      value: "MANUAL_PERSON_QA"
    }
    filters: {
      field: end_time
      value: "-null"
    }
    drill_fields: [username]
  }

  measure: total_news_QA_count {
    label: "Tasks - Total News QAs"
    type: count
    filters: {
      field: action_type
      value: "MANUAL_PERSON_QA"
    }
    filters: {
      field: end_time
      value: "-null"
    }
    filters: {
      field: duration
      value: ">120"
    }
    drill_fields: [username]
  }

  measure: total_no_news_QA_count {
    label: "Tasks - Total No News QAs"
    type: count
    filters: {
      field: action_type
      value: "MANUAL_PERSON_QA"
    }
    filters: {
      field: end_time
      value: "-null"
    }
    filters: {
      field: duration
      value: "<120"
    }
    drill_fields: [username]
  }

  measure: total_QA_duration {
    label: "Tasks - Total QA Duration"
    type: sum
    sql: ${duration} ;;
    filters: {
      field: action_type
      value: "MANUAL_PERSON_QA"
    }
    filters: {
      field: end_time
      value: "-null"
    }
    filters: {
      field: duration
      value: ">120"
    }
    drill_fields: [username]
  }

  measure: duration_average {
    type: average
    sql: ${duration} ;;
  }

  measure: duration_average_minutes {
    type: average
    sql: ${duration_minutes} ;;
  }

  measure: duration_median_minutes {
    type: median
    sql: ${duration_minutes} ;;
  }

  measure: duration_sum {
    type: sum
    sql: ${duration} ;;
  }

  measure: duration_sum_minutes {
    type: sum
    sql: ${duration_minutes} ;;
  }

  measure: duration_median {
    type: median
    sql: ${duration} ;;
    filters:  {
      field: end_date
      value: "-null"
    }
    #value_format_name: "decimal_2"
  }

  measure: total_log_outs {
    type: count
#     sql: ${action_item} ;;
    filters: {
      field: end_time
      value: "NULL"
    }
  }

#   measure: analyst_start_time {
#     sql: MIN(${start_time}) ;;
#     convert_tz: yes
#   }
}
