connection: "redshift"

# include all the views
include: "*.view"

# include all the dashboards
#include: "*.dashboard"

datagroup: etl_trigger {
  # trigger the rebuild of this table every hour.
  sql_trigger: SELECT DATE_PART('hour', GETDATE()) ;;
  #sql_trigger: SELECT COUNT(*) FROM public.rater_actions ;;
}

datagroup: etl_finished_trigger {
  sql_trigger: SELECT COUNT(*) FROM public.rater_actions ;;
}

explore: view_latest_reports {
  label: "Latest Reports"
  description: "This explore is filtered to show only the most recent report ran for each person in the system."

  join: reports {
    view_label: "B - Reports"
    type: inner
    sql_on: ${view_latest_reports.report_id} = ${reports.report_id}
    and ${reports.date_finished_raw} is not null
    and DATEDIFF(hour, reports.date_finished, GETDATE()) >= 1;;
    relationship: one_to_one
  }

  join: companies {
    view_label: "A - Companies"
    type: left_outer
    sql_on: ${companies.company_id} = ${reports.company_id} ;;
    relationship: one_to_many
  }

  join: profiles {
    view_label: "C - Profiles"
    type: left_outer
    sql_on: ${profiles.report_id} = ${reports.report_id}
      and ${profiles.deleted} = false ;;
    relationship: many_to_one
  }

  join: posts {
    view_label: "D - Posts"
    type: left_outer
    sql_on: ${posts.profile_id} = ${profiles.profile_id}
      and  ${posts.deleted} = false and ${posts.hidden} = false;;
    relationship: many_to_one
  }

  join: post_ratings {
    view_label: "E - Post Ratings"
    type: left_outer
    sql_on: ${post_ratings.post_id} = ${posts.post_id} ;;
    relationship: many_to_one
  }

  join: post_flags {
    view_label: "F - Post Flags"
    type: left_outer
    sql_on: ${post_flags.post_id} = ${posts.post_id} ;;
    relationship: many_to_one
  }

  join: articles {
    view_label: "G - Articles"
    type: left_outer
    sql_on: ${articles.report_id} = ${reports.report_id}
    and ${reports.date_finished_raw} is not null
    and DATEDIFF(hour, reports.date_finished, GETDATE()) > 1;;
    relationship: many_to_one
  }

  join: article_flags {
    view_label: "H - Article Flags"
    type: left_outer
    sql_on: ${article_flags.article_id} = ${articles.article_id}
    and ${articles.relevant};;
    relationship: many_to_one
  }

  join: flag_kits {
    view_label: "I - Flag Kits"
    type: left_outer
    sql_on: ${flag_kits.version_id} = ${reports.flag_kit_version_id} ;;
    relationship: many_to_one
  }


  join: report_flags {
    view_label: "J - Report Flags"
    type: left_outer
    sql_on: ${report_flags.report_id} = ${view_latest_reports.report_id}
    and ${reports.date_finished_raw} is not null
    and DATEDIFF(hour, reports.date_finished, GETDATE()) > 1;;
    relationship: many_to_one
  }

  join: users {
    view_label: "K - Users"
    type: left_outer
    sql_on: ${users.user_id} = ${reports.user_id} ;;
    relationship: one_to_many
  }
}

#Reports Explore
explore: companies {
  label: "Reports"
  description: "Main explore that uses companies as the main join table. This insures that companies without reports will still be accounted for."

  join: reports {
    type:  left_outer
    sql_on: ${reports.user_id} = ${users.user_id}
    and DATEDIFF(hour, reports.date_finished, GETDATE()) >= 1
    and ${reports.deleted} = false ;;
    relationship: many_to_one
  }

  join: report_addresses {
    type: left_outer
    sql_on: ${report_addresses.report_id} = ${reports.report_id}
    and ${reports.date_finished_raw} is not null
    and DATEDIFF(hour, reports.date_finished, GETDATE()) > 1 ;;
    relationship: many_to_one
  }

  join: report_aliases {
    type: left_outer
    sql_on: ${report_aliases.report_id} = ${reports.report_id}
    and ${reports.date_finished_raw} is not null
    and DATEDIFF(hour, reports.date_finished, GETDATE()) > 1 ;;
    relationship: many_to_one
  }

  join: report_degrees {
    type: left_outer
    sql_on: ${report_degrees.report_id} = ${reports.report_id}
    and ${reports.date_finished_raw} is not null
    and DATEDIFF(hour, reports.date_finished, GETDATE()) > 1 ;;
    relationship: many_to_one
  }

  join: report_emails {
    type: left_outer
    sql_on: ${report_emails.report_id} = ${reports.report_id}
    and ${reports.date_finished_raw} is not null
    and DATEDIFF(hour, reports.date_finished, GETDATE()) > 1 ;;
    relationship: many_to_one
  }

  join: report_jobs {
    type: left_outer
    sql_on: ${report_jobs.report_id} = ${reports.report_id}
    and ${reports.date_finished_raw} is not null
    and DATEDIFF(hour, reports.date_finished, GETDATE()) > 1 ;;
    relationship: many_to_one
  }

  join: report_phones {
    type: left_outer
    sql_on: ${report_phones.report_id} = ${reports.report_id}
    and ${reports.date_finished_raw} is not null
    and DATEDIFF(hour, reports.date_finished, GETDATE()) > 1 ;;
    relationship: many_to_one
  }

  join: articles {
    type: left_outer
    sql_on: ${articles.report_id} = ${reports.report_id}
    and ${reports.date_finished_raw} is not null
    and DATEDIFF(hour, reports.date_finished, GETDATE()) > 1 ;;
    relationship: many_to_one
  }

  join: article_ratings {
    type: left_outer
    sql_on: ${article_ratings.article_id} = ${articles.article_id} ;;
    relationship: many_to_one
  }

  join: profiles {
    type: left_outer
    sql_on: ${profiles.report_id} = ${reports.report_id}
    and ${profiles.deleted} = false
    and ${reports.date_finished_raw} is not null
    and DATEDIFF(hour, reports.date_finished, GETDATE()) > 1 ;;
    relationship: many_to_one
  }

  join: posts {
    type: left_outer
    sql_on: ${posts.profile_id} = ${profiles.profile_id}
    and  ${posts.deleted} = false and ${posts.hidden} = false;;
    relationship: many_to_one
  }

  join: post_ratings {
    type: left_outer
    sql_on: ${post_ratings.post_id} = ${posts.post_id} ;;
    relationship: many_to_one
  }

  join: post_flags {
    type: left_outer
    sql_on: ${post_flags.post_id} = ${posts.post_id} ;;
    relationship: many_to_one
  }

  join: post_media_entries {
    type: left_outer
    sql_on: ${post_media_entries.post_id} = ${posts.post_id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${users.company_id} = ${companies.company_id} ;;
    relationship: many_to_one
  }

  join: view_report_attribute_counts {
    view_label: "View - Report Attributes"
    type: inner
    sql_on: ${view_report_attribute_counts.report_id} = ${reports.report_id}
    and ${reports.date_finished_raw} is not null
    and DATEDIFF(hour, reports.date_finished, GETDATE()) > 1 ;;
    relationship: one_to_one
  }

  join: view_confirm_actions {
    type: left_outer
    sql_on: ${view_confirm_actions.report_id} = ${reports.report_id}
    and ${reports.date_finished_raw} is not null
    and DATEDIFF(hour, reports.date_finished, GETDATE()) > 1 ;;
    relationship: many_to_one
  }

  join: view_qa_actions {
    type: left_outer
    sql_on: ${view_qa_actions.report_id} = ${reports.report_id}
    and ${reports.date_finished_raw} is not null
    and DATEDIFF(hour, reports.date_finished, GETDATE()) > 1 ;;
    relationship: many_to_one
  }

  join: view_article_actions {
    type: left_outer
    sql_on: ${view_article_actions.article_id} = ${articles.article_id} ;;
    relationship: many_to_one
  }

  join: view_post_actions {
    type: left_outer
    sql_on: ${view_post_actions.post_id} = ${posts.post_id} ;;
    relationship: many_to_one
  }

  join: view_post_actions_meta_data {
    type: left_outer
    sql_on: ${view_post_actions_meta_data.post_id} = ${posts.post_id} ;;
    relationship: many_to_one
  }

  join: test_view_post_actions_meta_data{
    type:  left_outer
    sql_on: ${test_view_post_actions_meta_data.post_id} = ${posts.post_id} ;;
    relationship: many_to_one
  }

  join: view_profile_scrape_actions {
    type: left_outer
    sql_on: ${view_profile_scrape_actions.profile_id} = ${profiles.profile_id} ;;
    relationship: many_to_one
  }

  join: article_queries {
    type: left_outer
    sql_on: ${article_queries.article_id} = ${articles.article_id} ;;
    relationship: many_to_one
  }

  #Jordan Lincenberg, 12/14/17
  join: article_flags {
    type: left_outer
    sql_on: ${article_flags.article_id} = ${articles.article_id} ;;
    relationship: many_to_one
  }

  join: report_flags {
    type: left_outer
    sql_on: ${report_flags.report_id} = ${reports.report_id}
    and ${reports.date_finished_raw} is not null
    and DATEDIFF(hour, reports.date_finished, GETDATE()) > 1 ;;
    relationship: many_to_one
  }

  join: flag_kits {
    type: left_outer
    sql_on: ${flag_kits.version_id} = ${reports.flag_kit_version_id} ;;
    relationship: many_to_one
  }

  join: view_profile_flags_metrics {
    view_label: "View - Flagged Reports"
    type: inner
    sql_on: ${view_profile_flags_metrics.report_id} = ${reports.report_id}
    and ${reports.date_finished_raw} is not null
    and DATEDIFF(hour, reports.date_finished, GETDATE()) > 1 ;;
    relationship: one_to_one
  }

  join: view_auto_flagged_posts {
    view_label: "View - Auto Flagged Posts"
    type: left_outer
    sql_on: ${view_auto_flagged_posts.post_id} = ${posts.post_id}
    and ${reports.date_finished_raw} is not null
    and DATEDIFF(hour, reports.date_finished, GETDATE()) > 1 ;;
    relationship: many_to_one
  }

  join: view_report_flags {
    view_label: "View - Report Flags"
    type: inner
    sql_on: ${view_report_flags.report_id} = ${reports.report_id}
    and ${reports.date_finished_raw} is not null
    and DATEDIFF(hour, reports.date_finished, GETDATE()) > 1 ;;
    relationship: one_to_one
  }

  join: view_post_predictions {
    view_label: "Post Predictions"
    type: left_outer
    sql_on: ${view_post_predictions.post_id} = ${posts.post_id} ;;
    relationship: one_to_many
  }
}

explore: rater_actions {
  label: "Operations"

  join:  profiles{
    type: left_outer
    sql_on: ${rater_actions.action_item}= ${profiles.profile_id} ;;
    relationship: one_to_one
  }
  join: posts {
    type: left_outer
    sql_on: ${profiles.profile_id}=${posts.profile_id};;
    relationship: one_to_one
  }
  join: view_confirm_actions{
    type: left_outer
    sql_on: ${posts.report_id}=${view_confirm_actions.report_id}  ;;
    relationship: many_to_one
}
}

explore: flag_kits {
  hidden: yes
  label: "3 - Flag Kits"
  description: "Flag kit information"
  join: companies {
    type: left_outer
    sql_on: ${companies.company_id} = ${flag_kits.company_id} ;;
    relationship: one_to_many
  }
}

explore: posts {
  hidden: yes
  label: "Posts"
  join: post_flags {
    type: left_outer
    sql_on: ${post_flags.post_id} = ${posts.post_id} ;;
    relationship: many_to_one
  }

  join: reports {
    type: left_outer
    sql_on: ${posts.report_id} = ${reports.report_id}
    and ${reports.date_finished_raw} is not null
    and DATEDIFF(hour, reports.date_finished, GETDATE()) > 1 ;;
    relationship: many_to_one
  }

  join: profiles {
    type: left_outer
    sql_on: ${posts.profile_id} = ${profiles.profile_id} ;;
    relationship: many_to_one
  }

  join: companies {
    type: left_outer
    sql_on: ${reports.company_id} = ${companies.company_id} ;;
    relationship: many_to_one
  }

  join: view_post_actions {
    type:  left_outer
    sql_on: ${view_post_actions.post_id} = ${posts.post_id} ;;
    relationship: many_to_one
  }

  join: view_post_predictions_alcohol {
    type: left_outer
    sql_on: ${posts.post_id} = ${view_post_predictions_alcohol.post_id} ;;
    relationship: one_to_one
  }

  join: view_post_predictions_bigotry {
    type: left_outer
    sql_on: ${posts.post_id} = ${view_post_predictions_bigotry.post_id} ;;
    relationship: one_to_one
  }

  join: view_post_predictions_crime {
    type: left_outer
    sql_on: ${posts.post_id} = ${view_post_predictions_crime.post_id} ;;
    relationship: one_to_one
  }

  join: view_post_predictions_drugs {
    type: left_outer
    sql_on: ${posts.post_id} = ${view_post_predictions_drugs.post_id} ;;
    relationship: one_to_one
  }

  join: view_post_predictions_language {
    type: left_outer
    sql_on: ${posts.post_id} = ${view_post_predictions_language.post_id} ;;
    relationship: one_to_one
  }

  join: view_post_predictions_sex {
    type: left_outer
    sql_on: ${posts.post_id} = ${view_post_predictions_sex.post_id} ;;
    relationship: one_to_one
  }

  join: view_post_predictions_sexism {
    type: left_outer
    sql_on: ${posts.post_id} = ${view_post_predictions_sexism.post_id} ;;
    relationship: one_to_one
  }

  join: view_post_predictions_violence {
    type: left_outer
    sql_on: ${posts.post_id} = ${view_post_predictions_violence.post_id} ;;
    relationship: one_to_one
  }

}

explore: profiles {
  hidden: yes
  label: "1 - Profiles"
  description: "Only includes reports that have a profile."
  join: reports {
    type: left_outer
    sql_on: ${profiles.report_id} = ${reports.report_id}
    and ${reports.date_finished_raw} is not null
    and DATEDIFF(hour, reports.date_finished, GETDATE()) > 1 ;;
    relationship: many_to_one
  }

  join: companies {
    type: left_outer
    sql_on: ${reports.company_id} = ${companies.company_id} ;;
    relationship: many_to_one
  }
}

explore: rater_actions_unique {
  hidden:yes
  label: "Unique Rater Actions"
  description: "Excludes Post Re-Rates"
}

explore: view_post_actions {
  hidden:yes
  label: "View - Post Rater Actions"
  description: "This view only shows rater actions at the post level."
}

explore: users {
  hidden: yes
  join: companies {
    type: left_outer
    sql_on: ${users.company_id} = ${companies.company_id} ;;
    relationship: many_to_one
  }
}

explore: news_rating_manual_articles{
  hidden: yes
  join: reports {
    type: left_outer
    sql_on: ${news_rating_manual_articles.report_id} = ${reports.report_id}
    and ${reports.date_finished_raw} is not null
    and DATEDIFF(hour, reports.date_finished, GETDATE()) > 1 ;;
    relationship: many_to_one
  }
  join: articles {
    type: left_outer
    sql_on: ${news_rating_manual_articles.article_id} = ${articles.article_id} ;;
    relationship: many_to_one
  }
}

explore: predictions {
  hidden: yes

}
explore: post_audits {
  label: "Post Audits"
  view_name: companies
  extends: [companies]

  join: view_audit_profiles {
    type: inner
    sql_on: ${view_audit_profiles.profile_id} = ${profiles.profile_id} ;;
    relationship: one_to_one
  }
}

#explores and views used for profile flag analysis - BY
explore: view_audit_profiles {hidden:yes}
explore: view_post_predictions {hidden:yes}
explore: view_report_attribute_counts {hidden:yes}
explore: view_profile_flags {hidden:yes}
explore: view_profile_flags_metrics {hidden:yes}
explore: view_auto_flagged_posts {hidden:yes}
explore: view_report_times {hidden:yes}
explore: view_profile_count {
  hidden:yes
  view_label: "0 - View Profile Count"
#   join: view_latest_reports{
#     view_label: "3 - Latest Reports"
#     type: inner
#     sql_on: ${view_profile_count.report_id} = ${view_latest_reports.report_id} ;;
#     relationship: one_to_one
#   }

  join: reports {
    view_label: "2 - Reports"
    type: inner
    sql_on: ${view_profile_count.report_id} = ${reports.report_id}
    and ${reports.date_finished_raw} is not null
    and DATEDIFF(hour, reports.date_finished, GETDATE()) > 1 ;;
    relationship: one_to_one
  }

  join: companies {
    view_label: "3 - Companies"
    type: left_outer
    sql_on: ${companies.company_id} = ${reports.company_id} ;;
    relationship: many_to_one
  }

  join: view_profile_flags_metrics {
    view_label: "1 - View Profile Flag Metrics"
    type: left_outer
    sql_on: ${view_profile_flags_metrics.report_id} = ${reports.report_id}
    and ${reports.date_finished_raw} is not null
    and DATEDIFF(hour, reports.date_finished, GETDATE()) > 1 ;;
    relationship: many_to_one
  }
}
