view: predictions {
  sql_table_name: public.predictions ;;

#   derived_table: {
#     sql_trigger_value: etl_trigger ;;
#     distribution_style: all
#     sql:  select ROW_NUMBER() OVER (ORDER BY report_id, content_id, prediction_type), *
#           from public.predictions ;;
#   }

  dimension: pk {
    primary_key: yes
    hidden: no
    type: string
    sql: ${report_id} || ${content_id} ||${prediction_type} ;;
  }

  measure: predictions_count{
    type: count
    description: "counts the pk, which is report id + content id + prediction type"
  }

  dimension: report_id {
    type: string
    sql: ${TABLE}.report_id ;;
  }

  dimension: content_id {
    type: string
    sql: ${TABLE}.content_id ;;
  }

  dimension: model_type {
    type: string
    description: "post, post image, text post, news"
    sql: ${TABLE}.model_type ;;
  }

  dimension: name {
    type: string
    description: "name of the model"
    sql: ${TABLE}.name ;;
  }

  dimension: prediction_type {
    type: string
    description: "behavior + other news types"
    sql: ${TABLE}.prediction_type ;;
  }

  dimension: value {
    type: number
    sql: ${TABLE}.value ;;
  }

  dimension: threshold {
    type: number
    sql: ${TABLE}.threshold ;;
  }

  dimension: action {
    type: string
    description: "flag, review, skip"
    sql: ${TABLE}.action;;
    }


 # measure: count {
  #  type: count
 #   drill_fields: [report_id, content_id, model_type, name, prediction_type, value]}


}
