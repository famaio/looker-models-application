# If necessary, uncomment the line below to include explore_source.
# include: "customer_success.model.lkml"

view: view_reports_total_content {
  derived_table: {
    datagroup_trigger: etl_finished_trigger
    distribution_style: all
    explore_source: companies {
      column: report_id { field: reports.report_id }
      column: profile_count { field: profiles.count_distinct_type }
      column: twitter_profile_count { field: profiles.report_twitter_count }
      column: post_count { field: posts.count_distinct }
      column: twitter_post_count { field: posts.twitter_post_count }
      column: relevant_article_count { field: articles.relevant_count }
    }
  }
  dimension: report_id {
    primary_key: yes
    hidden: yes
    label: "Report ID"
  }
  dimension: profile_count {
    label: "Profile Count"
    type: number
  }
  dimension: twitter_profile_count {
    label: "Twitter Profile Count"
    type: number
  }
  dimension: post_count {
    label: "Post Count"
    type: number
  }
  dimension: twitter_post_count {
    label: "Twitter Post Count"
    type: number
  }
  dimension: relevant_article_count {
    label: "Relevant Article Count"
    type: number
  }
  dimension: has_content {
    label: "Has Content"
    type: yesno
    sql: CASE WHEN ${relevant_article_count} > 0 OR ${profile_count} > 0 THEN true
         ELSE false END;;
  }

  measure: reports_with_no_profiles {
    view_label: "Measures | Report Totals"
    label: "No Profiles"
    description: "This measure counts the number of reports that don't have a social media profile."
    type: count_distinct
    sql: ${report_id} ;;
    filters: {
      field: profile_count
      value: "0"
    }
  }

  measure: reports_with_no_profiles_and_news {
    view_label: "Measures | Report Totals"
    label: "News and No Profiles"
    description: "This measure counts the number of reports that have at least one confirmed news article and no social media profiles."
    type: count_distinct
    sql: ${report_id} ;;
    filters: {
      field: profile_count
      value: "0"
    }
    filters: {
      field: relevant_article_count
      value: ">0"
    }
  }

  measure: reports_with_private_profiles {
    view_label: "Measures | Report Totals"
    label: "Profiles and No Posts"
    description: "This measure counts the number of reports that have at least one profile and no posts."
    type: count_distinct
    sql: ${report_id} ;;
    filters: {
      field: profile_count
      value: ">0"
    }
    filters: {
      field: post_count
      value: "0"
    }
  }

  measure: reports_with_no_content {
    view_label: "Measures | Report Totals"
    label: "No Profiles and No Relevant Articles"
    description: "This measure counts the number of reports that have no flagged content (posts and articles)."
    type: count_distinct
    sql: ${report_id} ;;
    filters: {
      field: profile_count
      value: "0"
    }
    filters: {
      field: relevant_article_count
      value: "0"
    }
  }
  measure: reports_with_twitter_no_posts {
    view_label: "Measures | Report Totals"
    label: "Twitter Profile with No Posts"
    description: "This measure counts the number of reports that have a Twitter profile with no posts. Mean the profile didn't return any content."
    type: count_distinct
    sql: ${report_id} ;;
    filters: {
      field: twitter_profile_count
      value: ">0"
    }
    filters: {
      field: post_count
      value: "0"
    }
  }
  measure: reports_with_content {
    view_label: "Measures | Report Totals"
    label: "With Content"
    description: "This measure counts the number of reports that have either a profile or news article."
    type: count_distinct
    sql: ${report_id} ;;
    filters: {
      field: has_content
      value: "yes"
    }
  }
  measure: reports_with_twitter_profiles {
    view_label: "Measures | Report Totals"
    label: "With Twitter Profile"
    description: "This measure counts the number of reports that have a Twitter Profile."
    type: count_distinct
    sql: ${report_id} ;;
    filters: {
      field: twitter_profile_count
      value: ">0"
    }
  }
}
