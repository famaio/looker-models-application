
view: view_profile_scrape_actions{
  view_label: "Rater Actions - Profile Scrapes"
  derived_table: {
    sql_trigger_value:  SELECT COUNT(*) FROM public.rater_actions ;;
    distribution_style: all
    explore_source: rater_actions {
      column: report_id {field: rater_actions.action_item}
      column: action_type {}
      column: start_time {field: rater_actions.start_raw}
      column: end_time {field: rater_actions.end_raw}
      column: duration {field: rater_actions.duration}
      #column: duration_minutes {field: rater_actions.duration_minutes}
      column: rater_id {}
      column: username {}
      column: masked_username {}
      filters: {
        field: rater_actions.action_type
        value: "PROFILE^_MANUAL^_INSTAGRAM^_ADD, PROFILE^_MANUAL^_FACEBOOK^_ADD"
        }
    }
  }

  dimension: profile_id {
    primary_key: yes
  }
  dimension: action_type {}
  dimension: duration {
    type: number
  }
  dimension: duration_minutes {
    type: number
  }
  dimension: rater_id {}
  dimension: username {}


  dimension_group: start {
    type: time
    timeframes: [
      raw,
      time,
      date,
      day_of_week,
      week,
      month,
      quarter,
      year,
      hour_of_day,
      hour
    ]
    sql: ${TABLE}.start_time ;;
  }

  dimension_group: end {
    type: time
    timeframes: [
      raw,
      time,
      date,
      day_of_week,
      week,
      month,
      quarter,
      year,
      hour_of_day,
      hour
    ]
    sql: ${TABLE}.end_time ;;
  }

  measure: profile_scrape_count {
    type: count
    filters: {
      field: end_time
      value: "-null"
    }
  }

  measure: scrapes_longer_than_20_mintes {
    label: "Total Profile Scrapes Longer than 20 Minutes"
    type: count
    filters: {
      field: duration
      value: ">1200"
    }
  }

  measure: sum_duration {
    label: "Total Profile Scrape Seconds"
    type: sum
    sql: ${duration} ;;
  }

  measure: sum_duration_minutes {
    label: "Total Profile Scrape Minutes"
    type: sum
    sql: ${duration_minutes} ;;
  }
}
