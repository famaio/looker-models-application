view: view_audit_profiles {
  derived_table: {
    datagroup_trigger: etl_trigger
    distribution_style: all
#     indexes: ["report_id", "company_id", "flag_kit_version_id","user_id","date_finished", "date_created"]
    #sortkeys: ["date_finished", "date_created"]
    sql:
      select profiles.profile_id, 1 as "audit", reports.date_finished as "report_ran_date"
      from profiles
      left join reports on reports.report_id = profiles.report_id
      left join companies on companies.company_id = reports.company_id
      where profiles.url IN (
        select distinct(url) as "URL"
        from profiles
        left join reports on reports.report_id = profiles.report_id
        left join companies on companies.company_id = reports.company_id
        where companies.lower_case_name = 'ops team testing'
        and reports.first_name = 'Twitter'
      )
      ;;
  }

  # Define your dimensions and measures here, like this:
  dimension: profile_id {
    primary_key: yes
    description: "Profile Url of an audited twitter profile."
    type: string
    sql: ${TABLE}.profile_id ;;
  }

  dimension: audit {
    type: yesno
    sql: CASE WHEN ${TABLE}.audit = 1 THEN true
         ELSE false END;;
  }

  dimension: report_ran_date {
    type: date
    sql: ${TABLE}.report_ran_date;;
  }
}
