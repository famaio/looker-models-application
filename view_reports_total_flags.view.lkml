# If necessary, uncomment the line below to include explore_source.
# include: "customer_success.model.lkml"

view: view_reports_total_flags {
  derived_table: {
#     datagroup_trigger: etl_trigger
    sql_trigger_value: SELECT (select count(*) from ${view_reports_total_post_flags.SQL_TABLE_NAME}) + (select count(*) from ${view_reports_total_article_flags.SQL_TABLE_NAME}) as "Count" ;;
    distribution_style: all
    explore_source: companies {
      column: report_id { field: reports.report_id }
      column: posts_bad_flag_count { field: view_reports_total_post_flags.posts_bad_flag_count }
      column: posts_good_flag_count { field: view_reports_total_post_flags.posts_good_flag_count }
      column: posts_keyword_flag_count { field: view_reports_total_post_flags.posts_keyword_flag_count }
      column: articles_bad_flag_count { field: view_reports_total_article_flags.articles_bad_flag_count }
      column: articles_good_flag_count { field: view_reports_total_article_flags.articles_good_flag_count }
      column: articles_keyword_flag_count { field: view_reports_total_article_flags.articles_keyword_flag_count }
      column: articles_keyword_flag_count { field: view_reports_total_article_flags.articles_keyword_flag_count }

      # Flag Total Columns Used for Calculations
      column: posts_alcohol_flag_count { field: view_reports_total_post_flags.posts_alcohol_flag_count }
      column: posts_bigotry_flag_count { field: view_reports_total_post_flags.posts_bigotry_flag_count }
      column: posts_crime_flag_count { field: view_reports_total_post_flags.posts_crime_flag_count }
      column: posts_drugs_flag_count { field: view_reports_total_post_flags.posts_drugs_flag_count }
      column: posts_language_flag_count { field: view_reports_total_post_flags.posts_language_flag_count }
      column: posts_sex_flag_count { field: view_reports_total_post_flags.posts_sex_flag_count }
      column: posts_sexism_flag_count { field: view_reports_total_post_flags.posts_sexism_flag_count }
      column: posts_threat_flag_count { field: view_reports_total_post_flags.posts_threat_flag_count }
      column: posts_harassment_flag_count { field: view_reports_total_post_flags.posts_harassment_flag_count }
      column: posts_cannabis_flag_count { field: view_reports_total_post_flags.posts_cannabis_flag_count }
      column: posts_other_drugs_flag_count { field: view_reports_total_post_flags.posts_other_drugs_flag_count }
      column: posts_violence_flag_count { field: view_reports_total_post_flags.posts_violence_flag_count }
      column: posts_intolerance_flag_count { field: view_reports_total_post_flags.posts_intolerance_flag_count }

      column: posts_community_flag_count { field: view_reports_total_post_flags.posts_community_flag_count }
      column: posts_donation_flag_count { field: view_reports_total_post_flags.posts_donation_flag_count }
      column: posts_helping_others_flag_count { field: view_reports_total_post_flags.posts_helping_others_flag_count }
      column: posts_protecting_others_flag_count { field: view_reports_total_post_flags.posts_protecting_others_flag_count }
      column: posts_volunteering_flag_count { field: view_reports_total_post_flags.posts_volunteering_flag_count }

      column: articles_alcohol_flag_count { field: view_reports_total_article_flags.articles_alcohol_flag_count }
      column: articles_bigotry_flag_count { field: view_reports_total_article_flags.articles_bigotry_flag_count }
      column: articles_threat_flag_count { field: view_reports_total_article_flags.articles_threat_flag_count }
      column: articles_harassment_flag_count { field: view_reports_total_article_flags.articles_harassment_flag_count }
      column: articles_crime_flag_count { field: view_reports_total_article_flags.articles_crime_flag_count }
      column: articles_drugs_flag_count { field: view_reports_total_article_flags.articles_drugs_flag_count }
      column: articles_language_flag_count { field: view_reports_total_article_flags.articles_language_flag_count }
      column: articles_sex_flag_count { field: view_reports_total_article_flags.articles_sex_flag_count }
      column: articles_sexism_flag_count { field: view_reports_total_article_flags.articles_sexism_flag_count }
      column: articles_violence_flag_count { field: view_reports_total_article_flags.articles_violence_flag_count }
      column: articles_cannabis_flag_count { field: view_reports_total_article_flags.articles_cannabis_flag_count }
      column: articles_other_drugs_flag_count { field: view_reports_total_article_flags.articles_other_drugs_flag_count }
      column: articles_intolerance_flag_count { field: view_reports_total_article_flags.articles_intolerance_flag_count }

      column: articles_donation_flag_count { field: view_reports_total_article_flags.articles_donation_flag_count }
      column: articles_helping_others_flag_count { field: view_reports_total_article_flags.articles_helping_others_flag_count }
      column: articles_protecting_others_flag_count { field: view_reports_total_article_flags.articles_protecting_others_flag_count }
      column: articles_volunteering_flag_count { field: view_reports_total_article_flags.articles_volunteering_flag_count }
      column: articles_community_flag_count { field: view_reports_total_article_flags.articles_community_flag_count }

      #Derived Total Columns

      #Bad
      derived_column: total_alcohol_flags {sql:posts_alcohol_flag_count + articles_alcohol_flag_count;;}
      derived_column: total_bigotry_flags {sql:posts_bigotry_flag_count + articles_bigotry_flag_count;;}
      derived_column: total_crime_flags {sql:posts_crime_flag_count + articles_crime_flag_count;;}
      derived_column: total_drugs_flags {sql:posts_drugs_flag_count + articles_drugs_flag_count;;}
      derived_column: total_language_flags {sql:posts_language_flag_count + articles_language_flag_count;;}
      derived_column: total_sex_flags {sql:posts_sex_flag_count + articles_sex_flag_count;;}
      derived_column: total_sexism_flags {sql:posts_sexism_flag_count + articles_sexism_flag_count;;}
      derived_column: total_violence_flags {sql:posts_violence_flag_count + articles_violence_flag_count;;}
      derived_column: total_threat_flags {sql:posts_threat_flag_count + articles_threat_flag_count;;}
      derived_column: total_harassment_flags {sql:posts_harassment_flag_count + articles_harassment_flag_count;;}
      derived_column: total_cannabis_flags {sql:posts_cannabis_flag_count + articles_cannabis_flag_count;;}
      derived_column: total_other_drugs_flags {sql:posts_other_drugs_flag_count + articles_other_drugs_flag_count;;}
      derived_column: total_intolerance_flags {sql:posts_intolerance_flag_count + articles_intolerance_flag_count;;}




      #Good
      derived_column: total_donation_flags {sql:posts_donation_flag_count + articles_donation_flag_count;;}
      derived_column: total_helping_others_flags {sql:posts_helping_others_flag_count + articles_helping_others_flag_count;;}
      derived_column: total_protecting_others_flags {sql:posts_protecting_others_flag_count + articles_protecting_others_flag_count;;}
      derived_column: total_volunteering_flags {sql:posts_volunteering_flag_count + articles_volunteering_flag_count;;}
      derived_column: total_community_flags {sql:posts_community_flag_count + articles_community_flag_count;;}


#       derived_column: total_article_flags {sql:${view_reports_total_posts_flags.SQL_TABLE_NAME}.posts_alcohol_flag_count + ${view_reports_total_articles_flags.SQL_TABLE_NAME}.articles_alcohol_flag_count;;}
    }
  }
  dimension: report_id {
    primary_key: yes
    hidden: yes
    label: "Report Id"
    type: string
  }
  # Total Flag Counts
  dimension: total_alcohol_flags {
    label: "Bad | Alcohol "
    description: "Total Alcohol Flags (Posts + Articles)"
    type: number
  }
  dimension: total_bigotry_flags {
    label: "Bad | Bigotry "
    description: "Total Bigotry Flags (Posts + Articles)"
    type:number
  }
  dimension: total_crime_flags {
    label: "Bad | Crime "
    description: "Total Crime Flags (Posts + Articles)"
    type:number
  }
  dimension: total_drugs_flags {
    label: "Bad | Drugs "
    description: "Total Drugs Flags (Posts + Articles)"
    type:number
  }
  dimension: total_language_flags {
    label: "Bad | Language "
    description: "Total Language Flags (Posts + Articles)"
    type:number
  }
  dimension: total_sex_flags {
    label: "Bad | Sex "
    description: "Total Sex Flags (Posts + Articles)"
    type:number
  }
  dimension: total_sexism_flags {
    label: "Bad | Sexism "
    description: "Total Sexism Flags (Posts + Articles)"
    type:number
  }
  dimension: total_violence_flags {
    label: "Bad | Violence "
    description: "Total Violence Flags (Posts + Articles)"
    type:number
  }
  dimension: total_threat_flags {
    label: "Bad | Threat "
    description: "Total Threat Flags (Posts + Articles)"
    type:number
  }
  dimension: total_harassment_flags {
    label: "Bad | Harassment "
    description: "Total Harassment Flags (Posts + Articles)"
    type:number
  }
  dimension: total_other_drugs_flags {
    label: "Bad | Other Drugs "
    description: "Total Other Drugs Flags (Posts + Articles)"
    type: number
  }
  dimension: total_cannabis_flags {
    label: "Bad | Cannabis "
    description: "Total Cannabis Flags (Posts + Articles)"
    type: number
  }

  dimension: total_intolerance_flags {
    label: "Bad | Intolerance "
    description: "Total Intolerance Flags (Posts + Articles)"
    type:number
  }

  #Good
  dimension: total_donation_flags {
    label: "Good | Donation "
    description: "Total Donation Flags (Posts + Articles)"
    type:number
  }
  dimension: total_helping_others_flags {
    label: "Good | Helping Others "
    description: "Total Helping Others Flags (Posts + Articles)"
    type:number
  }
  dimension: total_protecting_others_flags {
    label: "Good | Protecting Others "
    description: "Total Protecting Others Flags (Posts + Articles)"
    type:number
  }
  dimension: total_volunteering_flags {
    label: "Good | Volunteering "
    description: "Total Volunteering Flags (Posts + Articles)"
    type:number
  }

  dimension: total_community_flags {
    label: "Good | Community "
    description: "Total Community Flags (Posts + Articles)"
    type:number
  }

  dimension: posts_bad_flag_count {
    hidden: yes
    label: "Posts | Total | Bad"
    type: number
  }
  dimension: posts_good_flag_count {
    hidden: yes
    label: "Posts | Total | Good"
    type: number
  }
  dimension: posts_keyword_flag_count {
    hidden: yes
    label: "Posts | Total | Keyword"
    type: number
  }
  dimension: articles_bad_flag_count {
    hidden: yes
    label: "Articles | Total | Bad"
    type: number
  }
  dimension: articles_good_flag_count {
    hidden: yes
    label: "Articles | Total | Good"
    type: number
  }
  dimension: articles_keyword_flag_count {
    hidden: yes
    label: "Articles | Total | Keywords"
    type: number
  }
  dimension: total_bad_flag_count {
    label: "Total | Bad"
    description: "Total Bad Flags (Posts + Articles)"
    type: number
    sql: ${posts_bad_flag_count}+${articles_bad_flag_count} ;;
  }
  dimension: total_good_flag_count {
    label: "Total | Good"
    description: "Total Good Flags (Posts + Articles)"
    type: number
    sql: ${posts_good_flag_count}+${articles_good_flag_count} ;;
  }
  dimension: total_keyword_flag_count {
    label: "Total | Keywords"
    description: "Total Keywords (Posts + Articles)"
    type: number
    sql: ${posts_keyword_flag_count}+${articles_keyword_flag_count} ;;
  }
  dimension: total_flag_count {
    label: "Total | Flags"
    description: "Total Overall Flags (Posts + Articles)"
    type: number
    sql: ${total_bad_flag_count}+${total_good_flag_count} ;;
  }
  dimension: really_bad_report {
    type: yesno
    sql: CASE WHEN ${total_bigotry_flags}>0 OR ${total_drugs_flags} > 0 OR ${total_crime_flags}>0 OR ${total_violence_flags}>0 THEN true
         ELSE false END;;
  }

  measure: total_bad_flags {
    type: sum
    sql: ${total_bad_flag_count} ;;
  }
  measure: total_good_flags {
    type: sum
    sql: ${total_good_flag_count} ;;
  }
  measure: average_bad_flags {
    type: average
    sql: ${total_bad_flag_count} ;;
    value_format: "0.00"
    filters: {
      field: total_flag_count
      value: ">0"
    }
  }
  measure: average_good_flags {
    type: average
    sql: ${total_good_flag_count} ;;
    value_format: "0.00"
    filters: {
      field: total_flag_count
      value: ">0"
    }
  }

  measure: reports_with_no_flagged_content {
    view_label: "Measures | Report Totals"
    label: "No Flagged Content"
    description: "This measure counts the number of reports that have no flagged content (posts and articles)."
    type: count_distinct
    sql: ${report_id} ;;
    filters: {
      field: total_flag_count
      value: "0"
    }
  }

  measure: reports_with_bad_flagged_content {
    view_label: "Measures | Report Totals"
    label: "Bad Flagged Content"
    description: "This measure counts the number of reports that have at least one bad flag on the report (posts and articles)."
    type: count_distinct
    sql: ${report_id} ;;
    filters: {
      field: total_bad_flag_count
      value: ">0"
    }
  }

  measure: reports_with_good_flagged_content {
    view_label: "Measures | Report Totals"
    label: "Good Flagged Content"
    description: "This measure counts the number of reports that have at least one good flag on the report (posts and articles)."
    type: count_distinct
    sql: ${report_id} ;;
    filters: {
      field: total_good_flag_count
      value: ">0"
    }
  }

  measure: reports_with_really_bad_flagged_content {
    view_label: "Measures | Report Totals"
    label: "Really Bad Flagged Content"
    description: "This measure counts the number of reports that have at least one crime, drugs, bigotry flag on the report (posts and articles)."
    type: count_distinct
    sql: ${report_id} ;;
    filters: {
      field: really_bad_report
      value: "YES"
    }
  }
}
