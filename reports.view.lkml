view: reports {
  sql_table_name: public.reports ;;

#   derived_table: {
#     distribution_style: all
#     sql: select * from public.reports ;;
#     datagroup_trigger: etl_trigger
#     indexes: ["report_id", "person_id","date_created","date_finished"]
#   }

  dimension: report_id {
    primary_key: yes
    type: string
    sql: ${TABLE}.report_id ;;
  }

  dimension: deleted {
    type: yesno
    sql: ${TABLE}.deleted ;;
  }

  dimension: review_status {
    type: string
    sql: ${TABLE}.review_status
    ;;
  }

  dimension: report_link {
    type: string
    sql: 'https://web.fama.io/#/person/' || ${TABLE}.report_id || '/report' ;;
  }

  dimension: company_id {
    type: string
    sql: ${TABLE}.company_id ;;
  }

  dimension_group: date_created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      day_of_week,
      hour_of_day,
      week,
      month,
      month_name,
      quarter,
      year,
      time_of_day
    ]
    sql: ${TABLE}.date_created ;;
  }

  dimension_group: date_finished {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      month_name,
      quarter,
      year,
      day_of_week,
      time_of_day,
      hour_of_day,
    ]
    sql: ${TABLE}.date_finished ;;
  }

  dimension: friday_weekend_report {
    label: "Friday to Monday Report"
    hidden: yes
    type: number
    sql: CASE WHEN ${date_created_day_of_week} = 'Friday' AND ${date_finished_day_of_week} = 'Monday' THEN 1 ELSE 0 END  ;;
  }

  dimension: saturday_weekend_report {
    label: "Saturday to Monday Report"
    hidden: yes
    type: number
    sql: CASE WHEN ${date_created_day_of_week} = 'Saturday' AND ${date_finished_day_of_week} = 'Monday' THEN 1 ELSE 0 END  ;;
  }

  dimension: sunday_weekend_report {
    label: "Sunday to Monday Report"
    hidden: yes
    type: number
    sql: CASE WHEN ${date_created_day_of_week} = 'Sunday' AND ${date_finished_day_of_week} = 'Monday' THEN 1 ELSE 0 END  ;;
  }

  dimension: report_run_duration {
    type: number
    hidden: yes
    sql:
      CASE
      WHEN ${date_finished_date} != '1970-01-01' THEN (datediff('seconds', ${date_created_raw}, ${date_finished_raw}))
      END;;
    value_format:"0.00"
  }

  dimension_group: suspended_time {
    type: duration
    group_label: "Report run duration"
    sql_start: ${date_created_raw} ;;
    sql_end:${date_finished_raw}  ;;
    intervals: [hour, minute, second]
  }

  dimension: seconds_duration_formatted {
    type: number
    label: "Report run duration - HH:MM:SS"
    sql: ${seconds_suspended_time}/86400.0 ;;
    value_format: "[hh]:mm:ss"
  }
  #WHEN ${date_finished_date} != '1970-01-01' AND ${saturday_weekend_report} = 1 THEN datediff('seconds', ${date_created_raw}, ${date_finished_raw})-172800
  #WHEN ${date_finished_date} != '1970-01-01' AND ${sunday_weekend_report} = 1 THEN datediff('seconds', ${date_created_raw}, ${date_finished_raw})-86400

  dimension: first_name {
    type: string
    sql: ${TABLE}.first_name ;;
  }

  dimension: middle_name {
    type: string
    sql: ${TABLE}.middle_name ;;
  }

  dimension: last_name {
    type: string
    sql: ${TABLE}.last_name ;;
  }

  dimension: full_name {
    type: string
    sql: CASE WHEN ${middle_name} = '' THEN ${first_name} + ' ' + ${last_name}
         ELSE ${first_name} + ' ' + ${last_name} END;;
  }

  dimension: person_id {
    type: string
    sql: ${TABLE}.person_id ;;
  }

  dimension: role {
    type: string
    sql: ${TABLE}.role ;;
  }

  dimension: flag_kit_version_id {
    type: string
    sql: ${TABLE}.flag_kit_version_id ;;
  }

  dimension: user_id {
    type: string
    # hidden: yes
    sql: ${TABLE}.user_id ;;
  }

  dimension: investigator{
    type: yesno
    sql: ${TABLE}.investigator ;;
  }

  dimension: visible_priority {
    type: string
    sql: ${TABLE}.visible_priority ;;
  }

  dimension: roll_over_report {
    type: yesno
    description: "Roll over report means the report was not finished the same day it was added."
    sql: CASE WHEN ${date_created_day_of_week} != ${date_finished_day_of_week} THEN 1 ELSE 0 END ;;
  }

  dimension: randomizer {
    description: "Sort on this column to randomize your query"
    type: number
    sql: RANDOM() ;;
  }

  measure: most_recent_report {
    type: yesno
    sql: CASE WHEN ${reports_count_distinct} > 1 AND ${most_recent_report_created} = ${date_created_time} THEN true
              WHEN ${reports_count_distinct} = 1 THEN true
              ELSE false END;;
  }

  measure: has_profiles {
    type: yesno
    sql: CASE WHEN profiles.count > 0 THEN true
              ELSE false END;;
  }

#   measure: people_count {
#     type: count
#     sql: ${person_id} ;;
#   }

  measure : most_recent_report_created {
#     type: date
    sql: MAX(${date_created_date}) ;;
#     convert_tz: no
  }

  measure: average_report_duration {
    type: average
    sql: ${report_run_duration} ;;
    value_format_name: "decimal_2"
    drill_fields: [report_details*]
  }

  measure: reports_count_distinct {
    label: "Reports Processed"
    description: "The number of reports (not screens) that have been fully processed."
    type: count_distinct
    sql:  ${report_id} ;;
    drill_fields: [detail*]
  }

  measure: count_distinct {
    label: "People Processed"
    description: "The number of people (not reports) that have been fully processed at least once."
    type: count_distinct
    sql:  ${person_id} ;;
    drill_fields: [detail*]
  }

  measure: count_report_updates_flag_kit {
    label: "Reports for Flag Kit Update"
    description: "The number of reports that have been updated due to a flag kit update."
    type: number
    sql: count(${report_id}) - count(DISTINCT ${person_id}) ;;
    drill_fields: [detail*]
  }

  #Brian Yardley - 12/18/17
  measure: sum_report_duration {
    type: sum
    hidden: yes
    sql:  ${report_run_duration} ;;
  }
#jessica
   measure: missed_weekday_SLA_count {
    type: count
    hidden: yes
    filters: {
      field: report_run_duration
      value: ">86400"
    }
    filters: {
      field: date_created_day_of_week
      value: "Monday,Tuesday,Wednesday,Thursday"
    }
  }

  measure: missed_friday_SLA_count {
    type: count
    hidden: yes
    filters: {
      field: report_run_duration
      value: ">259200"
    }
    filters: {
      field: date_created_day_of_week
      value: "Friday"
    }
  }

# jessica new SLA measures
#5 deadlines cases
dimension: due_EOD_monday {
  type: yesno
  group_label: "Weekday - SLA deadline"
 # group_item_label: "Monday"
  description: "choose yes for SLA deadline and yes SLA deadline missed for specific weekdays"
  sql: (${date_created_day_of_week}='Thursday' AND ${date_created_time_of_day} > '10:00')
       OR (${date_created_day_of_week}='Friday' AND ${date_created_time_of_day} < '10:00') ;;
  }
dimension: due_EOD_tuesday {
  type: yesno
  group_label: "Weekday - SLA deadline"
 # group_item_label: "Tuesday"
  sql: ${date_created_day_of_week} = 'Saturday'
        OR ${date_created_day_of_week} = 'Sunday'
        OR (${date_created_day_of_week}='Monday' AND ${date_created_time_of_day} < '10:00')
        OR (${date_created_day_of_week}='Friday' AND ${date_created_time_of_day}>'10:00');;
}
  dimension: due_EOD_wednesday {
    type: yesno
    group_label: "Weekday - SLA deadline"
   # group_item_label: "Wednesday"
    sql: (${date_created_day_of_week}='Monday' AND ${date_created_time_of_day} > '10:00')
      OR (${date_created_day_of_week}='Tuesday' AND ${date_created_time_of_day} < '10:00') ;;
  }

dimension: due_EOD_thursday {
  type: yesno
  group_label: "Weekday - SLA deadline"
 # group_item_label: "Thursday"
  sql: (${date_created_day_of_week}='Tuesday' AND ${date_created_time_of_day} > '10:00')
      OR (${date_created_day_of_week}='Wednesday' AND ${date_created_time_of_day} < '10:00') ;;
  }

  dimension: due_EOD_friday {
    type: yesno
    group_label: "Weekday - SLA deadline"
    #group_item_label: "Friday"
    sql: (${date_created_day_of_week}='Wednesday' AND ${date_created_time_of_day} > '10:00')
      OR (${date_created_day_of_week}='Thursday' AND ${date_created_time_of_day} < '10:00') ;;
  }



#5 deadline completed cases (based on day of week and time)
#  dimension: completed_EOD_tuesday {
#    type: yesno
#    group_label: "SLA completed status, select yes"
#    sql:
#     (${date_finished_day_of_week}='Tuesday' AND ${date_finished_time_of_day}>'14:00');;
#  }

#  dimension: completed_EOD_monday {
#    type: yesno
#    group_label: "SLA completed status, select yes"
#    sql: (${date_finished_day_of_week}='Monday' AND ${date_finished_time_of_day}>'14:00') ;;
#  }

#  dimension: completed_EOD_friday {
#   type: yesno
#    group_label: "SLA completed status, select yes"
#    sql: (${date_finished_day_of_week}='Friday' AND ${date_finished_time_of_day}>'14:00') ;;
#  }

#  dimension: completed_EOD_thursday {
#    type: yesno
#    group_label: "SLA completed status, select yes"
#    sql: (${date_finished_day_of_week}='Thursday' AND ${date_finished_time_of_day}>'14:00') ;;
#  }

#  dimension: completed_EOD_wednesday {
#    type: yesno
#    group_label: "SLA completed status, select yes"
#    sql: (${date_finished_day_of_week}='Wednesday' AND ${date_finished_time_of_day}>'14:00') ;;
#  }

#5 deadline completed cases (based on report run duration

  dimension: completed_EOD_monday {
    group_label: "Weekday - SLA deadline missed"
    label: "Monday"
    description: "choose yes for SLA deadline and yes SLA deadline missed for specific weekdays"
    type: yesno
    sql:(${report_run_duration}>360000)
    OR (${date_finished_day_of_week}='Monday' AND ${date_finished_time_of_day}>='14:00')
    OR (${date_finished_day_of_week}='Tuesday' AND ${date_finished_time_of_day}<='14:00')
    ;;
  }

  dimension: completed_EOD_tuesday {
    group_label: "Weekday - SLA deadline missed"
    label: "Tuesday"
    type: yesno
    sql:${report_run_duration}>360000
    OR (${date_finished_day_of_week}='Tuesday' AND ${date_finished_time_of_day}>='14:00')
    OR (${date_finished_day_of_week}='Wednesday' AND ${date_finished_time_of_day}<='14:00')
   ;;
    #the max time for an EOD tuesday report is 96 + 4 hours = 36000 seconds
  }

  dimension: completed_EOD_wednesday {
    group_label: "Weekday - SLA deadline missed"
    label: "Wednesday"
    type: yesno
    sql: ${report_run_duration}>187200
    OR (${date_finished_day_of_week}='Wednesday' AND ${date_finished_time_of_day}>='14:00')
    OR (${date_finished_day_of_week}='Thursday' AND ${date_finished_time_of_day}<='14:00');;
  }

  dimension: completed_EOD_thursday {
    group_label: "Weekday - SLA deadline missed"
    label: "Thursday"
    type: yesno
    sql: ${report_run_duration}>187200
    OR (${date_finished_day_of_week}='Thursday' AND ${date_finished_time_of_day}>='14:00')
    OR (${date_finished_day_of_week}='Friday' AND ${date_finished_time_of_day}<='14:00')
   ;;
  }

  dimension: completed_EOD_friday {
    type: yesno
    group_label: "Weekday - SLA deadline missed"
    label: "Friday"
    #max time for EOD friday is 48 + 4 = 187200 seconds
    sql: ${report_run_duration}>187200
    OR (${date_finished_day_of_week}='Friday' AND ${date_finished_time_of_day}>='14:00')
    OR (${date_finished_day_of_week}='Saturday' AND ${date_finished_time_of_day}<='14:00');;
  }


dimension: sla_missed_status {
  type: yesno
  label: "Missed SLA status"
  description: "Missed SLA status without specifying weekday"
  sql: (${due_EOD_monday} AND ${completed_EOD_monday})
  OR (${due_EOD_tuesday} AND ${completed_EOD_tuesday})
  OR (${due_EOD_wednesday} AND ${completed_EOD_wednesday})
  OR (${due_EOD_thursday} AND ${completed_EOD_thursday})
  OR (${due_EOD_friday} AND ${completed_EOD_friday});;
}


  measure: reports_processed_without_news {
    label: "Reports Processed Without News"
    type: count_distinct
    sql: ${report_id} ;;
    # spencer stuart, wag
    filters: {
      field: company_id
      value: "2d037f10-2bbc-11e8-94f8-c32cffe5d750, bf5eb7e0-5b4b-11e7-3b81-be921fb99b56"
    }
  }

  measure: total_roll_over_count {
    type: count
    filters: {
      field: roll_over_report
      value: "yes"
    }
  }

#   measure: latest_test {
#     type: count
#     filters: {
#       field: view_latest_reports.latest_report
#       value: "no"
#     }
#   }

#   measure: reports_with_no_profiles {
#     label: " Total Reports with No Profiles"
#     description: "This measure counts the number of reports that don't have a social media profile."
#     type: count_distinct
#     sql: ${report_id} ;;
#     filters: {
#       field: view_reports_total_content.profile_count
#       value: "0"
#     }
#   }
#
#   measure: reports_with_no_profiles_and_news {
#     label: " Total Reports with News and No Profiles"
#     description: "This measure counts the number of reports that have at least one confirmed news article and no social media profiles."
#     type: count_distinct
#     sql: ${report_id} ;;
#     filters: {
#       field: view_reports_total_content.profile_count
#       value: "0"
#     }
#     filters: {
#       field: view_reports_total_content.relevant_article_count
#       value: ">0"
#     }
#   }
#
#   measure: reports_with_no_flagged_content {
#     label: " Total Reports with No Flagged Content"
#     description: "This measure counts the number of reports that have no flagged content (posts and articles)."
#     type: count_distinct
#     sql: ${report_id} ;;
#     filters: {
#       field: view_reports_total_flags.total_flag_count
#       value: "0"
#     }
#   }
#
#   measure: reports_with_no_content {
#     label: " Total Reports with No Social Media Profiles and No Relevant News Articles"
#     description: "This measure counts the number of reports that have no flagged content (posts and articles)."
#     type: count_distinct
#     sql: ${report_id} ;;
#     filters: {
#       field: view_reports_total_content.profile_count
#       value: "0"
#     }
#     filters: {
#       field: view_reports_total_content.relevant_article_count
#       value: "0"
#     }
#   }




#   measure: total_risk_reports {
#     type: count
#     filters: {
#       field: risk
#       value: "yes"
#     }
#   }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      report_id,
      first_name,
      middle_name,
      last_name,
      users.username,
      users.user_id,
      articles.count,
      post_topics.count,
      posts.count,
      profiles.count,
      report_addresses.count,
      report_aliases.count,
      report_degrees.count,
      report_emails.count,
      report_jobs.count,
      report_phones.count
    ]
  }

  set: report_details {
    fields: [
      date_created_time,
      date_finished_time,
      report_run_duration,
      company_id,
      first_name,
      last_name
    ]
  }
}
