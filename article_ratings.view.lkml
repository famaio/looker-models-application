view: article_ratings {
  sql_table_name: public.article_ratings ;;

  dimension: pk {
    primary_key: yes
    hidden: yes
    type: string
    sql: ${article_id} || ${rating_id} ;;
  }

  dimension: article_id {
    type: string
    # hidden: yes
    sql: ${TABLE}.article_id ;;
  }

  dimension: rating_id {
    type: string
    sql: ${TABLE}.rating_id ;;
  }

  dimension: user_id {
    type: string
    # hidden: yes
    sql: ${TABLE}.user_id ;;
  }

  dimension: relevant {
    type: string
    # hidden: yes
    sql: ${TABLE}.relevant ;;
  }

  measure: count {
    type: count
    drill_fields: [articles.article_id, users.user_id, users.username]
  }
}
