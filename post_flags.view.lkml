view: post_flags {
  sql_table_name: public.post_flags ;;

# Looker needs one dimension for the primary key. We get this by concatenating post_id, flag_type, and flag
  dimension: pk {
    primary_key: yes
    hidden: yes
    sql: ${TABLE}.post_id || '-' ||  ${TABLE}.flag_type || '-' || ${TABLE}.flag;;
  }

  dimension: flag {
    type: string
    sql: ${TABLE}.flag ;;
  }

  dimension: flag_type {
    type: string
    sql: ${TABLE}.flag_type ;;
  }

  #Brian Yardley - 12/14/17
  dimension: language_flag {
    type: yesno
    sql: ${flag} = 'LANGUAGE' ;;
    description: "The field will tell you if a post has a language flag.  Yes means yes, no means no"
  }

  dimension: post_id {
    type: string
    sql: ${TABLE}.post_id ;;
  }

  dimension: keyword_category {
    label: "Keyword Category"
    type: string
    sql: CASE WHEN lower(${flag}) IN('associate','boss','collaboration','colleague','coworker','job','leadership','subordinate','team','teamwork','work','administrator',' assistant','client','collaborate','conference','contract','customer','director','employee','executive','foreman','manager','office','partner','promote','promotion','supervisor','taskmaster','vendor') THEN 'Workplace'
              WHEN lower(${flag}) IN('the line','lottery','bet') THEN 'Gambling'
              WHEN lower(${flag}) IN('defaulting','account','annuities','annuity','apr','artifices','assets','bank','bankroll','banks','barred','bet','bilk','blackmail','bogus','bookie','bookmaker','breach','bribe','cash','cents','cheat','checking','coerce','coin','coins','conned','conspiracy','corrupt','counterfeit','credit','debt','deceive','defraud','deposit','devised','disciplinary','disclosure','dishonest','divert','downpayment','dupe','embezzle','evasion','extort','falsifying','fiduciary','fines','fired','fleeced','forgery','fraud','funneling','gangsters','hoodwinked','hursti','illicit','incompetent','indentify','indenture','inept','infringement','interest','irregularities','jobbed','kickback','kidnap','larceny','launder','liable','misappropriate','misconduct','misfeasance','misguide','misinform','mislead','mismanage','misused','modernslavery','moneylaunder','money','moneys','monies','negligence','offense','overunder','over/under','owemoney','payoffs','peddle','phony','ponzi','punitive','ransom','revoke','salary','sanctionbreach','sanctions','savings','scam','scam','scammed','scandal','scheme','SEC','shortsell','siphoned','slave','slush','smuggle','snookered','socked','sportsbook','stashed','stocktrade','stocks','suckered','swindled','taxshelter','theline','thespread','theft','theft','tricked','trustfund','unappropriated','vacate','vandalise','waylaid','wrongdoing','accuse','allegation','alleged','arraign','bank','bankrupt','borrow','breach','bribe','casino','conspir','corrupt','creditor','debar','debt','decept','defraud','discharge','disciplin','discriminat','distort','dollar','earn','embezz','endowment','extort','financ','foreclose','foreclose','fraud','fund','gamble','harass','insolvent','invest','investigat','lawsuit','lease','lend','liquidate','litigate','loan','loan','misappropriat','mortgage','offender','overage','overdraft','pay','penalty','receivership','repossess','repossess','requisition','restructure','scandal','scheme','steal','suspen','swindle','tax shelter','terminate','unprofitable','verdict','violat') THEN 'Legal and Financial'
              ELSE 'Other' END;;
  }

  measure: bad_flag_reports{
    label: "Reports - Bad Flag"
    type: count_distinct
    sql: reports.report_id ;;
    filters: {
      field: flag
      value: "LANGUAGE, HARASSMENT, THREAT, ALCOHOL, SEX, VIOLENCE, CRIMINAL, BIGOTRY, DRUGS, SEXISM, CANNABIS, OTHER_DRUGS, INTOLERANCE"
    }
  }

  measure: good_flag_reports{
    label: "Reports - GOOD Flag"
    type: count_distinct
    sql: reports.report_id ;;
    filters: {
      field: flag
      value: "DONATION, VOLUNTEERING, HELPING_OTHERS, PROTECTING_OTHERS, COMMUNITY"
    }
  }

  measure: bad_flag_count {
    type: count
    filters: {
      field: flag
      value: "LANGUAGE, HARASSMENT, THREAT, ALCOHOL, SEX, VIOLENCE, CRIMINAL, BIGOTRY, DRUGS, SEXISM, CANNABIS, OTHER_DRUGS, INTOLERANCE"
    }
#     filters: {
#       field: flag_type
#       value: "SENSITIVITY"
#     }
  }

  #Jordan Lincenberg created this measure on 12/14/17
  measure: good_flag_count {
    type: count
    filters: {
      field: flag
      value: "DONATION, VOLUNTEERING, HELPING_OTHERS, PROTECTING_OTHERS, COMMUNITY"
    }
    filters: {
      field: flag_type
      value: "SENSITIVITY"
    }
  }

  #Jordan Lincenberg created this measure on 12/14/17
  measure: keyword_flag_count {
    type: count
    filters: {
      field: flag_type
      value: "KEYWORD, STEMMABLE_KEYWORD"
    }
  }

  #Brian Yardley created this measure on 01/17/17
  measure: bambino_flag_count {
    hidden: yes
    label: "Bambino Count"
    type: count
    description: "Specific flag count for bambino that excludes keywords and alcohol"
    filters: {
      field: flag_type
      value: "SENSITIVITY"
    }
    filters: {
      field: flag
      value: "-ALCOHOL"
    }
  }


  #Jordan Lincenberg created this measure on 12/14/17
  measure: alcohol_flag_count {
    type: count
    filters: {
      field: flag
      value: "ALCOHOL"
    }
    filters: {
      field: flag_type
      value: "SENSITIVITY"
    }
  }

#Brian Yardley created this measure on 04/25/18
  measure: alcohol_original_flag_count {
    type: count
    filters: {
      field: flag
      value: "ALCOHOL"
    }
    filters: {
      field: flag_type
      value: "SENSITIVITY"
    }
    filters: {
      field: posts.post_type
      value: "ORIGINAL"
    }
  }

  measure: alcohol_shared_flag_count {
    type: count
    filters: {
      field: flag
      value: "ALCOHOL"
    }
    filters: {
      field: flag_type
      value: "SENSITIVITY"
    }
    filters: {
      field: posts.post_type
      value: "SHARED"
    }
  }

  measure: alcohol_liked_flag_count {
    type: count
    filters: {
      field: flag
      value: "ALCOHOL"
    }
    filters: {
      field: flag_type
      value: "SENSITIVITY"
    }
    filters: {
      field: posts.post_type
      value: "LIKED"
    }
  }

  #Jordan Lincenberg created this measure on 12/14/17
  measure: language_flag_count {
    type: count
    filters: {
      field: flag
      value: "LANGUAGE"
    }
    filters: {
      field: flag_type
      value: "SENSITIVITY"
    }
  }

  measure: language_original_flag_count {
    type: count
    filters: {
      field: flag
      value: "LANGUAGE"
    }
    filters: {
      field: flag_type
      value: "SENSITIVITY"
    }
    filters: {
      field: posts.post_type
      value: "ORIGINAL"
    }
  }

  measure: language_shared_flag_count {
    type: count
    filters: {
      field: flag
      value: "LANGUAGE"
    }
    filters: {
      field: flag_type
      value: "SENSITIVITY"
    }
    filters: {
      field: posts.post_type
      value: "SHARED"
    }
  }

  measure: language_liked_flag_count {
    type: count
    filters: {
      field: flag
      value: "LANGUAGE"
    }
    filters: {
      field: flag_type
      value: "SENSITIVITY"
    }
    filters: {
      field: posts.post_type
      value: "LIKED"
    }
  }

  #Jordan Lincenberg created this measure on 12/14/17
  measure: sex_flag_count {
    type: count
    filters: {
      field: flag
      value: "SEX"
    }
    filters: {
      field: flag_type
      value: "SENSITIVITY"
    }
  }

  measure: sex_original_flag_count {
    type: count
    filters: {
      field: flag
      value: "SEX"
    }
    filters: {
      field: flag_type
      value: "SENSITIVITY"
    }
    filters: {
      field: posts.post_type
      value: "ORIGINAL"
    }
  }

  measure: sex_shared_flag_count {
    type: count
    filters: {
      field: flag
      value: "SEX"
    }
    filters: {
      field: flag_type
      value: "SENSITIVITY"
    }
    filters: {
      field: posts.post_type
      value: "SHARED"
    }
  }

  measure: sex_liked_flag_count {
    type: count
    filters: {
      field: flag
      value: "SEX"
    }
    filters: {
      field: flag_type
      value: "SENSITIVITY"
    }
    filters: {
      field: posts.post_type
      value: "LIKED"
    }
  }

  #Jordan Lincenberg created this measure on 12/14/17
  measure: bigotry_flag_count {
    type: count
    filters: {
      field: flag
      value: "BIGOTRY"
    }
    filters: {
      field: flag_type
      value: "SENSITIVITY"
    }
  }

  measure: bigotry_original_flag_count {
    type: count
    filters: {
      field: flag
      value: "BIGOTRY"
    }
    filters: {
      field: flag_type
      value: "SENSITIVITY"
    }
    filters: {
      field: posts.post_type
      value: "ORIGINAL"
    }
  }

  measure: bigotry_shared_flag_count {
    type: count
    filters: {
      field: flag
      value: "BIGOTRY"
    }
    filters: {
      field: flag_type
      value: "SENSITIVITY"
    }
    filters: {
      field: posts.post_type
      value: "SHARED"
    }
  }

  measure: bigotry_liked_flag_count {
    type: count
    filters: {
      field: flag
      value: "BIGOTRY"
    }
    filters: {
      field: flag_type
      value: "SENSITIVITY"
    }
    filters: {
      field: posts.post_type
      value: "LIKED"
    }
  }

  #Jordan Lincenberg created this measure on 12/14/17
  measure: drugs_flag_count {
    type: count
    filters: {
      field: flag
      value: "DRUGS"
    }
    filters: {
      field: flag_type
      value: "SENSITIVITY"
    }

  }

  measure: drugs_original_flag_count {
    type: count
    filters: {
      field: flag
      value: "DRUGS"
    }
    filters: {
      field: flag_type
      value: "SENSITIVITY"
    }
    filters: {
      field: posts.post_type
      value: "ORIGINAL"
    }
  }

  measure: drugs_shared_flag_count {
    type: count
    filters: {
      field: flag
      value: "DRUGS"
    }
    filters: {
      field: flag_type
      value: "SENSITIVITY"
    }
    filters: {
      field: posts.post_type
      value: "SHARED"
    }
  }

  measure: drugs_liked_flag_count {
    type: count
    filters: {
      field: flag
      value: "DRUGS"
    }
    filters: {
      field: flag_type
      value: "SENSITIVITY"
    }
    filters: {
      field: posts.post_type
      value: "LIKED"
    }
  }

  #Jordan Lincenberg created this measure on 12/14/17
  measure: violence_flag_count {
    type: count
    filters: {
      field: flag
      value: "VIOLENCE"
    }
    filters: {
      field: flag_type
      value: "SENSITIVITY"
    }
  }

  measure: violence_original_flag_count {
    type: count
    filters: {
      field: flag
      value: "VIOLENCE"
    }
    filters: {
      field: flag_type
      value: "SENSITIVITY"
    }
    filters: {
      field: posts.post_type
      value: "ORIGINAL"
    }
  }

  measure: violence_shared_flag_count {
    type: count
    filters: {
      field: flag
      value: "VIOLENCE"
    }
    filters: {
      field: flag_type
      value: "SENSITIVITY"
    }
    filters: {
      field: posts.post_type
      value: "SHARED"
    }
  }

  measure: violence_liked_flag_count {
    type: count
    filters: {
      field: flag
      value: "VIOLENCE"
    }
    filters: {
      field: flag_type
      value: "SENSITIVITY"
    }
    filters: {
      field: posts.post_type
      value: "LIKED"
    }
  }

  #JESSICA WU 10/31/19

  measure: cannabis_flag_count {
    type: count
    filters: {
      field: flag
      value: "CANNABIS"
    }
    filters: {
      field: flag_type
      value: "SENSITIVITY"
    }

  }

  measure: cannabis_original_flag_count {
    type: count
    filters: {
      field: flag
      value: "CANNABIS"
    }
    filters: {
      field: flag_type
      value: "SENSITIVITY"
    }
    filters: {
      field: posts.post_type
      value: "ORIGINAL"
    }
  }

  measure: cannabis_shared_flag_count {
    type: count
    filters: {
      field: flag
      value: "CANNABIS"
    }
    filters: {
      field: flag_type
      value: "SENSITIVITY"
    }
    filters: {
      field: posts.post_type
      value: "SHARED"
    }
  }

  measure: cannabis_liked_flag_count {
    type: count
    filters: {
      field: flag
      value: "CANNABIS"
    }
    filters: {
      field: flag_type
      value: "SENSITIVITY"
    }
    filters: {
      field: posts.post_type
      value: "LIKED"
    }
  }

  measure: other_drugs_flag_count {
    type: count
    filters: {
      field: flag
      value: "OTHER_DRUGS"
    }
    filters: {
      field: flag_type
      value: "SENSITIVITY"
    }

  }

  measure: other_drugs_original_flag_count {
    type: count
    filters: {
      field: flag
      value: "OTHER_DRUGS"
    }
    filters: {
      field: flag_type
      value: "SENSITIVITY"
    }
    filters: {
      field: posts.post_type
      value: "ORIGINAL"
    }
  }

  measure: other_drugs_shared_flag_count {
    type: count
    filters: {
      field: flag
      value: "OTHER_DRUGS"
    }
    filters: {
      field: flag_type
      value: "SENSITIVITY"
    }
    filters: {
      field: posts.post_type
      value: "SHARED"
    }
  }

  measure: other_drugs_liked_flag_count {
    type: count
    filters: {
      field: flag
      value: "OTHER_DRUGS"
    }
    filters: {
      field: flag_type
      value: "SENSITIVITY"
    }
    filters: {
      field: posts.post_type
      value: "LIKED"
    }
  }


  #jessica wu 7/1/19

  measure: threat_flag_count {
    type: count
    filters: {
      field: flag
      value: "THREAT"
    }
    filters: {
      field: flag_type
      value: "SENSITIVITY"
    }

  }

  measure: threat_original_flag_count {
    type: count
    filters: {
      field: flag
      value: "THREAT"
    }
    filters: {
      field: flag_type
      value: "SENSITIVITY"
    }
    filters: {
      field: posts.post_type
      value: "ORIGINAL"
    }
  }

  measure: threat_shared_flag_count {
    type: count
    filters: {
      field: flag
      value: "THREAT"
    }
    filters: {
      field: flag_type
      value: "SENSITIVITY"
    }
    filters: {
      field: posts.post_type
      value: "SHARED"
    }
  }

  measure: threat_liked_flag_count {
    type: count
    filters: {
      field: flag
      value: "THREAT"
    }
    filters: {
      field: flag_type
      value: "SENSITIVITY"
    }
    filters: {
      field: posts.post_type
      value: "LIKED"
    }
  }

  measure: harassment_flag_count {
    type: count
    filters: {
      field: flag
      value: "HARASSMENT"
    }
    filters: {
      field: flag_type
      value: "SENSITIVITY"
    }

  }

  measure: harassment_original_flag_count {
    type: count
    filters: {
      field: flag
      value: "HARASSMENT"
    }
    filters: {
      field: flag_type
      value: "SENSITIVITY"
    }
    filters: {
      field: posts.post_type
      value: "ORIGINAL"
    }
  }

  measure: harassment_shared_flag_count {
    type: count
    filters: {
      field: flag
      value: "HARASSMENT"
    }
    filters: {
      field: flag_type
      value: "SENSITIVITY"
    }
    filters: {
      field: posts.post_type
      value: "SHARED"
    }
  }

  measure: harassment_liked_flag_count {
    type: count
    filters: {
      field: flag
      value: "HARASSMENT"
    }
    filters: {
      field: flag_type
      value: "SENSITIVITY"
    }
    filters: {
      field: posts.post_type
      value: "LIKED"
    }
  }

  #JW 6/12/20
  measure: intolerance_flag_count {
    type: count
    filters: {
      field: flag
      value: "INTOLERANCE"
    }
    filters: {
      field: flag_type
      value: "SENSITIVITY"
    }

  }
  measure: intolerance_original_flag_count {
    type: count
    filters: {
      field: flag
      value: "INTOLERANCE"
    }
    filters: {
      field: flag_type
      value: "SENSITIVITY"
    }
    filters: {
      field: posts.post_type
      value: "ORIGINAL"
    }
  }

  measure: intolerance_shared_flag_count {
    type: count
    filters: {
      field: flag
      value: "INTOLERANCE"
    }
    filters: {
      field: flag_type
      value: "SENSITIVITY"
    }
    filters: {
      field: posts.post_type
      value: "SHARED"
    }
  }

  measure: intolerance_liked_flag_count {
    type: count
    filters: {
      field: flag
      value: "INTOLERANCE"
    }
    filters: {
      field: flag_type
      value: "SENSITIVITY"
    }
    filters: {
      field: posts.post_type
      value: "LIKED"
    }
  }

#Jordan Lincenberg created this measure on 12/14/17
  measure: crime_flag_count {
    type: count
    filters: {
      field: flag
      value: "CRIMINAL"
    }
    filters: {
      field: flag_type
      value: "SENSITIVITY"
    }
  }

  measure: crime_original_flag_count {
    type: count
    filters: {
      field: flag
      value: "CRIMINAL"
    }
    filters: {
      field: flag_type
      value: "SENSITIVITY"
    }
    filters: {
      field: posts.post_type
      value: "ORIGINAL"
    }
  }

  measure: crime_shared_flag_count {
    type: count
    filters: {
      field: flag
      value: "CRIMINAL"
    }
    filters: {
      field: flag_type
      value: "SENSITIVITY"
    }
    filters: {
      field: posts.post_type
      value: "SHARED"
    }
  }

  measure: crime_liked_flag_count {
    type: count
    filters: {
      field: flag
      value: "CRIMINAL"
    }
    filters: {
      field: flag_type
      value: "SENSITIVITY"
    }
    filters: {
      field: posts.post_type
      value: "LIKED"
    }
  }

  measure: sexism_flag_count {
    type: count
    filters: {
      field: flag
      value: "SEXISM"
    }
    filters: {
      field: flag_type
      value: "SENSITIVITY"
    }
  }

  measure: sexism_original_flag_count {
    type: count
    filters: {
      field: flag
      value: "SEXISM"
    }
    filters: {
      field: flag_type
      value: "SENSITIVITY"
    }
    filters: {
      field: posts.post_type
      value: "ORIGINAL"
    }
  }

  measure: sexism_shared_flag_count {
    type: count
    filters: {
      field: flag
      value: "SEXISM"
    }
    filters: {
      field: flag_type
      value: "SENSITIVITY"
    }
    filters: {
      field: posts.post_type
      value: "SHARED"
    }
  }

  measure: sexism_liked_flag_count {
    type: count
    filters: {
      field: flag
      value: "SEXISM"
    }
    filters: {
      field: flag_type
      value: "SENSITIVITY"
    }
    filters: {
      field: posts.post_type
      value: "LIKED"
    }
  }


  measure: donation_flag_count {
    type: count
    filters: {
      field: flag
      value: "DONATION"
    }
    filters: {
      field: flag_type
      value: "SENSITIVITY"
    }
  }

  measure: donation_original_flag_count {
    type: count
    filters: {
      field: flag
      value: "DONATION"
    }
    filters: {
      field: flag_type
      value: "SENSITIVITY"
    }
    filters: {
      field: posts.post_type
      value: "ORIGINAL"
    }
  }

  measure: donation_shared_flag_count {
    type: count
    filters: {
      field: flag
      value: "DONATION"
    }
    filters: {
      field: flag_type
      value: "SENSITIVITY"
    }
    filters: {
      field: posts.post_type
      value: "SHARED"
    }
  }

  measure: donation_liked_flag_count {
    type: count
    filters: {
      field: flag
      value: "DONATION"
    }
    filters: {
      field: flag_type
      value: "SENSITIVITY"
    }
    filters: {
      field: posts.post_type
      value: "LIKED"
    }
  }

  measure: volunteering_flag_count {
    type: count
    filters: {
      field: flag
      value: "VOLUNTEERING"
    }
    filters: {
      field: flag_type
      value: "SENSITIVITY"
    }
  }

  measure: volunteering_original_flag_count {
    type: count
    filters: {
      field: flag
      value: "VOLUNTEERING"
    }
    filters: {
      field: flag_type
      value: "SENSITIVITY"
    }
    filters: {
      field: posts.post_type
      value: "ORIGINAL"
    }
  }

  measure: volunteering_shared_flag_count {
    type: count
    filters: {
      field: flag
      value: "VOLUNTEERING"
    }
    filters: {
      field: flag_type
      value: "SENSITIVITY"
    }
    filters: {
      field: posts.post_type
      value: "SHARED"
    }
  }

  measure: volunteering_liked_flag_count {
    type: count
    filters: {
      field: flag
      value: "VOLUNTEERING"
    }
    filters: {
      field: flag_type
      value: "SENSITIVITY"
    }
    filters: {
      field: posts.post_type
      value: "LIKED"
    }
  }

  measure: helping_others_flag_count {
    type: count
    filters: {
      field: flag
      value: "HELPING_OTHERS"
    }
    filters: {
      field: flag_type
      value: "SENSITIVITY"
    }
  }

  measure: helping_others_original_flag_count {
    type: count
    filters: {
      field: flag
      value: "HELPING_OTHERS"
    }
    filters: {
      field: flag_type
      value: "SENSITIVITY"
    }
    filters: {
      field: posts.post_type
      value: "ORIGINAL"
    }
  }

  measure: helping_others_shared_flag_count {
    type: count
    filters: {
      field: flag
      value: "HELPING_OTHERS"
    }
    filters: {
      field: flag_type
      value: "SENSITIVITY"
    }
    filters: {
      field: posts.post_type
      value: "SHARED"
    }
  }

  measure: helping_others_liked_flag_count {
    type: count
    filters: {
      field: flag
      value: "HELPING_OTHERS"
    }
    filters: {
      field: flag_type
      value: "SENSITIVITY"
    }
    filters: {
      field: posts.post_type
      value: "LIKED"
    }
  }

  measure: protecting_others_flag_count {
    type: count
    filters: {
      field: flag
      value: "PROTECTING_OTHERS"
    }
    filters: {
      field: flag_type
      value: "SENSITIVITY"
    }
  }

  measure: protecting_others_original_flag_count {
    type: count
    filters: {
      field: flag
      value: "PROTECTING_OTHERS"
    }
    filters: {
      field: flag_type
      value: "SENSITIVITY"
    }
    filters: {
      field: posts.post_type
      value: "ORIGINAL"
    }
  }

  measure: protecting_others_shared_flag_count {
    type: count
    filters: {
      field: flag
      value: "PROTECTING_OTHERS"
    }
    filters: {
      field: flag_type
      value: "SENSITIVITY"
    }
    filters: {
      field: posts.post_type
      value: "SHARED"
    }
  }

  measure: protecting_others_liked_flag_count {
    type: count
    filters: {
      field: flag
      value: "PROTECTING_OTHERS"
    }
    filters: {
      field: flag_type
      value: "SENSITIVITY"
    }
    filters: {
      field: posts.post_type
      value: "LIKED"
    }
  }

  #JW 6/12/20

  measure: community_flag_count {
    type: count
    filters: {
      field: flag
      value: "COMMUNITY"
    }
    filters: {
      field: flag_type
      value: "SENSITIVITY"
    }

  }
  measure: community_original_flag_count {
    type: count
    filters: {
      field: flag
      value: "COMMUNITY"
    }
    filters: {
      field: flag_type
      value: "SENSITIVITY"
    }
    filters: {
      field: posts.post_type
      value: "ORIGINAL"
    }
  }

  measure: community_shared_flag_count {
    type: count
    filters: {
      field: flag
      value: "COMMUNITY"
    }
    filters: {
      field: flag_type
      value: "SENSITIVITY"
    }
    filters: {
      field: posts.post_type
      value: "SHARED"
    }
  }

  measure: community_liked_flag_count {
    type: count
    filters: {
      field: flag
      value: "COMMUNITY"
    }
    filters: {
      field: flag_type
      value: "SENSITIVITY"
    }
    filters: {
      field: posts.post_type
      value: "LIKED"
    }
  }

  measure: twitter_bad_flag_count {
    label: "Social - Twitter Bad Flag Count"
    type: count
    filters: {
      field: flag
      value: "LANGUAGE, INTOLERANCE, ALCOHOL, HARASSMENT, THREAT, SEX, VIOLENCE, CRIMINAL, BIGOTRY, DRUGS, SEXISM"
    }
    filters: {
      field: profiles.type
      value: "TWITTER"
    }
    filters: {
      field: flag_type
      value: "SENSITIVITY"
    }
  }

  measure: twitter_bad_flag_post_count {
    label: "Social - Twitter Bad Flag Post Count"
    type: count_distinct
    sql: ${post_id} ;;
    filters: {
      field: flag
      value: "LANGUAGE, ALCOHOL, INTOLERANCE, HARASSMENT, THREAT, SEX, VIOLENCE, CRIMINAL, BIGOTRY, DRUGS, SEXISM"
    }
    filters: {
      field: profiles.type
      value: "TWITTER"
    }
    filters: {
      field: flag_type
      value: "SENSITIVITY"
    }
  }

  measure: facebook_bad_flag_count {
    label: "Social - Facebook Bad Flag Count"
    type: count
    filters: {
      field: flag
      value: "LANGUAGE, ALCOHOL, HARASSMENT, INTOLERANCE, THREAT, SEX, VIOLENCE, CRIMINAL, BIGOTRY, DRUGS, SEXISM"
    }
    filters: {
      field: profiles.type
      value: "FACEBOOK"
    }
    filters: {
      field: flag_type
      value: "SENSITIVITY"
    }

  }



  measure: instagram_bad_flag_count {
    label: "Social - Instagram Bad Flag Count"
    type: count_distinct
    sql: ${post_id} ;;
    filters: {
      field: flag
      value: "LANGUAGE, ALCOHOL, HARASSMENT, THREAT, OTHER_DRUGS, CANNABIS, SEX, VIOLENCE, CRIMINAL, INTOLERANCE, BIGOTRY, DRUGS, SEXISM"
    }
    filters: {
      field: profiles.type
      value: "INSTAGRAM"
    }
    filters: {
      field: flag_type
      value: "SENSITIVITY"
    }

  }

  measure: count {
    label: "Post Flag Count"
    type: count
    drill_fields: [detail*]
  }

  measure: count_distinct {
    label: "Distinct Post Count"
    description: "Distinct post id count from query."
    type: count_distinct
    sql: ${post_id} ;;
    drill_fields: [detail*]
  }

  measure: bad_flag_post_count {
    label: "Bad Flag Post Count"
    description: "Number of posts with a bad flag."
    type: count_distinct
    sql: ${post_id} ;;
    filters: {
      field: flag
      value: "LANGUAGE, OTHER_DRUGS, INTOLERANCE CANNABIS, HARASSMENT, THREAT, ALCOHOL, SEX, VIOLENCE, CRIMINAL, BIGOTRY, DRUGS, SEXISM"
    }
    drill_fields: [detail*]
  }

  measure: good_flag_post_count {
    label: "Good Flag Post Count"
    description: "Number of posts with a good flag."
    type: count_distinct
    sql: ${post_id} ;;
    filters: {
      field: flag
      value: "VOLUNTEERING, COMMUNITY, HELPING_OTHERS, PROTECTING_OTHERS, DONATION"
    }
    drill_fields: [detail*]
  }

  measure: flagged_post_count {
    label: "Flagged Post Count"
    description: "Number of posts with a Good or Bad flag."
    type: count_distinct
    sql: ${post_id} ;;
    filters: {
      field: flag_type
      value: "SENSITIVITY"
    }
    drill_fields: [detail*]
  }

  set: detail {
    fields: [
      post_id,
      flag
    ]
  }
}
