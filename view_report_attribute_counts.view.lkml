

view: view_report_attribute_counts {
  derived_table: {
    explore_source: companies {
      column: report_id { field: reports.report_id }
      column: date_created {field: reports.date_created_raw}
      column: date_finished {field: reports.date_finished_raw}
      column: location_count { field: report_addresses.count }
      column: alias_count { field: report_aliases.count }
      column: education_count { field: report_degrees.count }
      column: email_count { field: report_emails.count }
      column: job_count { field: report_jobs.count }
      column: phone_count { field: report_phones.count }
      column: profile_count { field: profiles.count }
      column: proper_case_name {}
    }
  }
  dimension: report_id {
    primary_key: yes
  }
  dimension: proper_case_name {}
  dimension_group: date_created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      day_of_week,
      hour_of_day,
      week,
      month,
      month_name,
      quarter,
      year,
      time_of_day
    ]
    sql: ${TABLE}.date_created ;;
  }

  dimension_group: date_finished {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      month_name,
      quarter,
      year,
      day_of_week,
      time_of_day,
      hour_of_day,
    ]
    sql: ${TABLE}.date_finished ;;
  }
  dimension: education_count {
    label: "Number of Schools"
    type: number
  }
  dimension: location_count {
    label: "Number of Locations"
    type: number
  }dimension: alias_count {
    label: "Number of Aliases"
    type: number
  }
  dimension: email_count {
    label: "Number of Emails"
    type: number
  }
  dimension: job_count {
    label: "Number of Jobs"
    type: number
  }
  dimension: phone_count {
    label: "Number of Phones"
    type: number
  }
  dimension: profile_count {
    label: "Number of Profiles"
    type: number
  }
  dimension: total_attributes {
    type: number
    #add one at the end for name
    sql: ${education_count}+${email_count}+${job_count}+${phone_count}+${alias_count}+${location_count}+1 ;;
  }

  measure: count {
    type: count
  }

  measure: total_profiles {
    type: sum
    sql: ${profile_count} ;;
  }

  measure: total_profiles_no_att {
    type: sum
    sql: ${profile_count} ;;
    filters: {
      field: education_count
      value: "0"
    }
    filters: {
      field: job_count
      value: "0"
    }
  }

  measure: total_reports_no_att {
    type: count
    filters: {
      field: education_count
      value: "0"
    }
    filters: {
      field: job_count
      value: "0"
    }
  }

  measure: total_profiles_with_job_no_edu {
    type: sum
    sql: ${profile_count} ;;
    filters: {
      field: education_count
      value: "0"
    }
    filters: {
      field: job_count
      value: ">0"
    }
  }

  measure: total_reports_with_job_no_edu {
    type: count
    filters: {
      field: education_count
      value: "0"
    }
    filters: {
      field: job_count
      value: ">0"
    }
  }

  measure: total_profiles_with_edu_no_job{
    type: sum
    sql: ${profile_count} ;;
    filters: {
      field: education_count
      value: ">0"
    }
    filters: {
      field: job_count
      value: "0"
    }
  }

  measure: total_reports_with_edu_no_job {
    type: count
    filters: {
      field: education_count
      value: ">0"
    }
    filters: {
      field: job_count
      value: "0"
    }
  }

  measure: total_profiles_with_edu_and_job{
    type: sum
    sql: ${profile_count} ;;
    filters: {
      field: education_count
      value: ">0"
    }
    filters: {
      field: job_count
      value: ">0"
    }
  }

  measure: total_reports_with_edu_and_job {
    type: count
    filters: {
      field: education_count
      value: ">0"
    }
    filters: {
      field: job_count
      value: ">0"
    }
  }
}
