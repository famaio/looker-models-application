view: post_media_entries {
  sql_table_name: public.post_media_entries ;;

  dimension: media_type {
    type: string
    sql: ${TABLE}.media_type ;;
  }

  dimension: post_id {
    hidden: yes
    type: string
    sql: ${TABLE}.post_id ;;
  }

  dimension: thumbnail_url {
    type: string
    sql: ${TABLE}.thumbnail_url ;;
  }

  dimension: url {
    label: "Media Entry URL"
    type: string
    sql: ${TABLE}.url ;;
  }

  measure: count {
    label: "Media Entry Post Count"
    type: count
    drill_fields: [posts.post_id]
  }

  measure: post_image_count {
    label: "Posts with Image Count"
    type: count_distinct
    sql: posts.post_id ;;
    filters: {
      field: media_type
      value: "IMAGE"
    }
  }
}
