
view: test_view_post_actions_meta_data {
  view_label: "TEST Rater Actions - Posts"
  derived_table: {
    persist_for:"2 minutes"
    distribution_style: all
    explore_source: companies {
      column: post_id { field: view_post_actions.post_id }
      column: rater_id { field: view_post_actions.rater_id }
      column: action_type { field: view_post_actions.action_type }
      column: duration { field: view_post_actions.duration }
      column: start_time { field: view_post_actions.start_raw }
      column: end_time { field: view_post_actions.end_raw }
      column: username { field: view_post_actions.username }
      column: masked_username { field: view_post_actions.masked_username }
      column: profile_type { field: profiles.type }
      filters: {
        field: view_post_actions.post_id
        value: "-NULL"
      }
    }
  }
  dimension: compound_primary_key {
    primary_key: yes
    hidden: yes
    sql: ${TABLE}.post_id || ${TABLE}.duration ;;
  }
  dimension: post_id {
    label: "Post ID"
    #primary_key: yes
  }
  dimension: rater_id {
    label: "Rater ID"
  }
  dimension: action_type {
    label: "Action Type"
  }
  dimension: duration {
    label: "Duration"
    type: number
  }
  dimension: team {
    type: string
    sql:

    CASE WHEN ${TABLE}.username in ('yohandra','samir-s','michelle','julied','niaroane',
    'mark-coco','angel','georgios','favy','alex','jessica','dorga','david','james','tracy')
    THEN 'in-house'
        WHEN ${TABLE}.username ilike '%os'
    THEN 'AOS'
    WHEN ${TABLE}.username ilike '%cf'
    THEN 'CF'
    END;;
  }

  measure: inhouse_count {
    label: "Total in-house PR count"
    type: count
    filters: {field: username
      value: "in-house"}
  }

  dimension_group: start {
    type: time
    timeframes: [
      raw,
      time,
      date,
      day_of_week,
      week,
      month,
      quarter,
      year,
      hour_of_day,
      hour
    ]
    sql: ${TABLE}.start_time ;;
  }

  dimension_group: end {
    type: time
    timeframes: [
      raw,
      time,
      date,
      day_of_week,
      week,
      month,
      quarter,
      year,
      hour_of_day,
      hour
    ]
    sql: ${TABLE}.end_time ;;
  }
  dimension: username {
    label: "Username"
  }
  dimension: profile_type {
    label: "Profile Type"
  }

  measure: count {
    type: count
  }

  measure: average_duration {
    label: "Average Duration - Seconds"
    type: average
    sql: ${duration} ;;
    value_format_name: "decimal_2"
    filters: {
      field: end_date
      value: "-null"
    }
  }

  measure: total_duration {
    label: "Total Duration - Seconds"
    type: sum
    sql: ${duration} ;;
    value_format_name: "decimal_2"
    filters: {
      field: end_date
      value: "-null"
    }
  }

  measure: rated_post_count {
    label: "Tasks - Total Post Rates"
    type: count
#     sql: ${post_id} ;;
    filters: {
      field: end_time
      value: "-null"
    }
  }

  measure: rated_post_count_without_rerates {
    label: "Tasks - Total Post Rates Excluding Re-Rates"
    type: count_distinct
    sql: ${post_id} ;;
    filters: {
      field: end_time
      value: "-null"
    }
  }

  measure: rated_facebook_post_count {
    label: "Facebook - Rated Posts"
    type: count_distinct
    sql: ${post_id} ;;
    filters: {
      field: profile_type
      value: "FACEBOOK"
    }
    filters: {
      field: end_time
      value: "-null"
    }
  }

  measure: rated_twitter_post_count {
    label: "Twitter - Rated Posts"
    type: count_distinct
    sql: ${post_id} ;;
    filters: {
      field: profile_type
      value: "TWITTER"
    }
    filters: {
      field: end_time
      value: "-null"
    }
  }

  measure: rated_instagram_post_count {
    label: "Instagram - Rated Posts"
    type: count_distinct
    sql: ${post_id} ;;
    filters: {
      field: profile_type
      value: "INSTAGRAM"
    }
    filters: {
      field: end_time
      value: "-null"
    }
  }

  measure: rated_google_post_count {
    label: "Google + - Rated Posts"
    type: count_distinct
    sql: ${post_id} ;;
    filters: {
      field: profile_type
      value: "GOOGLEPLUS"
    }
    filters: {
      field: end_time
      value: "-null"
    }
  }

  measure: total_raters {
    label: "Total Number of Raters"
    type: count_distinct
    sql: ${username} ;;
  }
}
