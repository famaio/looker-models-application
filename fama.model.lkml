connection: "redshift"

include: "*.view.lkml"                       # include all views in this project
# include: "my_dashboard.dashboard.lookml"   # include a LookML dashboard called my_dashboard

# # Select the views that should be a part of this model,
# # and define the joins that connect them together.

#DATAGROUPS
datagroup: etl_trigger {
  # trigger the rebuild of this table when the post_ratings table has updated. it is the last major table in the ETL update process.
  # sql_trigger: SELECT DATE_PART('hour', GETDATE()) ;;
  sql_trigger: SELECT COUNT(*) FROM public.rater_actions ;;
}

datagroup: etl_finished_trigger {
  sql_trigger: SELECT COUNT(*) FROM public.rater_actions ;;
}

#FAMA DATA
explore: companies {
  #Fama Base Model
  label: "Fama Data"
  view_label: "01 | Companies"
  description: "Main explore that uses companies as the main join table. This insures that companies without reports will still be accounted for."

  join: users {
    view_label: "02 | Report Users"
    type: left_outer
    sql_on: ${users.company_id} = ${companies.company_id} ;;
    relationship: many_to_one
  }

  join: reports {
    view_label: "03 | Reports"
    type:  left_outer
    sql_on: ${reports.user_id} = ${users.user_id}
      and ${reports.deleted} = false
      and ${reports.date_finished_raw} is not null
      and DATEDIFF(day, reports.date_finished, GETDATE()) >= 1;;
    relationship: many_to_one
  }

  # second level base tables. profiles, articles and flag kits and their meta tables that are assoicated directly with the reports table
  join: profiles {
    view_label: "05 | Profiles"
    type: left_outer
    sql_on: ${profiles.report_id} = ${reports.report_id}
      and ${profiles.deleted} = false
      and ${reports.date_finished_raw} is not null
    and DATEDIFF(day, reports.date_finished, GETDATE()) > 1;;
    relationship: many_to_one
  }

  join: articles {
    view_label: "06 | Articles"
    type: left_outer
    sql_on: ${articles.report_id} = ${reports.report_id}
    and ${reports.date_finished_raw} is not null
    and DATEDIFF(hour, reports.date_finished, GETDATE()) > 1;;
    relationship: many_to_one
  }

  join: article_flags {
    view_label: "07 | Article Flags"
    type: left_outer
    sql_on: ${article_flags.article_id} = ${articles.article_id}
    and ${articles.relevant} ;;
    relationship: many_to_one
  }

  join: flag_kits {
    view_label: "04 | Flag Kits"
    type: left_outer
    sql_on: ${flag_kits.version_id} = ${reports.flag_kit_version_id} ;;
    relationship: many_to_one
  }

  #third level base tables their meta tables that are associated directly with profiles,

  join: posts {
    view_label: "08 | Posts"
    type: left_outer
    sql_on: ${posts.profile_id} = ${profiles.profile_id}
      and  ${posts.deleted} = false and ${posts.hidden} = false;;
    relationship: many_to_one
  }

  join: post_media_entries {
    view_label: "08 | Posts"
    type: left_outer
    sql_on: ${post_media_entries.post_id} = ${posts.post_id} ;;
    relationship: many_to_one
  }

  join: post_flags {
    view_label: "09 | Post Flag"
    type: left_outer
    sql_on: ${post_flags.post_id} = ${posts.post_id} ;;
    relationship: many_to_one
  }

  join: post_ratings {
    view_label: "09 | Post Flag"
    type: left_outer
    sql_on: ${post_ratings.post_id} = ${posts.post_id} ;;
    relationship: many_to_one
  }

  join: rating_flags {
    view_label: "10 | Rating Flags"
    type: left_outer
    sql_on: ${post_ratings.rating_id} = ${rating_flags.rating_id}
    and ${post_flags.post_id}=${posts.post_id};;
    relationship: many_to_one
  }

  # Calculation tables

  join: view_reports_total_post_flags {
    view_label: "11 | Post Flags Count"
    type: left_outer
    sql_on: ${view_reports_total_post_flags.report_id} = ${reports.report_id}
    and ${reports.date_finished_raw} is not null
    and DATEDIFF(hour, reports.date_finished, GETDATE()) > 1;;
    relationship: one_to_one
  }

  join: view_reports_total_article_flags {
    view_label: "12 | Article Flags Count"
    type: left_outer
    sql_on: ${view_reports_total_article_flags.report_id} = ${reports.report_id}
    and ${reports.date_finished_raw} is not null
    and DATEDIFF(hour, reports.date_finished, GETDATE()) > 1;;
    relationship: one_to_one
  }
  join: view_reports_total_flags {
    view_label: "13 | Report Flags Count"
    type: inner
    sql_on: ${view_reports_total_flags.report_id} = ${reports.report_id}
    and ${reports.date_finished_raw} is not null
    and DATEDIFF(hour, reports.date_finished, GETDATE()) > 1;;
    relationship: one_to_one
  }
  join: view_reports_total_content {
    view_label: "14 | Report Content Count"
    type: left_outer
    sql_on: ${view_reports_total_content.report_id} = ${reports.report_id}
    and ${reports.date_finished_raw} is not null
    and DATEDIFF(hour, reports.date_finished, GETDATE()) > 1;;
    relationship: one_to_one
  }

  # SWITCHES
  join: view_latest_reports {
    view_label: "Switch | View Only Latest Reports"
    type: left_outer
    sql_on: ${view_latest_reports.report_id} = ${reports.report_id}
    and ${reports.date_finished_raw} is not null
    and DATEDIFF(hour, reports.date_finished, GETDATE()) > 1;;
    relationship: one_to_one
  }
}

#FAMA OPERATIONS
explore: fama_operations {
  label: "Operations"
  view_name: companies
  extends: [companies]

  join: view_confirm_actions {
    view_label: "15 | Rater Actions | Confirms"
    type: left_outer
    sql_on: ${view_confirm_actions.report_id} = ${reports.report_id}
    and ${reports.date_finished_raw} is not null
    and DATEDIFF(hour, reports.date_finished, GETDATE()) > 1;;
    relationship: many_to_one
  }


  join: view_qa_actions {
    view_label: "16 | Rater Actions | QAs"
    type: left_outer
    sql_on: ${view_qa_actions.report_id} = ${reports.report_id}
    and ${reports.date_finished_raw} is not null
    and DATEDIFF(hour, reports.date_finished, GETDATE()) > 1;;
    relationship: many_to_one
  }

  join: view_post_actions {
    view_label: "17 | Rater Actions | Posts"
    type: left_outer
    sql_on: ${view_post_actions.post_id} = ${posts.post_id} ;;
    relationship: many_to_one
  }

  join: view_article_actions {
    view_label: "18 | Rater Actions | Articles"
    type: left_outer
    sql_on: ${view_article_actions.article_id} = ${articles.article_id} ;;
    relationship: many_to_one
  }
}

#PDT Table References
explore: rater_actions {hidden:yes}
explore: predictions {hidden:yes}
explore: view_profile_flags {hidden:yes}
explore: view_post_actions {hidden:yes}
