view: view_post_predictions_sex {
  label: "Post Prediction - Sex"
  derived_table: {
    sql_trigger_value:  SELECT COUNT(*) FROM public.predictions ;;
    distribution_style: all
    explore_source: predictions {
      column: post_id {field:predictions.content_id}
      column: sex_value {field:predictions.value}
      filters: {
        field: predictions.prediction_type
        value: "SEX"
      }
      filters: {
        field: predictions.model_type
        value: "POST,POST^_IMAGE"
      }
    }
  }
  dimension: post_id {
    primary_key: yes
  }
  dimension: sex_value {
    type: number
  }
}
