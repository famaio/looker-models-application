view: rater_actions_unique {
  # Or, you could make this view a derived table, like this:
  derived_table: {
    #sql_trigger_value:  SELECT FLOOR((UNIX_TIMESTAMP(NOW()) - 60*60*3)/(60*60*24));;
    sql_trigger_value: SELECT COUNT(*) FROM public.rater_actions ;;
    distribution_style: all
    sql:
    SELECT r.action_item, r.start_time, r.end_time, r.action_type, r.rater_id, r.username
    FROM
      (
      SELECT action_item, action_type, min(end_time) as first
      FROM rater_actions
      GROUP BY action_item, action_type
      ) foo
    JOIN rater_actions r on foo.action_item = r.action_item AND foo.first = r.end_time AND r.action_type = foo.action_type
    WHERE r.end_time IS NOT NULL
      ;;
  }

  dimension: action_item {
    type: string
    sql: ${TABLE}.action_item ;;
  }

  dimension_group: end {
    type: time
    timeframes: [
      raw,
      time,
      date,
      day_of_week,
      week,
      month,
      quarter,
      year,
      hour_of_day,
      hour
    ]
    sql: ${TABLE}.end_time ;;
  }

  dimension: rater_id {
    type: string
    sql: ${TABLE}.rater_id ;;
  }

  dimension_group: start {
    type: time
    timeframes: [
      raw,
      time,
      date,
      day_of_week,
      week,
      month,
      quarter,
      year,
      hour_of_day
    ]
    sql: ${TABLE}.start_time ;;
  }

  dimension: duration {
    type: number
    sql: CASE WHEN ${end_date} IS NOT NULL THEN datediff('seconds', ${start_raw}, ${end_raw}) END;;
    value_format: "0.00"
  }

  dimension: duration_minutes {
    type: number
    #sql: CASE WHEN ${end_date} IS NOT NULL THEN datediff('minutes', ${start_raw}, ${end_raw}) ELSE 0 END;;
    sql: datediff('minutes', ${start_raw}, ${end_raw});;
    value_format_name: "decimal_2"
  }


  measure: duration_average {
    type: average
    sql: ${duration} ;;
  }

  measure: duration_median {
    type: median
    sql: ${duration} ;;
  }

  measure: duration_average_minutes {
    type: average
    sql: ${duration_minutes} ;;
  }

  measure: duration_median_minutes {
    type: median
    sql: ${duration_minutes} ;;
  }

  measure: duration_sum {
    type: sum
    sql: ${duration} ;;
  }

  measure: duration_sum_minutes {
    type: sum
    sql: ${duration_minutes} ;;
  }

  dimension: action_type {
    type: string
    sql: ${TABLE}.action_type ;;
  }

  dimension: username {
    type: string
    sql: ${TABLE}.username ;;
  }

  measure: count {
    type: count
    drill_fields: [username]
  }

  measure: total_person_confirm_count {
    label: "Total Person Confirms"
    type: count
    filters: {
      field: action_type
      value: "MANUAL_PERSON_CONFIRM"
    }
    drill_fields: [username]
  }

  measure: total_profile_scrape_count {
    label: "Total Profile Scrape"
    type: count
    filters: {
      field: action_type
      value: "MANUAL_PROFILE_SCRAPE"
    }
    drill_fields: [username]
  }

  measure: total_news_rate_count {
    label: "Total News Rates"
    type: count
    filters: {
      field: action_type
      value: "MANUAL_NEWS_RATE"
    }
    drill_fields: [username]
  }

  measure: total_post_rate_count {
    label: "Total Post Rates"
    type: count
    filters: {
      field: action_type
      value: "MANUAL_POST_RATE"
    }
    drill_fields: [username]
  }

  measure: total_QA_count {
    label: "Total QAs"
    type: count
    filters: {
      field: action_type
      value: "MANUAL_PERSON_QA"
    }
    drill_fields: [username]
  }
}
