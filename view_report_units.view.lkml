#
#
# view: view_report_units {
#   derived_table: {
#     sql_trigger_value:  SELECT COUNT(*) FROM ${view_post_actions.SQL_TABLE_NAME} ;;
#     distribution_style: all
#     explore_source: companies {
#       column: date_created { field: reports.date_created_time }
#       column: date_finished { field: reports.date_finished_time }
#       column: profile_confirm_count { field: view_confirm_actions.profile_confirm_count }
#       column: rated_post_count { field: view_post_actions.rated_post_count }
#       column: article_rate_count { field: view_article_actions.article_rate_count }
#       column: reports_count_distinct { field: reports.reports_count_distinct }
#       filters: {
#         field: reports.date_finished_date
#         value: "after 2018/02/20"
#       }
#       filters: {
#         field: reports.reports_count_distinct
#         value: "NOT NULL"
#       }
#     }
#   }
#   dimension_group: date_created {
#     type: time
#     timeframes: [
#       raw,
#       time,
#       date,
#       day_of_week,
#       hour_of_day,
#       week,
#       month,
#       quarter,
#       year,
#       time_of_day
#     ]
#     sql: ${TABLE}.date_created_time ;;
#   }
#
#   dimension_group: date_finished {
#     type: time
#     timeframes: [
#       raw,
#       time,
#       date,
#       week,
#       month,
#       quarter,
#       year,
#       day_of_week,
#       time_of_day,
#       hour_of_day,
#     ]
#     sql: ${TABLE}.date_finished_time ;;
#   }
# #   dimension: date_finished_date {
# #     type: date
# #   }
#   dimension: profile_confirm_count {
#     label: "Rater Actions - Profile Confirms Profile Confirm Count"
#     type: number
#   }
#   dimension: rated_post_count {
#     label: "Rater Actions - Posts Rated Post Count"
#     type: number
#   }
#   dimension: article_rate_count {
#     label: "Rater Actions - Article Rating Article Rate Count"
#     type: number
#   }
#   dimension: reports_count_distinct {
#     label: "Reports Reports Processed"
#     description: "The number of reports (not screens) that have been fully processed."
#     type: number
#   }
# }
