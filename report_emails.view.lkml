view: report_emails {
  sql_table_name: public.report_emails ;;

  #Brian Yardley - 12/15/17
  dimension: compound_key {
    primary_key: yes
    hidden: yes
    sql: ${TABLE}.report_id || '-' ||  ${TABLE}.email;;
  }

  dimension: email {
    type: string
    sql: ${TABLE}.email ;;
  }

  dimension: report_id {
    type: string
    # hidden: yes
    sql: ${TABLE}.report_id ;;
  }

  measure: count {
    type: count
    drill_fields: [reports.last_name, reports.middle_name, reports.report_id, reports.first_name]
  }
}
