view: report_facebook_twitter {
 # Or, you could make this view a derived table, like this:
  derived_table: {
    datagroup_trigger: etl_trigger
    distribution_style: all
    sql: SELECT
          reports.report_id  AS "report_id",
          COUNT(DISTINCT profiles.profile_id ) AS "profiles.count",
          COUNT(DISTINCT profiles.type ) AS "profiles.count_distinct_type",
          1 as "has_facebook_twitter"
        FROM public.reports  AS reports
        LEFT JOIN public.profiles  AS profiles ON profiles.report_id = reports.report_id

        WHERE
          (profiles.type = 'FACEBOOK' OR profiles.type = 'TWITTER')
        GROUP BY 1
        HAVING (COUNT(DISTINCT profiles.profile_id ) > 1) AND (COUNT(DISTINCT profiles.type ) > 1)
            ;;
  }
  dimension: report_id {
    type: number
    primary_key: yes
    sql: ${TABLE}.report_id ;;
  }
  dimension: has_facebook_twitter {
    type: yesno
    sql: ${TABLE}.has_facebook_twitter ;;
  }

  measure: report_count {
    label: "Reports with a Facebook and Twitter"
    type:  count
    filters: {
      field: has_facebook_twitter
      value: "yes"
    }
  }
#   dimension: count {}
#   dimension: count_distinct_type {}
}
