

view: view_article_actions{
  view_label: "Rater Actions - Article Rating"
  derived_table: {
    sql_trigger_value:  SELECT COUNT(*) FROM public.rater_actions ;;
    distribution_style: all
    explore_source: rater_actions {
      column: article_id {field: rater_actions.action_item}
      column: action_type {}
      column: start_time {field: rater_actions.start_raw}
      column: end_time {field: rater_actions.end_raw}
      column: duration {field: rater_actions.duration}
      column: duration_minutes {field: rater_actions.duration_minutes}
      column: rater_id {}
      column: username {}
      column: masked_username {}
      filters: {
        field: rater_actions.action_type
        value: "ARTICLE_MANUAL_RATE"
      }
    }
  }
  dimension: compound_primary_key {
    primary_key: yes
    hidden: yes
    sql: ${TABLE}.article_id || ${TABLE}.start_time ;;
  }
  dimension: article_id {
    #primary_key: yes
  }
  dimension: action_type {}
  dimension: duration {
  }
  dimension: duration_minutes {
  }
  dimension: rater_id {}
  dimension: username {}
  dimension: masked_username {}

  dimension_group: start {
    type: time
    timeframes: [
      raw,
      time,
      date,
      day_of_week,
      week,
      month,
      quarter,
      year,
      hour_of_day,
      hour
    ]
    sql: ${TABLE}.start_time ;;
  }

  dimension_group: end {
    type: time
    timeframes: [
      raw,
      time,
      date,
      day_of_week,
      week,
      month,
      quarter,
      year,
      hour_of_day,
      hour
    ]
    sql: ${TABLE}.end_time ;;
  }

  measure: count {
    type: count
  }

  measure: article_rate_count {
    label: "Tasks - Total Article Rates"
    type: count
    #sql: ${article_id} ;;
    filters: {
      field: end_time
      value: "-null"
    }
  }

  measure: sum_duration {
    type: sum
    sql: ${duration} ;;
  }

  measure: avg_duration {
    label: "Average Duration - Seconds"
    type: average
    value_format_name: "decimal_2"
    sql: ${duration} ;;
    filters: {
      field: end_date
      value: "-null"
    }
  }

#   measure: median_duration {
#     label: "Median Duration - Seconds"
#     type: median
#     sql: ${duration} ;;
#     value_format_name: "decimal_2"
#   }

  measure: sum_duration_minutes {
    label: "Total Article Rating Minutes"
    type: sum
    sql: ${duration_minutes} ;;
  }

  measure: avg_duration_minutes {
    type: average
    sql: ${duration_minutes} ;;
  }
}
